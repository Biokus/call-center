﻿using DHU.HTTPClientLibraries;
using Domain.CallCenter;
using Domain.CallCenter.DTO;
using Domain.Common.MethodParameter;
using Domain.Common.MethodResult;
using Domain.Security;
using System;
using System.Configuration;
using System.Threading.Tasks;

namespace UIP.CallCenter
{
    public sealed class BL
    {
        #region fields
        private String _BaseAddress = @"http://localhost:62968/Service.svc/rest2/";
        private JSerializer js = null;
        private JClient jc = null;
        #endregion fields
        #region properties
        public String BaseAddress { get { return this._BaseAddress; } }
        #endregion properties
        #region constructors & destructor
        public BL()
        {
            this.js = new JSerializer();
            this.getLogURL();
            this.jc = new JClient(this._BaseAddress);
        }
        #endregion constructors & destructor
        #region getLogURL
        private void getLogURL()
        {
            String value = ConfigurationManager.AppSettings["URLwsREST01"];
            if (!String.IsNullOrEmpty(value))
            {
                this._BaseAddress = value;
            }
        }
        #endregion getLogURL
        //
        #region manageAdvertisingCampaign
        public async Task<ManageEntityResult<AdvertisingCampaign>> manageAdvertisingCampaign(ManageEntityParameter<AdvertisingCampaign> p)
        {
            ManageEntityResult<AdvertisingCampaign> result = null;
            // serialize
            SerializeResult r1 = js.Serialize(p, typeof(ManageEntityParameter<AdvertisingCampaign>));
            if (!r1.ExecutionResult.Result.Successful.Value)
            {
                (result = new ManageEntityResult<AdvertisingCampaign>()).setValue(r1.ExecutionResult);
            }
            else
            {
                // invoke web service
                InvokeOperationResult r2 = await this.jc.InvokePostOperation("manageAdvertisingCampaignR/", r1.Json);
                if (!r2.ExecutionResult.Result.Successful.Value)
                {
                    (result = new ManageEntityResult<AdvertisingCampaign>()).setValue(r2.ExecutionResult);
                }
                else
                {
                    // deserialize
                    DeserializeResult r3 = js.Deserialize(r2.ResponseContent, typeof(ManageEntityResult<AdvertisingCampaign>));
                    if (!r3.ExecutionResult.Result.Successful.Value)
                    {
                        (result = new ManageEntityResult<AdvertisingCampaign>()).setValue(r3.ExecutionResult);
                    }
                    else
                    {
                        result = (ManageEntityResult<AdvertisingCampaign>)r3.Object;
                    }
                }
            }
            return result;
        }
        #endregion manageAdvertisingCampaign
        #region manageProspectiveClientStatus
        public async Task<ManageEntityResult<ProspectiveClientStatus>> manageProspectiveClientStatus(ManageEntityParameter<ProspectiveClientStatus> p)
        {
            ManageEntityResult<ProspectiveClientStatus> result = null;
            // serialize
            SerializeResult r1 = js.Serialize(p, typeof(ManageEntityParameter<ProspectiveClientStatus>));
            if (!r1.ExecutionResult.Result.Successful.Value)
            {
                (result = new ManageEntityResult<ProspectiveClientStatus>()).setValue(r1.ExecutionResult);
            }
            else
            {
                // invoke web service
                InvokeOperationResult r2 = await this.jc.InvokePostOperation("ProspectiveClientStatusR/", r1.Json);
                if (!r2.ExecutionResult.Result.Successful.Value)
                {
                    (result = new ManageEntityResult<ProspectiveClientStatus>()).setValue(r2.ExecutionResult);
                }
                else
                {
                    // deserialize
                    DeserializeResult r3 = js.Deserialize(r2.ResponseContent, typeof(ManageEntityResult<ProspectiveClientStatus>));
                    if (!r3.ExecutionResult.Result.Successful.Value)
                    {
                        (result = new ManageEntityResult<ProspectiveClientStatus>()).setValue(r3.ExecutionResult);
                    }
                    else
                    {
                        result = (ManageEntityResult<ProspectiveClientStatus>)r3.Object;
                    }
                }
            }
            return result;
        }
        #endregion manageProspectiveClientStatus
        #region manageSocialSecurityTypes
        public async Task<ManageEntityResult<SocialSecurityTypes>> manageSocialSecurityTypes(ManageEntityParameter<SocialSecurityTypes> p)
        {
            ManageEntityResult<SocialSecurityTypes> result = null;
            // serialize
            SerializeResult r1 = js.Serialize(p, typeof(ManageEntityParameter<SocialSecurityTypes>));
            if (!r1.ExecutionResult.Result.Successful.Value)
            {
                (result = new ManageEntityResult<SocialSecurityTypes>()).setValue(r1.ExecutionResult);
            }
            else
            {
                // invoke web service
                InvokeOperationResult r2 = await this.jc.InvokePostOperation("manageSocialSecurityTypesR/", r1.Json);
                if (!r2.ExecutionResult.Result.Successful.Value)
                {
                    (result = new ManageEntityResult<SocialSecurityTypes>()).setValue(r2.ExecutionResult);
                }
                else
                {
                    // deserialize
                    DeserializeResult r3 = js.Deserialize(r2.ResponseContent, typeof(ManageEntityResult<SocialSecurityTypes>));
                    if (!r3.ExecutionResult.Result.Successful.Value)
                    {
                        (result = new ManageEntityResult<SocialSecurityTypes>()).setValue(r3.ExecutionResult);
                    }
                    else
                    {
                        result = (ManageEntityResult<SocialSecurityTypes>)r3.Object;
                    }
                }
            }
            return result;
        }
        #endregion manageSocialSecurityTypes
        #region manageEmployee
        public async Task<ManageEntityResult<Employee>> manageEmployee(ManageEntityParameter<Employee> p)
        {
            ManageEntityResult<Employee> result = null;
            // serialize
            SerializeResult r1 = js.Serialize(p, typeof(ManageEntityParameter<Employee>));
            if (!r1.ExecutionResult.Result.Successful.Value)
            {
                (result = new ManageEntityResult<Employee>()).setValue(r1.ExecutionResult);
            }
            else
            {
                // invoke web service
                InvokeOperationResult r2 = await this.jc.InvokePostOperation("manageEmployeeR/", r1.Json);
                if (!r2.ExecutionResult.Result.Successful.Value)
                {
                    (result = new ManageEntityResult<Employee>()).setValue(r2.ExecutionResult);
                }
                else
                {
                    // deserialize
                    DeserializeResult r3 = js.Deserialize(r2.ResponseContent, typeof(ManageEntityResult<Employee>));
                    if (!r3.ExecutionResult.Result.Successful.Value)
                    {
                        (result = new ManageEntityResult<Employee>()).setValue(r3.ExecutionResult);
                    }
                    else
                    {
                        result = (ManageEntityResult<Employee>)r3.Object;
                    }
                }
            }
            return result;
        }
        #endregion manageEmployee
        #region manageProfile
        public async Task<ManageEntityResult<Profile>> manageProfile(ManageEntityParameter<Profile> p)
        {
            ManageEntityResult<Profile> result = null;
            // serialize
            SerializeResult r1 = js.Serialize(p, typeof(ManageEntityParameter<Profile>));
            if (!r1.ExecutionResult.Result.Successful.Value)
            {
                (result = new ManageEntityResult<Profile>()).setValue(r1.ExecutionResult);
            }
            else
            {
                // invoke web service
                InvokeOperationResult r2 = await this.jc.InvokePostOperation("manageProfileR/", r1.Json);
                if (!r2.ExecutionResult.Result.Successful.Value)
                {
                    (result = new ManageEntityResult<Profile>()).setValue(r2.ExecutionResult);
                }
                else
                {
                    // deserialize
                    DeserializeResult r3 = js.Deserialize(r2.ResponseContent, typeof(ManageEntityResult<Profile>));
                    if (!r3.ExecutionResult.Result.Successful.Value)
                    {
                        (result = new ManageEntityResult<Profile>()).setValue(r3.ExecutionResult);
                    }
                    else
                    {
                        result = (ManageEntityResult<Profile>)r3.Object;
                    }
                }
            }
            return result;
        }
        #endregion manageProfile
        #region manageUser
        public async Task<ManageEntityResult<User>> manageUser(ManageEntityParameter<User> p)
        {
            ManageEntityResult<User> result = null;
            // serialize
            SerializeResult r1 = js.Serialize(p, typeof(ManageEntityParameter<User>));
            if (!r1.ExecutionResult.Result.Successful.Value)
            {
                (result = new ManageEntityResult<User>()).setValue(r1.ExecutionResult);
            }
            else
            {
                // invoke web service
                InvokeOperationResult r2 = await this.jc.InvokePostOperation("manageUserR/", r1.Json);
                if (!r2.ExecutionResult.Result.Successful.Value)
                {
                    (result = new ManageEntityResult<User>()).setValue(r2.ExecutionResult);
                }
                else
                {
                    // deserialize
                    DeserializeResult r3 = js.Deserialize(r2.ResponseContent, typeof(ManageEntityResult<User>));
                    if (!r3.ExecutionResult.Result.Successful.Value)
                    {
                        (result = new ManageEntityResult<User>()).setValue(r3.ExecutionResult);
                    }
                    else
                    {
                        result = (ManageEntityResult<User>)r3.Object;
                    }
                }
            }
            return result;
        }
        #endregion manageUser
        #region manageCreditType
        public async Task<ManageEntityResult<CreditType>> manageCreditType(ManageEntityParameter<CreditType> p)
        {
            ManageEntityResult<CreditType> result = null;
            // serialize
            SerializeResult r1 = js.Serialize(p, typeof(ManageEntityParameter<CreditType>));
            if (!r1.ExecutionResult.Result.Successful.Value)
            {
                (result = new ManageEntityResult<CreditType>()).setValue(r1.ExecutionResult);
            }
            else
            {
                // invoke web service
                InvokeOperationResult r2 = await this.jc.InvokePostOperation("manageCreditTypeR/", r1.Json);
                if (!r2.ExecutionResult.Result.Successful.Value)
                {
                    (result = new ManageEntityResult<CreditType>()).setValue(r2.ExecutionResult);
                }
                else
                {
                    // deserialize
                    DeserializeResult r3 = js.Deserialize(r2.ResponseContent, typeof(ManageEntityResult<CreditType>));
                    if (!r3.ExecutionResult.Result.Successful.Value)
                    {
                        (result = new ManageEntityResult<CreditType>()).setValue(r3.ExecutionResult);
                    }
                    else
                    {
                        result = (ManageEntityResult<CreditType>)r3.Object;
                    }
                }
            }
            return result;
        }
        #endregion manageCreditType
        #region managePropertyType
        public async Task<ManageEntityResult<PropertyType>> managePropertyType(ManageEntityParameter<PropertyType> p)
        {
            ManageEntityResult<PropertyType> result = null;
            // serialize
            SerializeResult r1 = js.Serialize(p, typeof(ManageEntityParameter<PropertyType>));
            if (!r1.ExecutionResult.Result.Successful.Value)
            {
                (result = new ManageEntityResult<PropertyType>()).setValue(r1.ExecutionResult);
            }
            else
            {
                // invoke web service
                InvokeOperationResult r2 = await this.jc.InvokePostOperation("managePropertyTypeR/", r1.Json);
                if (!r2.ExecutionResult.Result.Successful.Value)
                {
                    (result = new ManageEntityResult<PropertyType>()).setValue(r2.ExecutionResult);
                }
                else
                {
                    // deserialize
                    DeserializeResult r3 = js.Deserialize(r2.ResponseContent, typeof(ManageEntityResult<PropertyType>));
                    if (!r3.ExecutionResult.Result.Successful.Value)
                    {
                        (result = new ManageEntityResult<PropertyType>()).setValue(r3.ExecutionResult);
                    }
                    else
                    {
                        result = (ManageEntityResult<PropertyType>)r3.Object;
                    }
                }
            }
            return result;
        }
        #endregion managePropertyType
        #region managePurchaseType
        public async Task<ManageEntityResult<PurchaseType>> managePurchaseType(ManageEntityParameter<PurchaseType> p)
        {
            ManageEntityResult<PurchaseType> result = null;
            // serialize
            SerializeResult r1 = js.Serialize(p, typeof(ManageEntityParameter<PurchaseType>));
            if (!r1.ExecutionResult.Result.Successful.Value)
            {
                (result = new ManageEntityResult<PurchaseType>()).setValue(r1.ExecutionResult);
            }
            else
            {
                // invoke web service
                InvokeOperationResult r2 = await this.jc.InvokePostOperation("managePurchaseTypeR/", r1.Json);
                if (!r2.ExecutionResult.Result.Successful.Value)
                {
                    (result = new ManageEntityResult<PurchaseType>()).setValue(r2.ExecutionResult);
                }
                else
                {
                    // deserialize
                    DeserializeResult r3 = js.Deserialize(r2.ResponseContent, typeof(ManageEntityResult<PurchaseType>));
                    if (!r3.ExecutionResult.Result.Successful.Value)
                    {
                        (result = new ManageEntityResult<PurchaseType>()).setValue(r3.ExecutionResult);
                    }
                    else
                    {
                        result = (ManageEntityResult<PurchaseType>)r3.Object;
                    }
                }
            }
            return result;
        }
        #endregion managePurchaseType
        #region manageProspectiveClientDetailDto
        public async Task<ManageEntityResult<ProspectiveClientDetailDto>> manageProspectiveClientDetailDto(ManageEntityParameter<ProspectiveClientDetailDto> p)
        {
            ManageEntityResult<ProspectiveClientDetailDto> result = null;
            // serialize
            SerializeResult r1 = js.Serialize(p, typeof(ManageEntityParameter<ProspectiveClientDetailDto>));
            if (!r1.ExecutionResult.Result.Successful.Value)
            {
                (result = new ManageEntityResult<ProspectiveClientDetailDto>()).setValue(r1.ExecutionResult);
            }
            else
            {
                // invoke web service
                InvokeOperationResult r2 = await this.jc.InvokePostOperation("ProspectiveClientDetailDtoR/", r1.Json);
                if (!r2.ExecutionResult.Result.Successful.Value)
                {
                    (result = new ManageEntityResult<ProspectiveClientDetailDto>()).setValue(r2.ExecutionResult);
                }
                else
                {
                    // deserialize
                    DeserializeResult r3 = js.Deserialize(r2.ResponseContent, typeof(ManageEntityResult<ProspectiveClientDetailDto>));
                    if (!r3.ExecutionResult.Result.Successful.Value)
                    {
                        (result = new ManageEntityResult<ProspectiveClientDetailDto>()).setValue(r3.ExecutionResult);
                    }
                    else
                    {
                        result = (ManageEntityResult<ProspectiveClientDetailDto>)r3.Object;
                    }
                }
            }
            return result;
        }
        #endregion manageProspectiveClientDetailDto
    }
}
