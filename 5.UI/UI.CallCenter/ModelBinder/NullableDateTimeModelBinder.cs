﻿using System;
using System.Globalization;
using System.Threading;
using System.Web.Mvc;

namespace UI.CallCenter.ModelBinder
{
    public class NullableDateTimeModelBinder : IModelBinder
    {
        #region BindModel
        public Object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            ValueProviderResult value = bindingContext.ValueProvider.GetValue(bindingContext.ModelName);
            if (String.IsNullOrWhiteSpace(value.AttemptedValue))
            {
                return null;
            }
            DateTime dateTime;
            Boolean isDate = DateTime.TryParse(value.AttemptedValue, Thread.CurrentThread.CurrentUICulture, DateTimeStyles.None, out dateTime);
            if (!isDate)
            {
                bindingContext.ModelState.AddModelError(bindingContext.ModelName, Resources.ModelBindingValidation.InvalidDateTime);
                return DateTime.UtcNow;
            }
            return dateTime;
        }
        #endregion BindModel
    }
}