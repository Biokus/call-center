﻿using FluentValidation.Mvc;
using System;
using System.Globalization;
using System.Threading;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using UI.CallCenter.ModelBinder;

namespace UI.CallCenter
{
    public class MvcApplication : System.Web.HttpApplication
    {
        #region Application_Start
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            //
            System.Web.Mvc.ModelBinders.Binders.Add(typeof(DateTime), new DateTimeModelBinder());
            System.Web.Mvc.ModelBinders.Binders.Add(typeof(Nullable<DateTime>), new NullableDateTimeModelBinder());
            //
            FluentValidationModelValidatorProvider.Configure();
        }
        #endregion Application_Start
        #region Application_BeginRequest
        protected void Application_BeginRequest(Object sender, EventArgs e)
        {
            //Thread.CurrentThread.CurrentCulture = new CultureInfo("es");
            Thread.CurrentThread.CurrentUICulture = new CultureInfo("es-MX");
        }
        #endregion Application_BeginRequest
    }
}
