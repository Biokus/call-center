﻿using Domain.CallCenter;
using Domain.Common.Enums;
using Domain.Common.MethodParameter;
using Domain.Common.MethodResult;
using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;
using UIP.CallCenter;

namespace UI.CallCenter.Areas.Marketing.Controllers
{
    public class AdvertisingCampaignController : Controller
    {
        #region Index
        // GET: Marketing/AdvertisingCampaigns
        public async Task<ActionResult> Index()
        {
            BL bl = new BL();
            ManageEntityResult<AdvertisingCampaign> result = null;
            ManageEntityParameter<AdvertisingCampaign> par = new ManageEntityParameter<AdvertisingCampaign>(null, OperationType.Read, new AdvertisingCampaign());
            result = await bl.manageAdvertisingCampaign(par);
            return View(result.Items);
        }
        #endregion Index
        #region Details
        // GET: Marketing/AdvertisingCampaigns/Details/5
        public async Task<ActionResult> Details(Nullable<Int32> id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BL bl = new BL();
            ManageEntityResult<AdvertisingCampaign> result = null;
            ManageEntityParameter<AdvertisingCampaign> par = new ManageEntityParameter<AdvertisingCampaign>(null, OperationType.Read, new AdvertisingCampaign(id, null, null, null, null, null, null));
            result = await bl.manageAdvertisingCampaign(par);
            AdvertisingCampaign advertisingCampaign = result.Items.FirstOrDefault();
            if (advertisingCampaign == null)
            {
                return HttpNotFound();
            }
            return View(advertisingCampaign);
        }
        #endregion Details
        #region Create
        // GET: Marketing/AdvertisingCampaigns/Create
        public ActionResult Create()
        {
            return View();
        }
        // POST: Marketing/AdvertisingCampaigns/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Name,DateStart,DateEnd,ContactName,ContactEmail,ContactPhoneNumber")] AdvertisingCampaign advertisingCampaign)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    BL bl = new BL();
                    ManageEntityResult<AdvertisingCampaign> result = null;
                    ManageEntityParameter<AdvertisingCampaign> par = new ManageEntityParameter<AdvertisingCampaign>(null, OperationType.Create, advertisingCampaign);
                    result = await bl.manageAdvertisingCampaign(par);
                    if (result.ExecutionResult.Result.Successful.Value)
                    {
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        TempData["HasAlert"] = true;
                        TempData["AlertText1"] = result.ExecutionResult.UserError.Code;
                        TempData["AlertText2"] = result.ExecutionResult.UserError.Message;
                        return View(advertisingCampaign);
                    }
                }
                else
                {
                    return View(advertisingCampaign);
                }
            }
            catch
            {
                return View(advertisingCampaign);
            }
        }
        #endregion Create
        #region Edit
        // GET: Marketing/AdvertisingCampaigns/Edit/5
        public async Task<ActionResult> Edit(Nullable<Int32> id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BL bl = new BL();
            ManageEntityResult<AdvertisingCampaign> result = null;
            ManageEntityParameter<AdvertisingCampaign> par = new ManageEntityParameter<AdvertisingCampaign>(null, OperationType.Read, new AdvertisingCampaign(id, null, null, null, null, null, null));
            result = await bl.manageAdvertisingCampaign(par);
            AdvertisingCampaign advertisingCampaign = result.Items.FirstOrDefault();
            if (advertisingCampaign == null)
            {
                return HttpNotFound();
            }
            return View(advertisingCampaign);
        }
        // POST: Marketing/AdvertisingCampaigns/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Name,DateStart,DateEnd,ContactName,ContactEmail,ContactPhoneNumber")] AdvertisingCampaign advertisingCampaign)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    BL bl = new BL();
                    ManageEntityResult<AdvertisingCampaign> result = null;
                    ManageEntityParameter<AdvertisingCampaign> par = new ManageEntityParameter<AdvertisingCampaign>(null, OperationType.Update, advertisingCampaign);
                    result = await bl.manageAdvertisingCampaign(par);
                    return RedirectToAction("Index");
                }
                return View(advertisingCampaign);
            }
            catch
            {
                return View(advertisingCampaign);
            }
        }
        #endregion Edit
        #region Delete
        // GET: Marketing/AdvertisingCampaigns/Delete/5
        public async Task<ActionResult> Delete(Nullable<Int32> id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BL bl = new BL();
            ManageEntityResult<AdvertisingCampaign> result = null;
            ManageEntityParameter<AdvertisingCampaign> par = new ManageEntityParameter<AdvertisingCampaign>(null, OperationType.Read, new AdvertisingCampaign(id, null, null, null, null, null, null));
            result = await bl.manageAdvertisingCampaign(par);
            AdvertisingCampaign advertisingCampaign = result.Items.FirstOrDefault();
            if (advertisingCampaign == null)
            {
                return HttpNotFound();
            }
            return View(advertisingCampaign);
        }
        // POST: Marketing/AdvertisingCampaigns/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(Nullable<Int32> id)
        {
            AdvertisingCampaign advertisingCampaign = null;
            try
            {
                BL bl = new BL();
                ManageEntityResult<AdvertisingCampaign> result = null;
                ManageEntityParameter<AdvertisingCampaign> par = new ManageEntityParameter<AdvertisingCampaign>(null, OperationType.Read, new AdvertisingCampaign(id, null, null, null, null, null, null));
                result = await bl.manageAdvertisingCampaign(par);
                advertisingCampaign = result.Items.FirstOrDefault();
                //
                par = new ManageEntityParameter<AdvertisingCampaign>(null, OperationType.Delete, new AdvertisingCampaign(id, null, null, null, null, null, null));
                result = await bl.manageAdvertisingCampaign(par);
                return RedirectToAction("Index");
            }
            catch
            {
                return View(advertisingCampaign);
            }
        }
        #endregion Delete
        #region Dispose
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                //db.Dispose(); Agregar esto ???
            }
            base.Dispose(disposing);
        }
        #endregion Dispose
    }
}
