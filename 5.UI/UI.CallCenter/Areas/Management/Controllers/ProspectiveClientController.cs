﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Domain.CallCenter;
using UI.CallCenter.Models;

namespace UI.CallCenter.Areas.Management.Controllers
{
    public class ProspectiveClientController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Management/ProspectiveClient
        public async Task<ActionResult> Index()
        {
            var prospectiveClients = db.ProspectiveClients.Include(p => p.AdvertisingCampaign).Include(p => p.CreditType).Include(p => p.LocationCity).Include(p => p.LocationState).Include(p => p.PropertyType).Include(p => p.ProspectiveClientDetail).Include(p => p.ProspectiveClientStatus).Include(p => p.PurchaseType);
            return View(await prospectiveClients.ToListAsync());
        }

        // GET: Management/ProspectiveClient/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProspectiveClient prospectiveClient = await db.ProspectiveClients.FindAsync(id);
            if (prospectiveClient == null)
            {
                return HttpNotFound();
            }
            return View(prospectiveClient);
        }

        // GET: Management/ProspectiveClient/Create
        public ActionResult Create()
        {
            ViewBag.AdvertisingCampaignId = new SelectList(db.AdvertisingCampaigns, "Id", "Name");
            ViewBag.CreditTypeId = new SelectList(db.CreditTypes, "Id", "Name");
            ViewBag.LocationCityId = new SelectList(db.LocationCities, "Id", "Name");
            ViewBag.LocationStateId = new SelectList(db.LocationStates, "Id", "Name");
            ViewBag.PropertyTypeId = new SelectList(db.PropertyTypes, "Id", "Name");
            ViewBag.Id = new SelectList(db.ProspectiveClientDetails, "ProspectiveClientId", "NSS");
            ViewBag.ProspectiveClientStatusId = new SelectList(db.ProspectiveClientStatus, "Id", "Name");
            ViewBag.PurchaseTypeId = new SelectList(db.PurchaseTypes, "Id", "Name");
            return View();
        }

        // POST: Management/ProspectiveClient/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,FirstName,SecondName,LastName1,LastName2,DateCreation,BirthDate,Telephone1,Telephone2,Email1,Email2,ResidenceLocationId,BirthLocationId,CURP,RFC,PropertyValue,Deposit,RequestedAmount,AdvertisingCampaignId,ProspectiveClientStatusId,PropertyTypeId,CreditTypeId,PurchaseTypeId,LocationStateId,LocationCityId,BrokerId")] ProspectiveClient prospectiveClient)
        {
            if (ModelState.IsValid)
            {
                db.ProspectiveClients.Add(prospectiveClient);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.AdvertisingCampaignId = new SelectList(db.AdvertisingCampaigns, "Id", "Name", prospectiveClient.AdvertisingCampaignId);
            ViewBag.CreditTypeId = new SelectList(db.CreditTypes, "Id", "Name", prospectiveClient.CreditTypeId);
            ViewBag.LocationCityId = new SelectList(db.LocationCities, "Id", "Name", prospectiveClient.LocationCityId);
            ViewBag.LocationStateId = new SelectList(db.LocationStates, "Id", "Name", prospectiveClient.LocationStateId);
            ViewBag.PropertyTypeId = new SelectList(db.PropertyTypes, "Id", "Name", prospectiveClient.PropertyTypeId);
            ViewBag.Id = new SelectList(db.ProspectiveClientDetails, "ProspectiveClientId", "NSS", prospectiveClient.Id);
            ViewBag.ProspectiveClientStatusId = new SelectList(db.ProspectiveClientStatus, "Id", "Name", prospectiveClient.ProspectiveClientStatusId);
            ViewBag.PurchaseTypeId = new SelectList(db.PurchaseTypes, "Id", "Name", prospectiveClient.PurchaseTypeId);
            return View(prospectiveClient);
        }

        // GET: Management/ProspectiveClient/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProspectiveClient prospectiveClient = await db.ProspectiveClients.FindAsync(id);
            if (prospectiveClient == null)
            {
                return HttpNotFound();
            }
            ViewBag.AdvertisingCampaignId = new SelectList(db.AdvertisingCampaigns, "Id", "Name", prospectiveClient.AdvertisingCampaignId);
            ViewBag.CreditTypeId = new SelectList(db.CreditTypes, "Id", "Name", prospectiveClient.CreditTypeId);
            ViewBag.LocationCityId = new SelectList(db.LocationCities, "Id", "Name", prospectiveClient.LocationCityId);
            ViewBag.LocationStateId = new SelectList(db.LocationStates, "Id", "Name", prospectiveClient.LocationStateId);
            ViewBag.PropertyTypeId = new SelectList(db.PropertyTypes, "Id", "Name", prospectiveClient.PropertyTypeId);
            ViewBag.Id = new SelectList(db.ProspectiveClientDetails, "ProspectiveClientId", "NSS", prospectiveClient.Id);
            ViewBag.ProspectiveClientStatusId = new SelectList(db.ProspectiveClientStatus, "Id", "Name", prospectiveClient.ProspectiveClientStatusId);
            ViewBag.PurchaseTypeId = new SelectList(db.PurchaseTypes, "Id", "Name", prospectiveClient.PurchaseTypeId);
            return View(prospectiveClient);
        }

        // POST: Management/ProspectiveClient/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,FirstName,SecondName,LastName1,LastName2,DateCreation,BirthDate,Telephone1,Telephone2,Email1,Email2,ResidenceLocationId,BirthLocationId,CURP,RFC,PropertyValue,Deposit,RequestedAmount,AdvertisingCampaignId,ProspectiveClientStatusId,PropertyTypeId,CreditTypeId,PurchaseTypeId,LocationStateId,LocationCityId,BrokerId")] ProspectiveClient prospectiveClient)
        {
            if (ModelState.IsValid)
            {
                db.Entry(prospectiveClient).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.AdvertisingCampaignId = new SelectList(db.AdvertisingCampaigns, "Id", "Name", prospectiveClient.AdvertisingCampaignId);
            ViewBag.CreditTypeId = new SelectList(db.CreditTypes, "Id", "Name", prospectiveClient.CreditTypeId);
            ViewBag.LocationCityId = new SelectList(db.LocationCities, "Id", "Name", prospectiveClient.LocationCityId);
            ViewBag.LocationStateId = new SelectList(db.LocationStates, "Id", "Name", prospectiveClient.LocationStateId);
            ViewBag.PropertyTypeId = new SelectList(db.PropertyTypes, "Id", "Name", prospectiveClient.PropertyTypeId);
            ViewBag.Id = new SelectList(db.ProspectiveClientDetails, "ProspectiveClientId", "NSS", prospectiveClient.Id);
            ViewBag.ProspectiveClientStatusId = new SelectList(db.ProspectiveClientStatus, "Id", "Name", prospectiveClient.ProspectiveClientStatusId);
            ViewBag.PurchaseTypeId = new SelectList(db.PurchaseTypes, "Id", "Name", prospectiveClient.PurchaseTypeId);
            return View(prospectiveClient);
        }

        // GET: Management/ProspectiveClient/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProspectiveClient prospectiveClient = await db.ProspectiveClients.FindAsync(id);
            if (prospectiveClient == null)
            {
                return HttpNotFound();
            }
            return View(prospectiveClient);
        }

        // POST: Management/ProspectiveClient/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            ProspectiveClient prospectiveClient = await db.ProspectiveClients.FindAsync(id);
            db.ProspectiveClients.Remove(prospectiveClient);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
