﻿using Domain.CallCenter.DTO;
using Domain.Common.Enums;
using Domain.Common.MethodParameter;
using Domain.Common.MethodResult;
using System.Data.Entity;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;
using UI.CallCenter.Models;
using UIP.CallCenter;

namespace UI.CallCenter.Areas.Management.Controllers
{
    public class ProspectiveClientDetailDtoController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        #region Index
        // GET: Management/ProspectiveClientDetailDto
        public async Task<ActionResult> Index()
        {
            return View(await db.ProspectiveClientDetailDtoes.ToListAsync());
        }
        #endregion Index
        #region Details
        // GET: Management/ProspectiveClientDetailDto/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProspectiveClientDetailDto prospectiveClientDetailDto = await db.ProspectiveClientDetailDtoes.FindAsync(id);
            if (prospectiveClientDetailDto == null)
            {
                return HttpNotFound();
            }
            return View(prospectiveClientDetailDto);
        }
        #endregion Details
        #region Create
        // GET: Management/ProspectiveClientDetailDto/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Management/ProspectiveClientDetailDto/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,firstName,secondName,lastName1,lastName2, dateCreation, birthDate, telephone1, telephone2, email1, email2, residenceLocationId, birthLocationId, curp, rfc, propertyValue, deposit, requestedAmount, advertisingCampaignId, prospectiveClientStatusId, propertyTypeId, creditTypeId, purchaseTypeId, locationStateId, locationCityId, brokerId,prospectiveClientId, nss, monthlySalary1, antiquity1, monthlySalary2, antiquity2, isActiveSAT, activiy, moneyReceipt, civilStatusId, isWorngCreditBureau, debtMonthly, problemType, isHasCreditHistory, observations, socialSecurityTypesId")] ProspectiveClientDetailDto prospectiveClientDetailDto)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    BL bl = new BL();
                    ManageEntityResult<ProspectiveClientDetailDto> result = null;
                    ManageEntityParameter<ProspectiveClientDetailDto> par = new ManageEntityParameter<ProspectiveClientDetailDto>(null, OperationType.Create, prospectiveClientDetailDto);
                    result = await bl.manageProspectiveClientDetailDto(par);
                    if (result.ExecutionResult.Result.Successful.Value)
                    {
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        TempData["HasAlert"] = true;
                        TempData["AlertText1"] = result.ExecutionResult.UserError.Code;
                        TempData["AlertText2"] = result.ExecutionResult.UserError.Message;
                        return View(prospectiveClientDetailDto);
                    }
                }
                else
                {
                    return View(prospectiveClientDetailDto);
                }
            }
            catch
            {
                return View(prospectiveClientDetailDto);
            }
        }
        #endregion Create
        #region Edit
        // GET: Management/ProspectiveClientDetailDto/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            //if (id == null)
            //{
            //    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            //}
            //ProspectiveClientDetailDto prospectiveClientDetailDto = await db.ProspectiveClientDetailDtoes.FindAsync(id);
            //if (prospectiveClientDetailDto == null)
            //{
            //    return HttpNotFound();
            //}
            //return View(prospectiveClientDetailDto);
            return View();
        }

        // POST: Management/ProspectiveClientDetailDto/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id")] ProspectiveClientDetailDto prospectiveClientDetailDto)
        {
            if (ModelState.IsValid)
            {
                db.Entry(prospectiveClientDetailDto).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(prospectiveClientDetailDto);
        }
        #endregion Edit
        #region Delete
        // GET: Management/ProspectiveClientDetailDto/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProspectiveClientDetailDto prospectiveClientDetailDto = await db.ProspectiveClientDetailDtoes.FindAsync(id);
            if (prospectiveClientDetailDto == null)
            {
                return HttpNotFound();
            }
            return View(prospectiveClientDetailDto);
        }

        // POST: Management/ProspectiveClientDetailDto/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            ProspectiveClientDetailDto prospectiveClientDetailDto = await db.ProspectiveClientDetailDtoes.FindAsync(id);
            db.ProspectiveClientDetailDtoes.Remove(prospectiveClientDetailDto);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }
        #endregion Delete
        #region Dispose
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        #endregion Dispose
    }
}
