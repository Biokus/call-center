﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Domain.CallCenter;
using UI.CallCenter.Models;

namespace UI.CallCenter.Areas.Management.Controllers
{
    public class ProspectiveClientDetailController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Management/ProspectiveClientDetail
        public async Task<ActionResult> Index()
        {
            var prospectiveClientDetails = db.ProspectiveClientDetails.Include(p => p.CivilStatus).Include(p => p.ProspectiveClient).Include(p => p.SocialSecurityTypes);
            return View(await prospectiveClientDetails.ToListAsync());
        }

        // GET: Management/ProspectiveClientDetail/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProspectiveClientDetail prospectiveClientDetail = await db.ProspectiveClientDetails.FindAsync(id);
            if (prospectiveClientDetail == null)
            {
                return HttpNotFound();
            }
            return View(prospectiveClientDetail);
        }

        // GET: Management/ProspectiveClientDetail/Create
        public ActionResult Create()
        {
            ViewBag.CivilStatusId = new SelectList(db.CivilStatus, "Id", "Name");
            ViewBag.ProspectiveClientId = new SelectList(db.ProspectiveClients, "Id", "FirstName");
            ViewBag.SocialSecurityTypesId = new SelectList(db.SocialSecurityTypes, "Id", "Name");
            return View();
        }

        // POST: Management/ProspectiveClientDetail/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "ProspectiveClientId,NSS,MonthlySalary1,Antiquity1,MonthlySalary2,Antiquity2,IsActiveSAT,Activiy,MoneyReceipt,CivilStatusId,IsWorngCreditBureau,DebtMonthly,ProblemType,IsHasCreditHistory,Observations,SocialSecurityTypesId")] ProspectiveClientDetail prospectiveClientDetail)
        {
            if (ModelState.IsValid)
            {
                db.ProspectiveClientDetails.Add(prospectiveClientDetail);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.CivilStatusId = new SelectList(db.CivilStatus, "Id", "Name", prospectiveClientDetail.CivilStatusId);
            ViewBag.ProspectiveClientId = new SelectList(db.ProspectiveClients, "Id", "FirstName", prospectiveClientDetail.ProspectiveClientId);
            ViewBag.SocialSecurityTypesId = new SelectList(db.SocialSecurityTypes, "Id", "Name", prospectiveClientDetail.SocialSecurityTypesId);
            return View(prospectiveClientDetail);
        }

        // GET: Management/ProspectiveClientDetail/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProspectiveClientDetail prospectiveClientDetail = await db.ProspectiveClientDetails.FindAsync(id);
            if (prospectiveClientDetail == null)
            {
                return HttpNotFound();
            }
            ViewBag.CivilStatusId = new SelectList(db.CivilStatus, "Id", "Name", prospectiveClientDetail.CivilStatusId);
            ViewBag.ProspectiveClientId = new SelectList(db.ProspectiveClients, "Id", "FirstName", prospectiveClientDetail.ProspectiveClientId);
            ViewBag.SocialSecurityTypesId = new SelectList(db.SocialSecurityTypes, "Id", "Name", prospectiveClientDetail.SocialSecurityTypesId);
            return View(prospectiveClientDetail);
        }

        // POST: Management/ProspectiveClientDetail/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ProspectiveClientId,NSS,MonthlySalary1,Antiquity1,MonthlySalary2,Antiquity2,IsActiveSAT,Activiy,MoneyReceipt,CivilStatusId,IsWorngCreditBureau,DebtMonthly,ProblemType,IsHasCreditHistory,Observations,SocialSecurityTypesId")] ProspectiveClientDetail prospectiveClientDetail)
        {
            if (ModelState.IsValid)
            {
                db.Entry(prospectiveClientDetail).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.CivilStatusId = new SelectList(db.CivilStatus, "Id", "Name", prospectiveClientDetail.CivilStatusId);
            ViewBag.ProspectiveClientId = new SelectList(db.ProspectiveClients, "Id", "FirstName", prospectiveClientDetail.ProspectiveClientId);
            ViewBag.SocialSecurityTypesId = new SelectList(db.SocialSecurityTypes, "Id", "Name", prospectiveClientDetail.SocialSecurityTypesId);
            return View(prospectiveClientDetail);
        }

        // GET: Management/ProspectiveClientDetail/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProspectiveClientDetail prospectiveClientDetail = await db.ProspectiveClientDetails.FindAsync(id);
            if (prospectiveClientDetail == null)
            {
                return HttpNotFound();
            }
            return View(prospectiveClientDetail);
        }

        // POST: Management/ProspectiveClientDetail/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            ProspectiveClientDetail prospectiveClientDetail = await db.ProspectiveClientDetails.FindAsync(id);
            db.ProspectiveClientDetails.Remove(prospectiveClientDetail);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
