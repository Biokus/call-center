﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(UI.CallCenter.Startup))]
namespace UI.CallCenter
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
