﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace UI.CallCenter.Models
{
    // Puede agregar datos del perfil del usuario agregando más propiedades a la clase ApplicationUser. Para más información, visite http://go.microsoft.com/fwlink/?LinkID=317594.
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Tenga en cuenta que el valor de authenticationType debe coincidir con el definido en CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Agregar aquí notificaciones personalizadas de usuario
            return userIdentity;
        }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        public System.Data.Entity.DbSet<Domain.CallCenter.DTO.ProspectiveClientDetailDto> ProspectiveClientDetailDtoes { get; set; }

        public System.Data.Entity.DbSet<Domain.CallCenter.ProspectiveClient> ProspectiveClients { get; set; }

        public System.Data.Entity.DbSet<Domain.CallCenter.AdvertisingCampaign> AdvertisingCampaigns { get; set; }

        public System.Data.Entity.DbSet<Domain.CallCenter.CreditType> CreditTypes { get; set; }

        public System.Data.Entity.DbSet<Domain.CallCenter.LocationCity> LocationCities { get; set; }

        public System.Data.Entity.DbSet<Domain.CallCenter.LocationState> LocationStates { get; set; }

        public System.Data.Entity.DbSet<Domain.CallCenter.PropertyType> PropertyTypes { get; set; }

        public System.Data.Entity.DbSet<Domain.CallCenter.ProspectiveClientDetail> ProspectiveClientDetails { get; set; }

        public System.Data.Entity.DbSet<Domain.CallCenter.ProspectiveClientStatus> ProspectiveClientStatus { get; set; }

        public System.Data.Entity.DbSet<Domain.CallCenter.PurchaseType> PurchaseTypes { get; set; }

        public System.Data.Entity.DbSet<Domain.CallCenter.CivilStatus> CivilStatus { get; set; }

        public System.Data.Entity.DbSet<Domain.CallCenter.SocialSecurityTypes> SocialSecurityTypes { get; set; }
    }
}