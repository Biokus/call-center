﻿using System;
using System.ServiceModel.Configuration;

namespace SLL.CallCenter.Json
{
    public class NewtonsoftJsonBehaviorExtension : BehaviorExtensionElement
    {
        #region properties
        public override Type BehaviorType { get { return typeof(NewtonsoftJsonBehavior); } }
        #endregion properties
        #region CreateBehavior
        protected override Object CreateBehavior()
        {
            return new NewtonsoftJsonBehavior();
        }
        #endregion CreateBehavior
    }
}