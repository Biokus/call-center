﻿using System;
using System.ServiceModel.Channels;

namespace SLL.CallCenter.Json
{
    public class GenericJsonContentTypeMapper : WebContentTypeMapper
    {
        #region GetMessageFormatForContentType
        public override WebContentFormat GetMessageFormatForContentType(String contentType)
        {
            if (contentType == "text/javascript" || contentType == "text/plain; charset=UTF-8" || contentType == "text/plain;charset=UTF-8")
            {
                return WebContentFormat.Json;
            }
            else
            {
                return WebContentFormat.Default;
            }
        }
        #endregion GetMessageFormatForContentType
    }
}
