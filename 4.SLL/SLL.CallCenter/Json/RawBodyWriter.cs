﻿using System;
using System.ServiceModel.Channels;
using System.Xml;

namespace SLL.CallCenter.Json
{
    public class RawBodyWriter : BodyWriter
    {
        #region fields
        private Byte[] content;
        #endregion fields
        #region constructors & destructor
        public RawBodyWriter(Byte[] content) : base(true)
        {
            this.content = content;
        }
        #endregion constructors & destructor
        #region OnWriteBodyContents
        protected override void OnWriteBodyContents(XmlDictionaryWriter writer)
        {
            writer.WriteStartElement("Binary");
            writer.WriteBase64(content, 0, content.Length);
            writer.WriteEndElement();
        }
        #endregion OnWriteBodyContents
    }
}