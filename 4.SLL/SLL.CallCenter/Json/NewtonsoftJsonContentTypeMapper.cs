﻿using System;
using System.ServiceModel.Channels;

namespace SLL.CallCenter.Json
{
    public class NewtonsoftJsonContentTypeMapper : WebContentTypeMapper
    {
        #region GetMessageFormatForContentType
        public override WebContentFormat GetMessageFormatForContentType(String contentType)
        {
            return WebContentFormat.Raw;
        }
        #endregion GetMessageFormatForContentType
    }
}