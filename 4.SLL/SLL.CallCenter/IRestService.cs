﻿using Domain.CallCenter;
using Domain.CallCenter.DTO;
using Domain.Common.MethodParameter;
using Domain.Common.MethodResult;
using Domain.Security;
using System.ComponentModel;
using System.ServiceModel;
using System.ServiceModel.Web;

namespace SLL.CallCenter
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de interfaz "IRestService" en el código y en el archivo de configuración a la vez.
    [ServiceContract(Name = "IRestService", Namespace = "htttp://www.socasesores.com")]
    public interface IRestService
    {
        #region manageAdvertisingCampaignR
        [OperationContract(Name = "manageAdvertisingCampaignR")]
        [WebInvoke(Method = "POST", UriTemplate = "manageAdvertisingCampaignR/", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        [Description("Administración de lectura, creación, modificación y eliminación de campañas")]
        ManageEntityResult<AdvertisingCampaign> manageAdvertisingCampaignR(ManageEntityParameter<AdvertisingCampaign> p);
        #endregion manageAdvertisingCampaignR
        #region manageProspectiveClientStatusR
        [OperationContract(Name = "manageProspectiveClientStatusR")]
        [WebInvoke(Method = "POST", UriTemplate = "manageProspectiveClientStatusR/", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        [Description("Administración de lectura, creación, modificación y eliminación de Estatus de Prospectos")]
        ManageEntityResult<ProspectiveClientStatus> manageProspectiveClientStatusR(ManageEntityParameter<ProspectiveClientStatus> p);
        #endregion manageEmployeeR
        #region manageSocialSecurityTypesR
        [OperationContract(Name = "manageSocialSecurityTypesR")]
        [WebInvoke(Method = "POST", UriTemplate = "manageSocialSecurityTypesR/", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        [Description("Administración de lectura, creación, modificación y eliminación de Tipos de Seguro Social")]
        ManageEntityResult<SocialSecurityTypes> manageSocialSecurityTypesR(ManageEntityParameter<SocialSecurityTypes> p);
        #endregion manageEmployeeR
        #region manageEmployeeR
        [OperationContract(Name = "manageEmployeeR")]
        [WebInvoke(Method = "POST", UriTemplate = "manageEmployeeR/", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        [Description("Administración de lectura, creación, modificación y eliminación de Empleados")]
        ManageEntityResult<Employee> manageEmployeeR(ManageEntityParameter<Employee> p);
        #endregion manageEmployeeR
        #region manageProfileR
        [OperationContract(Name = "manageProfileR")]
        [WebInvoke(Method = "POST", UriTemplate = "manageProfileR/", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        [Description("Administración de lectura, creación, modificación y eliminación de Perfiles")]
        ManageEntityResult<Profile> manageProfileR(ManageEntityParameter<Profile> p);
        #endregion manageProfileR
        #region manageUserR
        [OperationContract(Name = "manageUserR")]
        [WebInvoke(Method = "POST", UriTemplate = "manageUserR/", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        [Description("Administración de lectura, creación, modificación y eliminación de Usuarios")]
        ManageEntityResult<User> manageUserR(ManageEntityParameter<User> p);
        #endregion manageProfileR
        #region manageCreditTypeR
        [OperationContract(Name = "manageCreditTypeR")]
        [WebInvoke(Method = "POST", UriTemplate = "manageCreditTypeR/", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        [Description("Administración de lectura, creación, modificación y eliminación de Tipo de Créditos")]
        ManageEntityResult<CreditType> manageCreditTypeR(ManageEntityParameter<CreditType> p);
        #endregion manageCreditTypeR
        #region managePropertyTypeR
        [OperationContract(Name = "managePropertyTypeR")]
        [WebInvoke(Method = "POST", UriTemplate = "managePropertyTypeR/", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        [Description("Administración de lectura, creación, modificación y eliminación de Tipo de Inmueble")]
        ManageEntityResult<PropertyType> managePropertyTypeR(ManageEntityParameter<PropertyType> p);
        #endregion managePropertyTypeR
        #region managePurchaseTypeR
        [OperationContract(Name = "managePurchaseTypeR")]
        [WebInvoke(Method = "POST", UriTemplate = "managePurchaseTypeR/", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        [Description("Administración de lectura, creación, modificación y eliminación de Tipo de Compra")]
        ManageEntityResult<PurchaseType> managePurchaseTypeR(ManageEntityParameter<PurchaseType> p);
        #endregion managePurchaseTypeR
        #region manageProspectiveClientDtoR
        [OperationContract(Name = "manageProspectiveClientDtoR")]
        [WebInvoke(Method = "POST", UriTemplate = "manageProspectiveClientDtoR/", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        [Description("Administración de lectura, creación, modificación y eliminación de Prospecto Simple")]
        ManageEntityResult<ProspectiveClientDto> manageProspectiveClientDtoR(ManageEntityParameter<ProspectiveClientDto> p);
        #endregion manageProspectiveClientDtoR
        #region manageProspectiveClientDetailDtoR
        [OperationContract(Name = "manageProspectiveClientDetailDtoR")]
        [WebInvoke(Method = "POST", UriTemplate = "manageProspectiveClientDetailDtoR/", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        [Description("Administración de lectura, creación, modificación y eliminación de Prospecto")]
        ManageEntityResult<ProspectiveClientDetailDto> manageProspectiveClientDetailDtoR(ManageEntityParameter<ProspectiveClientDetailDto> p);
        #endregion manageProspectiveClientDetailDtoR
    }
}
