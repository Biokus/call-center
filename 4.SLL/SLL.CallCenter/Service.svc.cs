﻿using BLL.CallCenter;
using Domain.CallCenter;
using Domain.CallCenter.DTO;
using Domain.Common.MethodParameter;
using Domain.Common.MethodResult;
using Domain.Security;
using System;

namespace SLL.CallCenter
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class Service : ISoapService, IRestService
    {
        #region fields
        private Logic l = null;
        #endregion fields
        #region constructors & destructor
        public Service()
        {
            this.l = new Logic();
        }
        #endregion constructors & destructor
        //
        #region manageAdvertisingCampaignS
        public ManageEntityResult<AdvertisingCampaign> manageAdvertisingCampaignS(ManageEntityParameter<AdvertisingCampaign> p)
        {
            return this.l.manageAdvertisingCampaign(p);
        }
        #endregion manageAdvertisingCampaignS
        #region manageAdvertisingCampaignR
        public ManageEntityResult<AdvertisingCampaign> manageAdvertisingCampaignR(ManageEntityParameter<AdvertisingCampaign> p)
        {
            ManageEntityResult<AdvertisingCampaign> result = null;
            try
            {
                result = this.l.manageAdvertisingCampaign(p);
                return result;
            }
            catch (Exception ex)
            {
                string e = ex.Message;
            }
            return result;
        }
        #endregion manageAdvertisingCampaignR
        #region manageProspectiveClientStatusS
        public ManageEntityResult<ProspectiveClientStatus> manageProspectiveClientStatusS(ManageEntityParameter<ProspectiveClientStatus> p)
        {
            return this.l.manageProspectiveClientStatus(p);
        }
        #endregion manageProspectiveClientStatusS
        #region manageProspectiveClientStatusR
        public ManageEntityResult<ProspectiveClientStatus> manageProspectiveClientStatusR(ManageEntityParameter<ProspectiveClientStatus> p)
        {
            ManageEntityResult<ProspectiveClientStatus> result = null;
            try
            {
                result = this.l.manageProspectiveClientStatus(p);
                return result;
            }
            catch(Exception ex)
            {
                string e = ex.Message;
            }
            return result;
        }
        #endregion manageProspectiveClientStatusR
        #region manageSocialSecurityTypesS
        public ManageEntityResult<SocialSecurityTypes> manageSocialSecurityTypesS(ManageEntityParameter<SocialSecurityTypes> p)
        {
            return this.l.manageSocialSecurityTypes(p);
        }
        #endregion manageSocialSecurityTypesS
        #region manageSocialSecurityTypesR
        public ManageEntityResult<SocialSecurityTypes> manageSocialSecurityTypesR(ManageEntityParameter<SocialSecurityTypes> p)
        {
            ManageEntityResult<SocialSecurityTypes> result = null;
            try
            {
                result = this.l.manageSocialSecurityTypes(p);
                return result;
            }
            catch (Exception ex)
            {
                string e = ex.Message;
            }
            return result;
        }
        #endregion manageSocialSecurityTypesR
        #region manageEmployeeS
        public ManageEntityResult<Employee> manageEmployeeS(ManageEntityParameter<Employee> p)
        {
            return this.l.manageEmployee(p);
        }
        #endregion manageEmployeeS
        #region manageEmployeeR
        public ManageEntityResult<Employee> manageEmployeeR(ManageEntityParameter<Employee> p)
        {
            ManageEntityResult<Employee> result = null;
            try
            {
                result = this.l.manageEmployee(p);
            }
            catch (Exception ex)
            {
                string e = ex.Message;
            }
            return result;
        }
        #endregion manageEmployeeR
        #region manageProfileS
        public ManageEntityResult<Profile> manageProfileS(ManageEntityParameter<Profile> p)
        {
            return this.l.manageProfile(p);
        }
        #endregion manageProfileS
        #region manageProfileR
        public ManageEntityResult<Profile> manageProfileR(ManageEntityParameter<Profile> p)
        {
            ManageEntityResult<Profile> result = null;
            try
            {
                result = this.l.manageProfile(p);
                return result;
            }
            catch (Exception ex)
            {
                string e = ex.Message;
            }
            return result;
        }
        #endregion manageProfileR
        #region manageUserS
        public ManageEntityResult<User> manageUserS(ManageEntityParameter<User> p)
        {
            return this.l.manageUser(p);
        }
        #endregion manageUserS
        #region manageUserR
        public ManageEntityResult<User> manageUserR(ManageEntityParameter<User> p)
        {
            ManageEntityResult<User> result = null;
            try
            {
                result = this.l.manageUser(p);
                return result;
            }
            catch (Exception ex)
            {
                string e = ex.Message;
            }
            return result;
        }
        #endregion manageUserR
        #region manageCreditTypeS
        public ManageEntityResult<CreditType> manageCreditTypeS(ManageEntityParameter<CreditType> p)
        {
            return this.l.manageCreditType(p);
        }
        #endregion manageCreditTypeS
        #region manageCreditTypeR
        public ManageEntityResult<CreditType> manageCreditTypeR(ManageEntityParameter<CreditType> p)
        {
            ManageEntityResult<CreditType> result = null;
            try
            {
                result = this.l.manageCreditType(p);
                return result;
            }
            catch (Exception ex)
            {
                string e = ex.Message;
            }
            return result;
        }
        #endregion manageCreditTypeR
        #region managePropertyTypeS
        public ManageEntityResult<PropertyType> managePropertyTypeS(ManageEntityParameter<PropertyType> p)
        {
            return this.l.managePropertyType(p);
        }
        #endregion managePropertyTypeS
        #region managePropertyTypeR
        public ManageEntityResult<PropertyType> managePropertyTypeR(ManageEntityParameter<PropertyType> p)
        {
            ManageEntityResult<PropertyType> result = null;
            try
            {
                result = this.l.managePropertyType(p);
                return result;
            }
            catch (Exception ex)
            {
                string e = ex.Message;
            }
            return result;
        }
        #endregion managePropertyTypeR
        #region managePurchaseTypeS
        public ManageEntityResult<PurchaseType> managePurchaseTypeS(ManageEntityParameter<PurchaseType> p)
        {
            return this.l.managePurchaseType(p);
        }
        #endregion managePurchaseTypeS
        #region managePurchaseTypeR
        public ManageEntityResult<PurchaseType> managePurchaseTypeR(ManageEntityParameter<PurchaseType> p)
        {
            ManageEntityResult<PurchaseType> result = null;
            try
            {
                result = this.l.managePurchaseType(p);
                return result;
            }
            catch (Exception ex)
            {
                string e = ex.Message;
            }
            return result;
        }
        #endregion managePurchaseTypeR
        #region manageProspectiveClientDtoS
        public ManageEntityResult<ProspectiveClientDto> manageProspectiveClientDtoS(ManageEntityParameter<ProspectiveClientDto> p)
        {
            return this.l.manageProspectiveClientDto(p);
        }
        #endregion manageProspectiveClientDtoS
        #region manageProspectiveClientDtoR
        public ManageEntityResult<ProspectiveClientDto> manageProspectiveClientDtoR(ManageEntityParameter<ProspectiveClientDto> p)
        {
            ManageEntityResult<ProspectiveClientDto> result = null;
            try
            {
                result = this.l.manageProspectiveClientDto(p);
                return result;
            }
            catch (Exception ex)
            {
                string e = ex.Message;
            }
            return result;
        }
        #endregion manageProspectiveClientDtoR
        #region manageProspectiveClientDetailDtoS
        public ManageEntityResult<ProspectiveClientDetailDto> manageProspectiveClientDetailDtoS(ManageEntityParameter<ProspectiveClientDetailDto> p)
        {
            return this.l.manageProspectiveClientDetailDto(p);
        }
        #endregion manageProspectiveClientDetailDtoS
        #region manageProspectiveClientDetailDtoR
        public ManageEntityResult<ProspectiveClientDetailDto> manageProspectiveClientDetailDtoR(ManageEntityParameter<ProspectiveClientDetailDto> p)
        {
            ManageEntityResult<ProspectiveClientDetailDto> result = null;
            try
            {
                result = this.l.manageProspectiveClientDetailDto(p);
                return result;
            }
            catch (Exception ex)
            {
                string e = ex.Message;
            }
            return result;
        }
        #endregion manageProspectiveClientDetailDtoR
    }
}
