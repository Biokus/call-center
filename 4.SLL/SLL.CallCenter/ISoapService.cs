﻿using Domain.CallCenter;
using Domain.CallCenter.DTO;
using Domain.Common.MethodParameter;
using Domain.Common.MethodResult;
using Domain.Security;
using System.ComponentModel;
using System.ServiceModel;

namespace SLL.CallCenter
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de interfaz "ISoapService" en el código y en el archivo de configuración a la vez.
    [ServiceContract(Name = "ISoapService", Namespace = "htttp://www.socasesores.com")]
    public interface ISoapService
    {
        #region manageAdvertisingCampaignS
        [OperationContract(Name = "manageAdvertisingCampaignS")]
        [Description("Administración de lectura, creación, modificación y eliminación de campañas")]
        ManageEntityResult<AdvertisingCampaign> manageAdvertisingCampaignS(ManageEntityParameter<AdvertisingCampaign> p);
        #endregion manageAdvertisingCampaignS
        #region manageProspectiveClientStatusS
        [OperationContract(Name = "manageProspectiveClientStatusS")]
        [Description("Administración de lectura, creación, modificación y eliminación de Estatus de Prospectos")]
        ManageEntityResult<ProspectiveClientStatus> manageProspectiveClientStatusS(ManageEntityParameter<ProspectiveClientStatus> p);
        #endregion manageProspectiveClientStatusS
        #region manageSocialSecurityTypesS
        [OperationContract(Name = "manageSocialSecurityTypesS")]
        [Description("Administración de lectura, creación, modificación y eliminación de Tipos de Securidad Social")]
        ManageEntityResult<SocialSecurityTypes> manageSocialSecurityTypesS(ManageEntityParameter<SocialSecurityTypes> p);
        #endregion manageSocialSecurityTypesS
        #region manageEmployeeS
        [OperationContract(Name = "manageEmployeeS")]
        [Description("Administración de lectura, creación, modificación y eliminación de Empleados")]
        ManageEntityResult<Employee> manageEmployeeS(ManageEntityParameter<Employee> p);
        #endregion manageSocialSecurityTypesS
        #region manageProfileS
        [OperationContract(Name = "manageProfileS")]
        [Description("Administración de lectura, creación, modificación y eliminación de Perfiles")]
        ManageEntityResult<Profile> manageProfileS(ManageEntityParameter<Profile> p);
        #endregion manageProfileS
        #region manageUserS
        [OperationContract(Name = "manageUserS")]
        [Description("Administración de lectura, creación, modificación y eliminación de Users")]
        ManageEntityResult<User> manageUserS(ManageEntityParameter<User> p);
        #endregion manageUserS
        #region manageCreditTypeS
        [OperationContract(Name = "manageCreditTypeS")]
        [Description("Administración de lectura, creación, modificación y eliminación de Tipos de Crédito")]
        ManageEntityResult<CreditType> manageCreditTypeS(ManageEntityParameter<CreditType> p);
        #endregion manageCreditTypeS
        #region managePropertyTypeS
        [OperationContract(Name = "managePropertyTypeS")]
        [Description("Administración de lectura, creación, modificación y eliminación de Tipos de Inmueble")]
        ManageEntityResult<PropertyType> managePropertyTypeS(ManageEntityParameter<PropertyType> p);
        #endregion manageUserS
        #region managePurchaseTypeS
        [OperationContract(Name = "managePurchaseTypeS")]
        [Description("Administración de lectura, creación, modificación y eliminación de Tipos de Compra")]
        ManageEntityResult<PurchaseType> managePurchaseTypeS(ManageEntityParameter<PurchaseType> p);
        #endregion manageUserS
        #region manageProspectiveClientDtoS
        [OperationContract(Name = "manageProspectiveClientDtoS")]
        [Description("Administración de lectura, creación, modificación y eliminación de Prospecto Simple")]
        ManageEntityResult<ProspectiveClientDto> manageProspectiveClientDtoS(ManageEntityParameter<ProspectiveClientDto> p);
        #endregion manageProspectiveClientDtoS
        #region manageProspectiveClientDetailDtoS
        [OperationContract(Name = "manageProspectiveClientDetailDtoS")]
        [Description("Administración de lectura, creación, modificación y eliminación de Prospecto")]
        ManageEntityResult<ProspectiveClientDetailDto> manageProspectiveClientDetailDtoS(ManageEntityParameter<ProspectiveClientDetailDto> p);
        #endregion manageProspectiveClientDetailDtoS
    }
}