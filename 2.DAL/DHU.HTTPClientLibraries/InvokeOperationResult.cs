﻿using Domain.Common.Control;
using System;

namespace DHU.HTTPClientLibraries
{
    public sealed class InvokeOperationResult
    {
        #region fields
        
        #endregion fields
        #region properties
        public ExecutionResult ExecutionResult { get; set; }
        public String ResponseContent { get; set; }
        #endregion properties
        #region constructors & destructor
        public InvokeOperationResult()
        {
            this.ExecutionResult = new ExecutionResult();
            this.ResponseContent = null;
        }
        #endregion constructors & destructor
        #region setValue
        public void setValue(ExecutionResult executionResult)
        {
            this.ExecutionResult.setValue(executionResult);
        }
        public void setValue(ExecutionResult executionResult, String responseContent)
        {
            this.ExecutionResult.setValue(executionResult);
            this.ResponseContent = responseContent;
        }
        #endregion setValue
        #region setValueOk
        public void setValueOk()
        {
            this.ExecutionResult.setValueOk();
        }
        public void setValueOk(String responseContent)
        {
            this.setValueOk();
            this.ResponseContent = responseContent;
        }
        #endregion setValueOk
        #region setValueError
        public void setValueError(String errorMessage, String code, String message)
        {
            this.ExecutionResult.setValueError(errorMessage, code, message);
        }
        #endregion setValueError
    }
}
