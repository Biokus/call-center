﻿using Domain.Common.Control;
using System;

namespace DHU.HTTPClientLibraries
{
    public sealed class DeserializeResult
    {
        #region fields

        #endregion fields
        #region properties
        public ExecutionResult ExecutionResult { get; set; }
        public Object Object { get; set; }
        #endregion properties
        #region constructors & destructor
        public DeserializeResult()
        {
            this.ExecutionResult = new ExecutionResult();
            this.Object = null;
        }
        #endregion constructors & destructor
        #region setValue
        public void setValue(ExecutionResult executionResult)
        {
            this.ExecutionResult.setValue(executionResult);
        }
        public void setValue(ExecutionResult executionResult, Object obj)
        {
            this.ExecutionResult.setValue(executionResult);
            this.Object = obj;
        }
        #endregion setValue
        #region setValueOk
        public void setValueOk()
        {
            this.ExecutionResult.setValueOk();
        }
        public void setValueOk(Object obj)
        {
            this.setValueOk();
            this.Object = obj;
        }
        #endregion setValueOk
        #region setValueError
        public void setValueError(String errorMessage, String code, String message)
        {
            this.ExecutionResult.setValueError(errorMessage, code, message);
        }
        #endregion setValueError
    }
}
