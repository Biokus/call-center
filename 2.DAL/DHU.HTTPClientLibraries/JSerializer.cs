﻿using Domain.Common.Control;
using Newtonsoft.Json;
using System;

namespace DHU.HTTPClientLibraries
{
    public sealed class JSerializer
    {
        #region fields
        private String _uecException = @"99";
        private String _uemException = @"Error indeterminado";
        private JsonSerializerSettings _Settings = null;
        private Formatting _OutputFormat = Formatting.None;
        #endregion fields
        #region properties
        public JsonSerializerSettings Settings { get { return this._Settings; } }
        public Formatting OutputFormat { get { return this._OutputFormat; } set { this._OutputFormat = value; } }
        #endregion properties
        #region constructors & destructor
        public JSerializer()
        {
            this._Settings = new JsonSerializerSettings();
            this.setSettings();
        }
        #endregion constructors & destructor
        // helpers
        #region setExecutionError
        private ExecutionResult setExecutionError(String rem, String uec1, String uec2, String uem)
        {
            ExecutionResult result = new ExecutionResult();
            result.setValueError(rem, uec1 + @"." + uec2, uem);
            return result;
        }
        #endregion setExecutionError
        #region setExecutionException
        private ExecutionResult setExecutionException(Exception ex, String uec)
        {
            ExecutionResult result = new ExecutionResult();
            result.setValueError(ex.ToString(), uec + @"." + this._uecException, this._uemException);
            return result;
        }
        #endregion setExecutionException
        #region setSettings
        private void setSettings()
        {
            this._Settings.DateFormatHandling = DateFormatHandling.MicrosoftDateFormat;
            this._Settings.MissingMemberHandling = MissingMemberHandling.Ignore;
            this._Settings.ReferenceLoopHandling = ReferenceLoopHandling.Error;
            this._Settings.NullValueHandling = NullValueHandling.Include;
            this._Settings.DefaultValueHandling = DefaultValueHandling.Include;
            this._Settings.ObjectCreationHandling = ObjectCreationHandling.Auto;
            this._Settings.TypeNameHandling = TypeNameHandling.None;
            this._Settings.TypeNameAssemblyFormat = System.Runtime.Serialization.Formatters.FormatterAssemblyStyle.Simple;
            this._Settings.ConstructorHandling = ConstructorHandling.Default;
        }
        #endregion setSettings
        #region Serialize
        public SerializeResult Serialize(Object obj, Type type = null)
        {
            SerializeResult result = new SerializeResult();
            String uec = @"01";
            try
            {
                String json = JsonConvert.SerializeObject(obj, type, this._OutputFormat, this._Settings);
                result.setValueOk(json);
            }
            catch (Exception ex)
            {
                result.setValue(this.setExecutionException(ex, uec));
            }
            return result;
        }
        #endregion Serialize
        #region Deserialize
        public DeserializeResult Deserialize(String json, Type type)
        {
            DeserializeResult result = new DeserializeResult();
            String uec = @"02";
            try
            {
                Object obj = JsonConvert.DeserializeObject(json, type, this._Settings);
                result.setValueOk(obj);
            }
            catch (Exception ex)
            {
                result.setValue(this.setExecutionException(ex, uec));
            }
            return result;
        }
        #endregion Deserialize
    }
}
