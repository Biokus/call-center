﻿using Domain.Common.Control;
using System;

namespace DHU.HTTPClientLibraries
{
    public sealed class SerializeResult
    {
        #region fields
        
        #endregion fields
        #region properties
        public ExecutionResult ExecutionResult { get; set; }
        public String Json { get; set; }
        #endregion properties
        #region constructors & destructor
        public SerializeResult()
        {
            this.ExecutionResult = new ExecutionResult();
            this.Json = null;
        }
        #endregion constructors & destructor
        #region setValue
        public void setValue(ExecutionResult executionResult)
        {
            this.ExecutionResult.setValue(executionResult);
        }
        public void setValue(ExecutionResult executionResult, String json)
        {
            this.ExecutionResult.setValue(executionResult);
            this.Json = json;
        }
        #endregion setValue
        #region setValueOk
        public void setValueOk()
        {
            this.ExecutionResult.setValueOk();
        }
        public void setValueOk(String json)
        {
            this.setValueOk();
            this.Json = json;
        }
        #endregion setValueOk
        #region setValueError
        public void setValueError(String errorMessage, String code, String message)
        {
            this.ExecutionResult.setValueError(errorMessage, code, message);
        }
        #endregion setValueError
    }
}
