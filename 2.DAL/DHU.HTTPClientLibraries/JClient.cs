﻿using Domain.Common.Control;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace DHU.HTTPClientLibraries
{
    public sealed class JClient
    {
        #region fields
        private String _uecException = @"99";
        private String _uemException = @"Error indeterminado";
        private String _BaseAddress = null;
        #endregion fields
        #region properties
        public String BaseAddress { get { return this._BaseAddress; } }
        #endregion properties
        #region constructors & destructor
        public JClient(String baseAddress)
        {
            this._BaseAddress = baseAddress;
        }
        #endregion constructors & destructor
        // helpers
        #region setExecutionError
        private ExecutionResult setExecutionError(String rem, String uec1, String uec2, String uem)
        {
            ExecutionResult result = new ExecutionResult();
            result.setValueError(rem, uec1 + @"." + uec2, uem);
            return result;
        }
        #endregion setExecutionError
        #region setExecutionException
        private ExecutionResult setExecutionException(Exception ex, String uec)
        {
            ExecutionResult result = new ExecutionResult();
            result.setValueError(ex.ToString(), uec + @"." + this._uecException, this._uemException);
            return result;
        }
        #endregion setExecutionException
        #region InvokePostOperation
        public async Task<InvokeOperationResult> InvokePostOperation(String operation, String requestContent)
        {
            InvokeOperationResult result = new InvokeOperationResult();
            String uec = @"01";
            try
            {
                using (HttpClient httpClient = new HttpClient())
                {
                    httpClient.BaseAddress = new Uri(this._BaseAddress);
                    httpClient.DefaultRequestHeaders.Accept.Clear();
                    httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    using (StringContent sc = new StringContent(requestContent, Encoding.UTF8, "application/json"))
                    {
                        using (HttpResponseMessage response = await httpClient.PostAsync(operation, sc))
                        {
                            if (response.IsSuccessStatusCode)
                            {
                                String responseContent = await response.Content.ReadAsStringAsync();
                                result.setValueOk(responseContent);
                            }
                            else
                            {
                                String rem = response.StatusCode.ToString() + " - " + response.ReasonPhrase;
                                result.setValue(this.setExecutionError(rem, uec, @"01", @"No se puede invocar el servicio web"));
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                // HttpRequestException, TaskCanceledException
                result.setValue(this.setExecutionException(ex, uec));
            }
            return result;
        }
        #endregion InvokePostOperation
    }
}
