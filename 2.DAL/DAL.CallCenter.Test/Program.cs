﻿using BLL.CallCenter;
using Domain.Security;
using System;

namespace DAL.CallCenter.Test
{
    class Program
    {
        #region Main
        static void Main(string[] args)
        {
            Console.Clear();
            
            Employee p = new Employee(null,"Ely","Eliza ","Najera","Hernández","NAHE900429MDFJRL10","enajera@socasesores.com",DateTime.Now);
            using (var l = new Logic())
            {
                //e = new l.manageEmployee();
                //r.Create(p);
                //Employee e = r.Search(x => x.FirstName == @"Elizabeth");
                //PrintEntity(e);
                //Console.ReadLine();
            }
        }
        #endregion Main
        #region PrintEntity
        public static void PrintEntity(Employee p)
        {
            if (p != null)
            {
                Console.WriteLine(@"Id: " + p.Id.ToString());
                Console.WriteLine(@"Name: " + p.FirstName);
            }
            else
            {
                Console.WriteLine(@"Entidad no existente");
            }
        }
        #endregion PrintEntity
    }
}