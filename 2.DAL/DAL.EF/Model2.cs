namespace DAL.EF
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class Model2 : DbContext
    {
        public Model2()
            : base("name=Model5")
        {
        }

        public virtual DbSet<Campanas> Campanas { get; set; }
        public virtual DbSet<CompraTipos> CompraTipos { get; set; }
        public virtual DbSet<CreditoTipos> CreditoTipos { get; set; }
        public virtual DbSet<Employees> Employees { get; set; }
        public virtual DbSet<EstadoCivil> EstadoCivil { get; set; }
        public virtual DbSet<InmuebleTipos> InmuebleTipos { get; set; }
        public virtual DbSet<Profile> Profile { get; set; }
        public virtual DbSet<ProspectoDetalles> ProspectoDetalles { get; set; }
        public virtual DbSet<ProspectoEstatus> ProspectoEstatus { get; set; }
        public virtual DbSet<ProspectoHistorial> ProspectoHistorial { get; set; }
        public virtual DbSet<Prospectos> Prospectos { get; set; }
        public virtual DbSet<SeguroSocialTipos> SeguroSocialTipos { get; set; }
        public virtual DbSet<UbicacionCiudades> UbicacionCiudades { get; set; }
        public virtual DbSet<UbicacionEstados> UbicacionEstados { get; set; }
        public virtual DbSet<Users> Users { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Campanas>()
                .HasMany(e => e.Prospectos)
                .WithRequired(e => e.Campanas)
                .HasForeignKey(e => e.CampanaId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<CompraTipos>()
                .HasMany(e => e.Prospectos)
                .WithOptional(e => e.CompraTipos)
                .HasForeignKey(e => e.CompraTipoId);

            modelBuilder.Entity<CreditoTipos>()
                .HasMany(e => e.Prospectos)
                .WithOptional(e => e.CreditoTipos)
                .HasForeignKey(e => e.CreditoTipoId);

            modelBuilder.Entity<Employees>()
                .HasMany(e => e.Users)
                .WithRequired(e => e.Employees)
                .HasForeignKey(e => e.IdEmployee)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<InmuebleTipos>()
                .HasMany(e => e.Prospectos)
                .WithOptional(e => e.InmuebleTipos)
                .HasForeignKey(e => e.InmuebleTipoId);

            modelBuilder.Entity<Profile>()
                .HasMany(e => e.Users)
                .WithRequired(e => e.Profile)
                .HasForeignKey(e => e.IdProfile)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ProspectoDetalles>()
                .Property(e => e.IngresoMensual1)
                .HasPrecision(19, 4);

            modelBuilder.Entity<ProspectoDetalles>()
                .Property(e => e.IngresoMensual2)
                .HasPrecision(19, 4);

            modelBuilder.Entity<ProspectoDetalles>()
                .Property(e => e.DeudaMensual)
                .HasPrecision(19, 4);

            modelBuilder.Entity<ProspectoEstatus>()
                .HasMany(e => e.Prospectos)
                .WithRequired(e => e.ProspectoEstatus)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ProspectoEstatus>()
                .HasMany(e => e.ProspectoHistorial)
                .WithRequired(e => e.ProspectoEstatus)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Prospectos>()
                .Property(e => e.ValorInmueble)
                .HasPrecision(19, 4);

            modelBuilder.Entity<Prospectos>()
                .Property(e => e.Enganche)
                .HasPrecision(19, 4);

            modelBuilder.Entity<Prospectos>()
                .Property(e => e.MontoSolicitado)
                .HasPrecision(19, 4);

            modelBuilder.Entity<Prospectos>()
                .HasOptional(e => e.ProspectoDetalles)
                .WithRequired(e => e.Prospectos);

            modelBuilder.Entity<Prospectos>()
                .HasMany(e => e.ProspectoHistorial)
                .WithRequired(e => e.Prospectos)
                .HasForeignKey(e => e.ProspectoId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SeguroSocialTipos>()
                .HasMany(e => e.ProspectoDetalles)
                .WithOptional(e => e.SeguroSocialTipos)
                .HasForeignKey(e => e.SeguroSocialTipoId);

            modelBuilder.Entity<UbicacionCiudades>()
                .HasMany(e => e.Prospectos)
                .WithOptional(e => e.UbicacionCiudades)
                .HasForeignKey(e => e.UbicacionCiudadId);

            modelBuilder.Entity<UbicacionEstados>()
                .HasMany(e => e.Prospectos)
                .WithOptional(e => e.UbicacionEstados)
                .HasForeignKey(e => e.LugarResidencia);

            modelBuilder.Entity<UbicacionEstados>()
                .HasMany(e => e.Prospectos1)
                .WithOptional(e => e.UbicacionEstados1)
                .HasForeignKey(e => e.LugarNacimiento);

            modelBuilder.Entity<UbicacionEstados>()
                .HasMany(e => e.Prospectos2)
                .WithOptional(e => e.UbicacionEstados2)
                .HasForeignKey(e => e.UbicacionEstadoId);

            modelBuilder.Entity<UbicacionEstados>()
                .HasMany(e => e.UbicacionCiudades)
                .WithRequired(e => e.UbicacionEstados)
                .HasForeignKey(e => e.UbicacionEstadoId)
                .WillCascadeOnDelete(false);
        }
    }
}
