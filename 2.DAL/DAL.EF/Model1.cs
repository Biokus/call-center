namespace DAL.EF
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class Model1 : DbContext
    {
        public Model1()
            : base("name=Model12")
        {
        }

        public virtual DbSet<Employees> Employees { get; set; }
        public virtual DbSet<SecurityProfiles> SecurityProfiles { get; set; }
        public virtual DbSet<SecurityUsers> SecurityUsers { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Employees>()
                .HasMany(e => e.SecurityUsers)
                .WithRequired(e => e.Employees)
                .HasForeignKey(e => e.EmployeeId)
                .WillCascadeOnDelete(false);
        }
    }
}
