namespace DAL.EF
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Prospectos
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Prospectos()
        {
            ProspectoHistorial = new HashSet<ProspectoHistorial>();
        }

        public int Id { get; set; }

        public int ProspectoEstatusId { get; set; }

        public int CampanaId { get; set; }

        public DateTime FechaRegistro { get; set; }

        [Required]
        [StringLength(250)]
        public string Nombre { get; set; }

        [Column(TypeName = "date")]
        public DateTime? FechaNacimiento { get; set; }

        [Required]
        [StringLength(20)]
        public string Telefono1 { get; set; }

        [StringLength(20)]
        public string Telefono2 { get; set; }

        [Required]
        [StringLength(250)]
        public string Email1 { get; set; }

        [StringLength(250)]
        public string Email2 { get; set; }

        public int? LugarResidencia { get; set; }

        public int? LugarNacimiento { get; set; }

        [StringLength(20)]
        public string CURP { get; set; }

        [StringLength(15)]
        public string RFC { get; set; }

        [Column(TypeName = "money")]
        public decimal? ValorInmueble { get; set; }

        [Column(TypeName = "money")]
        public decimal? Enganche { get; set; }

        [Column(TypeName = "money")]
        public decimal? MontoSolicitado { get; set; }

        public int? InmuebleTipoId { get; set; }

        public int? CreditoTipoId { get; set; }

        public int? CompraTipoId { get; set; }

        public int? UbicacionEstadoId { get; set; }

        public int? UbicacionCiudadId { get; set; }

        public int? BrokerId { get; set; }

        public virtual Campanas Campanas { get; set; }

        public virtual CompraTipos CompraTipos { get; set; }

        public virtual CreditoTipos CreditoTipos { get; set; }

        public virtual InmuebleTipos InmuebleTipos { get; set; }

        public virtual ProspectoDetalles ProspectoDetalles { get; set; }

        public virtual ProspectoEstatus ProspectoEstatus { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ProspectoHistorial> ProspectoHistorial { get; set; }

        public virtual UbicacionEstados UbicacionEstados { get; set; }

        public virtual UbicacionEstados UbicacionEstados1 { get; set; }

        public virtual UbicacionEstados UbicacionEstados2 { get; set; }

        public virtual UbicacionCiudades UbicacionCiudades { get; set; }
    }
}
