namespace DAL.EF
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ProspectoHistorial")]
    public partial class ProspectoHistorial
    {
        public int Id { get; set; }

        public int ProspectoId { get; set; }

        public DateTime Fecha { get; set; }

        public int ProspectoEstatusId { get; set; }

        public int EjecutivoId { get; set; }

        [StringLength(500)]
        public string Comentario { get; set; }

        public virtual ProspectoEstatus ProspectoEstatus { get; set; }

        public virtual Prospectos Prospectos { get; set; }
    }
}
