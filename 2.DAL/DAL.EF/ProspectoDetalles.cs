namespace DAL.EF
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ProspectoDetalles
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ProspectoId { get; set; }

        public int? SeguroSocialTipoId { get; set; }

        [StringLength(15)]
        public string NSS { get; set; }

        [Column(TypeName = "money")]
        public decimal? IngresoMensual1 { get; set; }

        public short? Antigüedad1 { get; set; }

        [Column(TypeName = "money")]
        public decimal? IngresoMensual2 { get; set; }

        public bool AltaHacienda { get; set; }

        public short? Antigüedad2 { get; set; }

        [StringLength(250)]
        public string Actividad { get; set; }

        [StringLength(250)]
        public string ComprobanteIngresos { get; set; }

        public int? EstadoCivilId { get; set; }

        public bool ProblemasBuro { get; set; }

        [Column(TypeName = "money")]
        public decimal? DeudaMensual { get; set; }

        [StringLength(250)]
        public string TipoProblema { get; set; }

        public bool HistorialCrediticio { get; set; }

        [StringLength(500)]
        public string Observaciones { get; set; }

        public virtual EstadoCivil EstadoCivil { get; set; }

        public virtual Prospectos Prospectos { get; set; }

        public virtual SeguroSocialTipos SeguroSocialTipos { get; set; }
    }
}
