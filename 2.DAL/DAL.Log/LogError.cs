﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace DAL.Log
{
    public static class LogError
    {
        #region fields

        #endregion fields
        #region Add
        public static Nullable<Int64> Add(Guid userId, DateTime registrationDate, String repositoryMethod, String error1, String error2, String error3)
        {
            Nullable<Int64> Id = null;
            try
            { 
                String s = ConfigurationManager.ConnectionStrings["LogError"].ConnectionString;
                using (SqlConnection con = new SqlConnection(s))
                {
                    using (SqlCommand com = new SqlCommand())
                    {
                        com.Connection = con;
                        com.CommandType = CommandType.StoredProcedure;
                        com.CommandText = @"InsertError";
                        SqlParameter parIn = null;
                        SqlParameter parOut = null;
                        SqlParameter parRV = null;
                        parIn = new SqlParameter(@"@UserId", SqlDbType.UniqueIdentifier);
                        parIn.Direction = ParameterDirection.Input;
                        parIn.Value = userId;
                        com.Parameters.Add(parIn);
                        parIn = new SqlParameter(@"@RegistrationDate", SqlDbType.DateTime);
                        parIn.Direction = ParameterDirection.Input;
                        parIn.Value = registrationDate;
                        com.Parameters.Add(parIn);
                        parIn = new SqlParameter(@"@RepositoryMethod", SqlDbType.NVarChar, 20);
                        parIn.Direction = ParameterDirection.Input;
                        parIn.Value = repositoryMethod;
                        com.Parameters.Add(parIn);
                        parIn = new SqlParameter(@"@Error1", SqlDbType.NVarChar, 500);
                        parIn.Direction = ParameterDirection.Input;
                        parIn.Value = error1;
                        com.Parameters.Add(parIn);
                        parIn = new SqlParameter(@"@Error2", SqlDbType.NVarChar, 4000);
                        parIn.Direction = ParameterDirection.Input;
                        parIn.Value = error2;
                        com.Parameters.Add(parIn);
                        parIn = new SqlParameter(@"@Error3", SqlDbType.NVarChar, 4000);
                        parIn.Direction = ParameterDirection.Input;
                        parIn.Value = error3;
                        com.Parameters.Add(parIn);
                        parOut = new SqlParameter(@"@Id", SqlDbType.BigInt);
                        parOut.Direction = ParameterDirection.Output;
                        com.Parameters.Add(parOut);
                        parRV = new SqlParameter("@retParam", SqlDbType.Int);
                        parRV.Direction = ParameterDirection.ReturnValue;
                        com.Parameters.Add(parRV);
                        con.Open();
                        com.ExecuteReader();
                        con.Close();
                        Id = Convert.ToInt32(parOut.Value);
                    }
                }
            }
            catch(Exception)
            {
                Id = null;
            }
            return Id;
        }
        #endregion Add
        #region Add
        public static Nullable<Int64> Add(Guid userId, DateTime registrationDate, String repositoryMethod, Exception ex)
        {
            Nullable<Int64> Id = null;
            Id = Add(userId, registrationDate, repositoryMethod, ex.Message, ex.ToString(), ex.StackTrace);
            return Id;
        }
        #endregion Add
    }
}
