﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace DAL.CallCenter
{
    internal interface IRepository<TEntity> : IDisposable where TEntity : class
    {
        TEntity Create(TEntity entity);
        IList<TEntity> Read(Expression<Func<TEntity, Boolean>> predicate);
        IList<TEntity> Read();
        TEntity Search(Expression<Func<TEntity, Boolean>> predicate);
        Boolean Update(TEntity entity);
        Boolean Delete(TEntity entity);
    }
}
