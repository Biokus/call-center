﻿using Domain.CallCenter;
using Domain.Security;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace DAL.CallCenter
{
    public partial class Model : DbContext
    {
        #region properties
        public virtual DbSet<AdvertisingCampaign> AdvertisingCampaigns { get; set; }
        public virtual DbSet<ProspectiveClientStatus> ProspectiveClientStatus { get; set; }
        public virtual DbSet<Employee> Employee { get; set; }
        public virtual DbSet<Profile> Profile { get; set; }
        public virtual DbSet<User> User { get; set; }
        public virtual DbSet<CreditType> CreditType { get; set; }
        public virtual DbSet<PropertyType> PropertyType { get; set; }
        public virtual DbSet<PurchaseType> PurchaseType { get; set; }
        public virtual DbSet<LocationState> LocationState { get; set; }
        public virtual DbSet<LocationCity> LocationCity { get; set; }
        public virtual DbSet<CivilStatus> CivilStatus { get; set; }
        public virtual DbSet<ProspectiveClient> ProspectiveClient { get; set; }
        public virtual DbSet<ProspectiveClientDetail> ProspectiveClientDetail { get; set; }
        public virtual DbSet<ProspectiveClientHistory> ProspectiveClientHistory { get; set; }
        #endregion properties
        #region constructors & destructor
        public Model() : base("name=CallCenterDB")
        {
            // Disable lazy loading
            this.Configuration.LazyLoadingEnabled = false;
        }
        #endregion constructors & destructor
        #region OnModelCreating
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            this.configAdvertisingCampaign(modelBuilder);
            this.configProspectiveClientStatus(modelBuilder);
            this.configSocialSecurityTypes(modelBuilder);
            this.configEmployee(modelBuilder);
            this.configProfiles(modelBuilder);
            this.configUsers(modelBuilder);
            this.configCreditType(modelBuilder);
            this.configPropertyType(modelBuilder);
            this.configPurchaseType(modelBuilder);
            this.configLocationState(modelBuilder);
            this.configLocationCity(modelBuilder);
            this.configCivilState(modelBuilder);
            this.configProspectiveClient(modelBuilder);
            this.configProspectiveClientDetail(modelBuilder);
            this.configProspectiveClientHistory(modelBuilder);
        }
        #endregion OnModelCreating
        #region configAdvertisingCampaign
        private void configAdvertisingCampaign(DbModelBuilder modelBuilder)
        {
            // Mapping database
            EntityTypeConfiguration<AdvertisingCampaign> e = modelBuilder.Entity<AdvertisingCampaign>();
            e.ToTable("AdvertisingCampaign");
            e.HasKey(x => x.Id).Property(x => x.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            e.Property(x => x.Name).HasColumnName("Name").HasMaxLength(250).IsRequired();
            //e.Property(x => x.DateStart).HasColumnName("DateStart").HasColumnType("date").IsRequired();
            e.Property(x => x.DateStart).HasColumnName("DateStart").IsRequired();
            e.Property(x => x.DateEnd).HasColumnName("DateEnd").HasColumnType("date").IsRequired();
            e.Property(x => x.ContactName).HasColumnName("ContactName").HasMaxLength(250).IsOptional();
            e.Property(x => x.ContactEmail).HasColumnName("ContactEmail").HasMaxLength(250).IsOptional();
            e.Property(x => x.ContactPhoneNumber).HasColumnName("ContactPhoneNumber").HasMaxLength(20).IsOptional();
            e.HasMany(x => x.ProspectiveClient).WithRequired(x => x.AdvertisingCampaign).HasForeignKey(x => x.AdvertisingCampaignId).WillCascadeOnDelete(false);
        }
        #endregion configAdvertisingCampaign
        #region configProspectiveClientStatus
        private void configProspectiveClientStatus(DbModelBuilder modelBuilder)
        {// Tabla ProspectiveClientStatus(EstatusProspecto)
            EntityTypeConfiguration<ProspectiveClientStatus> e = modelBuilder.Entity<ProspectiveClientStatus>();
            e.ToTable("ProspectiveClientStatus");
            e.HasKey(t => t.Id).Property(t => t.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            e.Property(n => n.Name).HasColumnName("Name").HasMaxLength(250).IsRequired();
            e.HasMany(x => x.ProspectiveClient).WithRequired(x => x.ProspectiveClientStatus).WillCascadeOnDelete(false);
            e.HasMany(x => x.ProspectiveClientHistory).WithRequired(x => x.ProspectiveClientStatus).WillCascadeOnDelete(false);
        }
        #endregion configProspectiveClientStatus
        #region configSocialSecurityTypes
        public void configSocialSecurityTypes(DbModelBuilder modelBuilder)
        {
            // Tabla SocialSecurityTypes(Seguro Social Tipos)
            EntityTypeConfiguration<SocialSecurityTypes> e = modelBuilder.Entity<SocialSecurityTypes>();
            e.ToTable("SocialSecurityTypes");
            e.HasKey(t => t.Id).Property(t => t.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            e.Property(n => n.Name).HasColumnName("Name").HasMaxLength(250).IsRequired();
            e.HasMany(x => x.ProspectiveClientDetail).WithOptional(x => x.SocialSecurityTypes).HasForeignKey(x => x.SocialSecurityTypesId);
        }
        #endregion configSocialSecurityTypes
        #region configEmployee
        public void configEmployee(DbModelBuilder modelBuilder)
        {
            // Tabla Employee(Empleados)
            EntityTypeConfiguration<Employee> e = modelBuilder.Entity<Employee>();
            e.ToTable("Employees");
            e.HasKey(t => t.Id).Property(t => t.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity).IsRequired(); ;
            e.Property(n => n.FirstName).HasColumnName("FirstName").HasMaxLength(250).IsRequired();
            e.Property(n => n.SecondName).HasColumnName("SecondName").HasMaxLength(250);
            e.Property(n => n.LastName1).HasColumnName("LastName1").HasMaxLength(250).IsRequired();
            e.Property(n => n.LastName2).HasColumnName("LastName2").HasMaxLength(250);
            e.Property(n => n.CURP).HasColumnName("CURP").HasMaxLength(20).IsRequired();
            e.Property(n => n.Email).HasColumnName("Email").HasMaxLength(50);
            e.Property(n => n.DateCreation).HasColumnName("DateCreation").HasColumnType("Date").IsRequired();
            e.HasMany(x => x.User).WithRequired(x => x.Employee).HasForeignKey(x => x.IdEmployee).WillCascadeOnDelete(false);
        }
        #endregion configEmployee
        #region configProfiles
        public void configProfiles(DbModelBuilder modelBuilder)
        {
            // Tabla Profiles(Roles)
            EntityTypeConfiguration<Profile> e = modelBuilder.Entity<Profile>();
            e.ToTable("Profiles");
            e.HasKey(t => t.Id).Property(t => t.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            e.Property(n => n.Name).HasColumnName("Name").HasMaxLength(250).IsRequired();
            e.HasMany(x => x.User).WithRequired(x => x.Profile).HasForeignKey(x => x.IdProfile).WillCascadeOnDelete(false);
        }
        #endregion configProfiles
        #region configUsers
        public void configUsers(DbModelBuilder modelBuilder)
        {
            // Tabla User(Usuarios)
            EntityTypeConfiguration<User> e = modelBuilder.Entity<User>();
            e.ToTable("Users");
            e.HasKey(t => t.Id).Property(t => t.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            e.Property(n => n.UserName).HasColumnName("UserName").HasMaxLength(250).IsRequired();
            e.Property(n => n.Password).HasColumnName("Password").HasMaxLength(250).IsRequired();
        }
        #endregion configUsers
        #region configCreditType
        public void configCreditType(DbModelBuilder modelBuilder)
        {
            // Tabla CreditType(Tipos de Crédito)
            EntityTypeConfiguration<CreditType> e = modelBuilder.Entity<CreditType>();
            e.ToTable("CreditType");
            e.HasKey(x => x.Id).Property(t => t.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            e.Property(x => x.Name).HasColumnName("Name").HasMaxLength(250).IsRequired();
            e.HasMany(x => x.ProspectiveClient).WithOptional(x => x.CreditType).HasForeignKey(x => x.CreditTypeId);
        }
        #endregion configCreditType
        #region configPropertyType
        public void configPropertyType(DbModelBuilder modelBuilder)
        {
            // Tabla PropertyType(Tipos de Inmueble)
            EntityTypeConfiguration<PropertyType> e = modelBuilder.Entity<PropertyType>();
            e.ToTable("PropertyType");
            e.HasKey(t => t.Id).Property(t => t.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            e.Property(n => n.Name).HasColumnName("Name").HasMaxLength(250).IsRequired();
            e.HasMany(x => x.ProspectiveClient).WithOptional(x => x.PropertyType).HasForeignKey(x => x.PropertyTypeId);
        }
        #endregion configPropertyType
        #region configPurchaseType
        public void configPurchaseType(DbModelBuilder modelBuilder)
        {
            // Tabla PurchaseType(Tipos de Compra)
            EntityTypeConfiguration<PurchaseType> e = modelBuilder.Entity<PurchaseType>();
            e.ToTable("PurchaseType");
            e.HasKey(x => x.Id).Property(t => t.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            e.Property(x => x.Name).HasColumnName("Name").HasMaxLength(250).IsRequired();
            e.HasMany(x => x.ProspectiveClient).WithOptional(x => x.PurchaseType).HasForeignKey(x => x.PurchaseTypeId);
        }
        #endregion configPurchaseType
        #region configLocationState
        public void configLocationState(DbModelBuilder modelBuilder)
        {
            // Tabla LocationState(UbicacionEstados)
            EntityTypeConfiguration<LocationState> e = modelBuilder.Entity<LocationState>();
            e.ToTable("LocationState");
            e.HasKey(t => t.Id).Property(t => t.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            e.Property(n => n.Name).HasColumnName("Name").HasMaxLength(250).IsRequired();
            e.HasMany(x => x.LocationCity).WithRequired(x => x.LocationState).HasForeignKey(x => x.IdLocationState).WillCascadeOnDelete(false);
            e.HasMany(x => x.ProspectiveClient).WithOptional(x => x.LocationState).HasForeignKey(x => x.ResidenceLocationId);
            e.HasMany(x => x.ProspectiveClient1).WithOptional(x => x.LocationState1).HasForeignKey(x => x.BirthLocationId);
            e.HasMany(x => x.ProspectiveClient2).WithOptional(x => x.LocationState2).HasForeignKey(x => x.LocationStateId);
        }
        #endregion configLocationState
        #region configLocationCity
        public void configLocationCity(DbModelBuilder modelBuilder)
        {
            // Tabla LocationCity(UbicacionCiudades)
            EntityTypeConfiguration<LocationCity> e = modelBuilder.Entity<LocationCity>();
            e.ToTable("LocationCity");
            e.HasKey(t => t.Id).Property(t => t.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            e.Property(n => n.Name).HasColumnName("Name").HasMaxLength(250).IsRequired();
            e.HasMany(x => x.ProspectiveClient).WithOptional(x => x.LocationCity).HasForeignKey(x => x.LocationCityId);
        }
        #endregion configLocationCity
        #region configCivilState
        public void configCivilState(DbModelBuilder modelBuilder)
        {
            // Tabla CivilStatus(EstadoCivil)
            EntityTypeConfiguration<CivilStatus> e = modelBuilder.Entity<CivilStatus>();
            e.ToTable("CivilStatus");
            e.HasKey(t => t.Id).Property(t => t.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            e.Property(n => n.Name).HasColumnName("Name").HasMaxLength(250).IsRequired();
        }
        #endregion configCivilState
        #region configProspectiveClient
        public void configProspectiveClient(DbModelBuilder modelBuilder)
        {
            // Tabla ProspectiveClient(Prospecto)
            EntityTypeConfiguration<ProspectiveClient> e = modelBuilder.Entity<ProspectiveClient>();
            e.ToTable("ProspectiveClient");
            e.HasKey(x => x.Id).Property(x => x.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            e.Property(x => x.FirstName).HasColumnName("FirstName").HasMaxLength(250).IsRequired();
            e.Property(x => x.SecondName).HasColumnName("SecondName").HasMaxLength(250).IsOptional();
            e.Property(x => x.LastName1).HasColumnName("LastName1").HasMaxLength(250).IsRequired();
            e.Property(x => x.LastName2).HasColumnName("LastName2").HasMaxLength(250).IsOptional();
            e.Property(x => x.DateCreation).HasColumnName("DateCreation").HasColumnType("Date").IsOptional();
            e.Property(x => x.BirthDate).HasColumnName("BirthDate").HasColumnType("Date").IsOptional();
            e.Property(x => x.Telephone1).HasColumnName("Telephone1").HasMaxLength(20).IsRequired();
            e.Property(x => x.Telephone2).HasColumnName("Telephone2").HasMaxLength(20).IsOptional();
            e.Property(x => x.Email1).HasColumnName("Email1").HasMaxLength(250).IsRequired();
            e.Property(x => x.Email2).HasColumnName("Email2").HasMaxLength(250).IsOptional();
            e.Property(x => x.CURP).HasColumnName("CURP").HasMaxLength(20).IsOptional();
            e.Property(x => x.RFC).HasColumnName("RFC").HasMaxLength(15).IsOptional();
            e.Property(x => x.PropertyValue).HasColumnName("PropertyValue").HasColumnType("money").HasPrecision(19, 4).IsOptional();
            e.Property(x => x.Deposit).HasColumnName("Deposit").HasColumnType("money").HasPrecision(19, 4).IsOptional();
            e.Property(x => x.RequestedAmount).HasColumnName("RequestedAmount").HasColumnType("money").HasPrecision(19, 4).IsOptional();
            //
            e.HasOptional(x => x.ProspectiveClientDetail).WithRequired(x => x.ProspectiveClient);
            e.HasMany(x => x.ProspectiveClientHistory).WithRequired(x => x.ProspectiveClient).HasForeignKey(x => x.ProspectiveClientId).WillCascadeOnDelete(false);
        }
        #endregion configProspectiveClient
        #region configProspectiveClientDetail
        public void configProspectiveClientDetail(DbModelBuilder modelBuilder)
        {
            // Tabla ProspectiveClient(Prospecto)
            EntityTypeConfiguration<ProspectiveClientDetail> e = modelBuilder.Entity<ProspectiveClientDetail>();
            e.ToTable("ProspectiveClientDetail");
            e.HasKey(x => x.ProspectiveClientId).Property(x => x.ProspectiveClientId).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            e.Property(x => x.NSS).HasColumnName("NSS").HasMaxLength(15).IsOptional();
            e.Property(x => x.MonthlySalary1).HasColumnName("MonthlySalary1").HasColumnType("money").HasPrecision(19, 4).IsOptional();
            e.Property(x => x.Antiquity1).HasColumnName("Antiquity1").IsOptional();
            e.Property(x => x.MonthlySalary2).HasColumnName("MonthlySalary2").HasColumnType("money").HasPrecision(19, 4).IsOptional();
            e.Property(x => x.Antiquity2).HasColumnName("Antiquity2").IsOptional();
            e.Property(x => x.IsActiveSAT).HasColumnName("IsActiveSAT").IsOptional();
            e.Property(x => x.Activiy).HasColumnName("Activiy").HasMaxLength(250).IsOptional();
            e.Property(x => x.MoneyReceipt).HasColumnName("MoneyReceipt").HasMaxLength(250).IsOptional();
            e.Property(x => x.IsWorngCreditBureau).HasColumnName("IsWorngCreditBureau").IsOptional();
            e.Property(x => x.DebtMonthly).HasColumnName("DebtMonthly").HasColumnType("money").HasPrecision(19, 4).IsOptional();
            e.Property(x => x.ProblemType).HasColumnName("ProblemType").HasMaxLength(250).IsOptional();
            e.Property(x => x.IsHasCreditHistory).HasColumnName("IsHasCreditHistory").IsOptional();
            e.Property(x => x.Observations).HasColumnName("Observations").HasMaxLength(500).IsOptional();
        }
        #endregion configProspectiveClientDetail
        #region configProspectiveClientHistory
        public void configProspectiveClientHistory(DbModelBuilder modelBuilder)
        {
            // Tabla ProspectiveClient(Prospecto)
            EntityTypeConfiguration<ProspectiveClientHistory> e = modelBuilder.Entity<ProspectiveClientHistory>();
            e.ToTable("ProspectiveClientHistory");
            e.HasKey(x => x.Id).Property(x => x.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            e.Property(x => x.Date).HasColumnName("Date").HasColumnType("Date").IsRequired();
            e.Property(x => x.Commentary).HasColumnName("Commentary").HasMaxLength(500).IsOptional();
        }
        #endregion configProspectiveClientHistory
    }
}