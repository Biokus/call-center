﻿using DAL.Log;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace DAL.CallCenter
{
    public sealed class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        #region fields
        private Model model = null;
        #endregion fields
        #region properties
        public Model DbModel { get { return this.model; } }
        private DbSet<TEntity> EntitySet { get { return this.model.Set<TEntity>(); } }
        #endregion properties
        #region constructors & destructor
        public Repository()
        {
            this.model = new Model();
        }
        public Repository(Model model)
        {
            this.model = model;
        }
        #endregion constructors & destructor
        #region Dispose
        public void Dispose()
        {
            if (this.model != null) { this.model.Dispose(); }
        }
        #endregion Dispose
        #region Create
        public TEntity Create(TEntity entity)
        {
            TEntity result = null;
            try
            {
                this.EntitySet.Add(entity);
                Int32 r = this.model.SaveChanges();
                result = entity;
            }
            catch (Exception ex)
            {
                LogError.Add(Guid.Empty, DateTime.Now, @"Create", ex);
            }
            return result;
        }
        #endregion Create
        #region Read
        public IList<TEntity> Read(Expression<Func<TEntity, Boolean>> predicate)
        {
            IList<TEntity> result = null;
            try
            {
                result = this.EntitySet.Where(predicate).ToList();
            }
            catch (Exception ex)
            {
                LogError.Add(Guid.Empty, DateTime.Now, @"Read with predicate", ex);
            }
            return result;
        }
        public IList<TEntity> Read()
        {
            IList<TEntity> result = null;
            try
            {
                result = EntitySet.ToList();
            }
            catch (Exception ex)
            {
                LogError.Add(Guid.Empty, DateTime.Now, @"Read", ex);
            }
            return result;
        }
        #endregion Read
        #region Search
        public TEntity Search(Expression<Func<TEntity, Boolean>> predicate)
        {
            TEntity result = null;
            try
            {
                result = this.EntitySet.FirstOrDefault(predicate);
            }
            catch (Exception ex)
            {
                LogError.Add(Guid.Empty, DateTime.Now, @"Search", ex);
            }
            return result;
        }
        #endregion Search
        #region Update
        public Boolean Update(TEntity entity)
        {
            Boolean result = false;
            try
            {
                this.EntitySet.Attach(entity);
                this.model.Entry<TEntity>(entity).State = EntityState.Modified;
                result = (this.model.SaveChanges() > 0);
            }
            catch (Exception ex)
            {
                LogError.Add(Guid.Empty, DateTime.Now, @"Update", ex);
            }
            return result;
        }
        #endregion Update
        #region Delete
        public Boolean Delete(TEntity entity)
        {
            Boolean result = false;
            try
            {
                this.EntitySet.Attach(entity);
                this.EntitySet.Remove(entity);
                result = (this.model.SaveChanges() > 0);
            }
            catch (Exception ex)
            {
                LogError.Add(Guid.Empty, DateTime.Now, @"Delete", ex);
            }
            return result;
        }
        #endregion Delete
    }
}
