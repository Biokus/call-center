﻿using Domain.CallCenter;
using Domain.Common.Enums;
using Domain.Common.MethodParameter;
using Domain.Common.MethodResult;
using System;

namespace BLL.CallCenter.Test
{
    class Program
    {
        #region Main
        static void Main(String[] args)
        {
            using (Logic l = new Logic())
            {
                SocialSecurityTypes e = new SocialSecurityTypes(null, "Fovissste");
                ManageEntityParameter <SocialSecurityTypes> p = null;
                ManageEntityResult<SocialSecurityTypes> result = null;
                int opc = 0;
                if(opc == 1)
                {
                    p = new ManageEntityParameter<SocialSecurityTypes>(null,OperationType.Create, e);
                    result = l.manageSocialSecurityTypes(p);
                    Console.WriteLine("Nombre insertado: " + p.Entity.Name);
                    Console.ReadLine();
                }
                //ProspectusStatus e = new ProspectusStatus(null,"XX");
                //ManageEntityParameter<ProspectusStatus> p = null;
                //ManageEntityResult<ProspectusStatus> result = null;
                //int opc = 0;
                //if (opc ==1) //Crear
                //{
                //    p = new ManageEntityParameter<ProspectusStatus>(null, OperationType.Create,e);
                //    result = l.manageProspectusStatus(p);
                //    Console.WriteLine("Nombre insertado: "+ p.Entity.Name);
                //    Console.ReadLine();
                //}
                //else if(opc == 2)// Read
                //{
                //    e = new ProspectusStatus();
                //    e.Id = 1;
                //    p = new ManageEntityParameter<ProspectusStatus>(null,OperationType.SpecificRead,e);
                //    result = l.manageProspectusStatus(p);
                //    foreach (var i in result.Items)
                //    {
                //        Console.WriteLine("Nombres: "+i.Name);
                //    }
                //    Console.ReadLine();
                //}
                //else if(opc == 3)
                //{
                //    p = new ManageEntityParameter<ProspectusStatus>(null,OperationType.Update,e);
                //    result = l.manageProspectusStatus(p);
                //    foreach (var i in result.Items)
                //    {
                //        Console.WriteLine("Nombres: " + i.Name);
                //    }
                //    Console.ReadLine();
                //}else if (opc == 4)
                //{
                //    e = new ProspectusStatus();
                //    e.Id = 13;
                //    p = new ManageEntityParameter<ProspectusStatus>(null,OperationType.Delete,e);
                //    result = l.manageProspectusStatus(p);
                //    foreach (var i in result.Items)
                //    {
                //        Console.WriteLine("Nombre Eliminado: " + i.Name+ " Id Eliminado: "+ i.Id);
                //    }
                //    Console.ReadLine();
                //}

                //AdvertisingCampaign e = new AdvertisingCampaign(null, @"Campaña 003", new DateTime(2016, 10, 2), DateTime.Now, @"Erwin Gomez", @"contacto@biokus.com", @"5529558341");
                //ManageEntityParameter<AdvertisingCampaign> p = null;
                //ManageEntityResult<AdvertisingCampaign> result = null;
                //Int32 opc = 3;
                //if (opc == 1) // Create
                //{
                //    p = new ManageEntityParameter<AdvertisingCampaign>(null, OperationType.Create, e);
                //    result = l.manageAdvertisingCampaign(p);
                //}
                //else if (opc == 2) // Read
                //{
                //    p = new ManageEntityParameter<AdvertisingCampaign>(null, OperationType.Read, null);
                //    result = l.manageAdvertisingCampaign(p);
                //}
                //else if (opc == 3) // Specific Read
                //{
                //    e = new AdvertisingCampaign();
                //    e.Id = 2;
                //    p = new ManageEntityParameter<AdvertisingCampaign>(null, OperationType.SpecificRead, e);
                //    result = l.manageAdvertisingCampaign(p);
                //}
                //else if (opc == 4) // Update
                //{
                //    e = new AdvertisingCampaign(1, @"Campaña 001 Mod", new DateTime(2016, 8, 1), DateTime.Now, @"Erwin Gomez", @"contacto@biokus.com", @"5529558341");
                //    p = new ManageEntityParameter<AdvertisingCampaign>(null, OperationType.Update, e);
                //    result = l.manageAdvertisingCampaign(p);
                //}
                //else if (opc == 5) // Delete
                //{
                //    e = new AdvertisingCampaign();
                //    e.Id = 1;
                //    p = new ManageEntityParameter<AdvertisingCampaign>(null, OperationType.Delete, e);
                //    result = l.manageAdvertisingCampaign(p);
                //}
            }
        }
        #endregion Main
    }
}
