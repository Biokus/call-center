﻿using DAL.CallCenter;
using Domain.CallCenter;
using Domain.CallCenter.DTO;
using Domain.Common.Control;
using Domain.Common.Enums;
using Domain.Common.GlobalConstants;
using Domain.Common.MethodParameter;
using Domain.Common.MethodResult;
using Domain.Security;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Linq;

namespace BLL.CallCenter
{
    public class Logic : IDisposable
    {
        #region fields
        private Boolean _disposed;
        //
        private String _uecInvalidOperationType = ValidationConst.uecInvalidOperationType;
        private String _xemInvalidOperationType = ValidationConst.xemInvalidOperationType;
        //
        private String _remDbEntityValidationException = ValidationConst.remDbEntityValidationException;
        private String _uecDbEntityValidationException = ValidationConst.uecDbEntityValidationException;
        //
        private String _uecDbUpdateException = ValidationConst.uecDbUpdateException;
        private String _uemDbUpdateException = ValidationConst.uemDbUpdateException;
        //
        private String _uecSqlException = ValidationConst.uecSqlException;
        private String _uemSqlException = ValidationConst.uemSqlException;
        //
        private String _uecInvalidOperationException = ValidationConst.uecInvalidOperationException;
        private String _uemInvalidOperationException = ValidationConst.uemInvalidOperationException;
        //
        private String _uecException = ValidationConst.uecException;
        private String _uemException = ValidationConst.uemException;
        #endregion fields
        #region properties
        #endregion properties
        #region constructors & destructor
        public Logic()
        {

        }
        #endregion constructors & destructor
        #region Dispose
        public void Dispose()
        {
            Dispose(true);
            // Use SupressFinalize in case a subclass of this type implements a finalizer.
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(Boolean disposing)
        {
            // If you need thread safety, use a lock around these operations, as well as in your methods that use the resource.
            if (!this._disposed)
            {
                if (disposing)
                {
                    //if (this._resource != null) this._resource.Dispose();
                }
                // Indicate that the instance has been disposed.
                //this._resource = null;
                this._disposed = true;
            }
        }
        #endregion Dispose
        // helpers
        #region getDisplayName
        public static String getDisplayName(Type dataType, String fieldName)
        {
            // First look into attributes on a type and it's parents
            DisplayNameAttribute attr;
            attr = (DisplayNameAttribute)dataType.GetProperty(fieldName).GetCustomAttributes(typeof(DisplayNameAttribute), true).SingleOrDefault();

            // Look for [MetadataType] attribute in type hierarchy
            if (attr == null)
            {
                MetadataTypeAttribute metadataType = (MetadataTypeAttribute)dataType.GetCustomAttributes(typeof(MetadataTypeAttribute), true).FirstOrDefault();
                if (metadataType != null)
                {
                    var property = metadataType.MetadataClassType.GetProperty(fieldName);
                    if (property != null)
                    {
                        attr = (DisplayNameAttribute)property.GetCustomAttributes(typeof(DisplayNameAttribute), true).SingleOrDefault();
                    }
                }
            }
            return (attr != null) ? attr.DisplayName : String.Empty;
        }
        #endregion getDisplayName
        #region getValidationErrors
        private String getValidationErrors(DbEntityValidationException ex)
        {
            String uem = String.Empty;

            foreach (var eve in ex.EntityValidationErrors)
            {
                foreach (var ve in eve.ValidationErrors)
                {
                    var displayName = String.Empty;
                    if (ve.PropertyName != null)
                    {
                        displayName = getDisplayName(eve.Entry.Entity.GetType(), ve.PropertyName) + @" ";
                    }
                    uem += displayName + ve.ErrorMessage + @"|";
                }
            }
            uem = uem.Substring(0, uem.Length - 1);

            return uem;
        }
        #endregion getValidationErrors
        #region setExecutionError
        private ExecutionResult setExecutionError(String rem, String uec1, String uec2, String uem)
        {
            ExecutionResult result = new ExecutionResult();
            result.setValueError(rem, uec1 + @"." + uec2, uem);
            return result;
        }
        #endregion setExecutionError
        #region setExecutionInvalidOperationType
        private ExecutionResult setExecutionInvalidOperationType(String uec)
        {
            return this.setExecutionError(this._xemInvalidOperationType, uec, this._uecInvalidOperationType, this._xemInvalidOperationType);
        }
        #endregion setExecutionInvalidOperationType
        #region setExecutionDbEntityValidationException
        private ExecutionResult setExecutionDbEntityValidationException(DbEntityValidationException ex, String uec)
        {
            return this.setExecutionError(this._remDbEntityValidationException, uec, this._uecDbEntityValidationException, this.getValidationErrors(ex));
        }
        #endregion setExecutionDbEntityValidationException
        #region setExecutionDbUpdateException
        private ExecutionResult setExecutionDbUpdateException(DbUpdateException ex, String uec)
        {
            return this.setExecutionError(ex.ToString(), uec, this._uecDbUpdateException, this._uemDbUpdateException);
        }
        #endregion setExecutionDbUpdateException
        #region setExecutionSqlException
        private ExecutionResult setExecutionSqlException(SqlException ex, String uec)
        {
            // ??? Falta estructurar el mensaje de error del sistema
            return this.setExecutionError(ex.ToString(), uec, this._uecSqlException, this._uemSqlException);
        }
        #endregion setExecutionSqlException
        #region setExecutionInvalidOperationException
        private ExecutionResult setExecutionInvalidOperationException(InvalidOperationException ex, String uec)
        {
            return this.setExecutionError(ex.ToString(), uec, this._uecInvalidOperationException, this._uemInvalidOperationException);
        }
        #endregion setExecutionInvalidOperationException
        #region setExecutionException
        private ExecutionResult setExecutionException(Exception ex, String uec)
        {
            return this.setExecutionError(ex.ToString(), uec, this._uecException, this._uemException);
        }
        #endregion setExecutionException

        #region getAdvertisingCampaignById
        private AdvertisingCampaign getAdvertisingCampaignById(Repository<AdvertisingCampaign> r, Nullable<Int32> id)
        {
            AdvertisingCampaign result = null;
            result = r.Search(x => x.Id == id);
            return result;
        }
        #endregion getAdvertisingCampaignById
        #region getAdvertisingCampaignByName
        private AdvertisingCampaign getAdvertisingCampaignByName(OperationType op, Repository<AdvertisingCampaign> r, String name, Nullable<Int32> id = null)
        {
            AdvertisingCampaign result = null;
            if (op == OperationType.Create)
            {
                result = r.Search(x => x.Name == name);
            }
            else if (op == OperationType.Update)
            {
                result = r.Search(x => x.Name == name && x.Id != id);
            }
            return result;
        }
        #endregion getAdvertisingCampaignByName
        #region manageAdvertisingCampaign
        public ManageEntityResult<AdvertisingCampaign> manageAdvertisingCampaign(ManageEntityParameter<AdvertisingCampaign> p)
        {
            ManageEntityResult<AdvertisingCampaign> result = new ManageEntityResult<AdvertisingCampaign>();
            String uec = @"01";
            #region Validation of parameters
            if (p == null) p = new ManageEntityParameter<AdvertisingCampaign>();
            else
            {
                if (p.Token == null) p.Token = Guid.Empty;
                if (p.OperationType == null) p.OperationType = OperationType.Indefined;
                if (p.Entity == null) p.Entity = new AdvertisingCampaign();
            }
            #endregion Validation of parameters
            #region Operation execution
            String ExistingName = @"Nombre de Campaña existente";
            String ExistingId = @"Identificador de Campaña inválido";
            String ErrorUpdate = @"Error al actualizar la Campaña";
            String ErrorDelete = @"Error al eliminar la Campaña";
            Repository<AdvertisingCampaign> r = null;
            DbContextTransaction t = null;
            try
            {
                r = new Repository<AdvertisingCampaign>();
                t = r.DbModel.Database.BeginTransaction();
                if (p.OperationType == OperationType.Read)
                {
                    result.setValueOk(r.Read().ToList());
                }
                else if (p.OperationType == OperationType.SpecificRead)
                {
                    result.setValueOk(r.Read(x => x.Id == p.Entity.Id).ToList());
                }
                else if (p.OperationType == OperationType.Create)
                {
                    // Búsqueda del elemento por 'Nombre'
                    AdvertisingCampaign e = this.getAdvertisingCampaignByName(p.OperationType.Value, r, p.Entity.Name);
                    if (e == null)
                    {
                        // Elemento no existe: Inserción física
                        result.setValueOk(r.Create(p.Entity));
                    }
                    else
                    {
                        // Elemento existente
                        result.setValue(this.setExecutionError(ExistingName, uec, @"01", ExistingName));
                    }
                }
                else if (p.OperationType == OperationType.Update || p.OperationType == OperationType.Delete)
                {
                    // Búsqueda del elemento por 'Id'
                    AdvertisingCampaign e1 = this.getAdvertisingCampaignById(r, p.Entity.Id);
                    if (e1 == null)
                    {
                        result.setValue(this.setExecutionError(ExistingId, uec, @"02", ExistingId));
                    }
                    else
                    {
                        if (p.OperationType == OperationType.Update)
                        {
                            // Búsqueda del elemento por 'Nombre'
                            AdvertisingCampaign e2 = this.getAdvertisingCampaignByName(p.OperationType.Value, r, p.Entity.Name, p.Entity.Id);
                            if (e2 != null)
                            {
                                result.setValue(this.setExecutionError(ExistingName, uec, @"03", ExistingName));
                            }
                            else
                            {
                                e1.updateValue(p.Entity);
                                Boolean s = r.Update(e1);
                                if (s)
                                    result.setValueOk(e1);
                                else
                                    result.setValue(this.setExecutionError(ErrorUpdate, uec, @"04", ErrorUpdate));
                            }
                        }
                        else if (p.OperationType == OperationType.Delete)
                        {
                            // Validación de referencias a otras entidades ???
                            e1.clearValues();
                            Boolean s = r.Delete(e1);
                            if (s)
                                result.setValueOk(e1);
                            else
                                result.setValue(this.setExecutionError(ErrorDelete, uec, @"05", ErrorDelete));
                        }
                    }
                }
                else
                {
                    result.setValue(this.setExecutionInvalidOperationType(uec));
                }
                t.Commit();
            }
            catch (DbEntityValidationException ex)
            {
                result.setValue(this.setExecutionDbEntityValidationException(ex, uec));
                if (t != null) t.Rollback();
            }
            catch (DbUpdateException ex)
            {
                result.setValue(this.setExecutionDbUpdateException(ex, uec));
                if (t != null) t.Rollback();
            }
            catch (SqlException ex)
            {
                result.setValue(this.setExecutionSqlException(ex, uec));
                if (t != null) t.Rollback();
            }
            catch (InvalidOperationException ex)
            {
                result.setValue(this.setExecutionInvalidOperationException(ex, uec));
                if (t != null) t.Rollback();
            }
            catch (Exception ex)
            {
                result.setValue(this.setExecutionException(ex, uec));
                if (t != null) t.Rollback();
            }
            finally
            {
                if (t != null) t.Dispose();
                if (r != null) r.Dispose();
            }
            #endregion Operation execution
            return result;
        }
        #endregion manageAdvertisingCampaign

        #region getProspectiveClientStatusById
        private ProspectiveClientStatus getProspectiveClientStatusById(Repository<ProspectiveClientStatus> r, Nullable<Int32> id)
        {
            ProspectiveClientStatus result = null;
            result = r.Search(x => x.Id == id);
            return result;
        }
        #endregion getProspectiveClientStatusById 
        #region getProspectiveClientStatusByName
        private ProspectiveClientStatus getProspectiveClientStatusByName(OperationType op, Repository<ProspectiveClientStatus> r, String name, Nullable<Int32> id = null)
        {
            ProspectiveClientStatus result = null;
            if (op == OperationType.Create)
            {
                result = r.Search(x => x.Name == name);
            }
            else if (op == OperationType.Update)
            {
                result = r.Search(x => x.Name == name && x.Id != id);
            }
            return result;
        }
        #endregion getProspectiveClientStatusByName
        #region manageProspectiveClientStatus
        public ManageEntityResult<ProspectiveClientStatus> manageProspectiveClientStatus(ManageEntityParameter<ProspectiveClientStatus> p)
        {
            ManageEntityResult<ProspectiveClientStatus> result = new ManageEntityResult<ProspectiveClientStatus>();
            String uec = @"02";
            #region Validation of parameters
            if (p == null) p = new ManageEntityParameter<ProspectiveClientStatus>();
            else
            {
                if (p.Token == null) p.Token = Guid.Empty;
                if (p.OperationType == null) p.OperationType = OperationType.Indefined;
                if (p.Entity == null) p.Entity = new ProspectiveClientStatus();
            }
            #endregion Validation of parameters
            #region Operation execution
            String ExistingName = @"Nombre de Estatus existente";
            String ExistingId = @"Identificador de Estatus inválido";
            String ErrorUpdate = @"Error al actualizar la Estatus";
            String ErrorDelete = @"Error al eliminar la Estatus";
            Repository<ProspectiveClientStatus> r = null;
            DbContextTransaction t = null;
            try
            {
                r = new Repository<ProspectiveClientStatus>();
                t = r.DbModel.Database.BeginTransaction();
                if (p.OperationType == OperationType.Read)
                {
                    result.setValueOk(r.Read().ToList());
                }
                else if (p.OperationType == OperationType.SpecificRead)
                {
                    result.setValueOk(r.Read(x => x.Id == p.Entity.Id || x.Name == p.Entity.Name).ToList());
                }
                else if (p.OperationType == OperationType.Create)
                {
                    // Búsqueda del elemento por 'Nombre'
                    ProspectiveClientStatus e = this.getProspectiveClientStatusByName(p.OperationType.Value, r, p.Entity.Name);
                    if (e == null)
                    {
                        // Elemento no existe: Inserción física
                        result.setValueOk(r.Create(p.Entity));
                    }
                    else
                    {
                        // Elemento existente
                        result.setValue(this.setExecutionError(ExistingName, uec, @"01", ExistingName));
                    }
                }
                else if (p.OperationType == OperationType.Update || p.OperationType == OperationType.Delete)
                {
                    // Búsqueda del elemento por 'Id'
                    ProspectiveClientStatus e1 = this.getProspectiveClientStatusById(r, p.Entity.Id);
                    if (e1 == null)
                    {
                        result.setValue(this.setExecutionError(ExistingId, uec, @"02", ExistingId));
                    }
                    else
                    {
                        if (p.OperationType == OperationType.Update)
                        {
                            // Búsqueda del elemento por 'Nombre'
                            ProspectiveClientStatus e2 = this.getProspectiveClientStatusByName(p.OperationType.Value, r, p.Entity.Name, p.Entity.Id);
                            if (e2 != null)
                            {
                                result.setValue(this.setExecutionError(ExistingName, uec, @"03", ExistingName));
                            }
                            else
                            {
                                e1.updateValue(p.Entity);
                                Boolean s = r.Update(e1);
                                if (s)
                                    result.setValueOk(e1);
                                else
                                    result.setValue(this.setExecutionError(ErrorUpdate, uec, @"04", ErrorUpdate));
                            }
                        }
                        else if (p.OperationType == OperationType.Delete)
                        {
                            // Validación de referencias a otras entidades ???
                            e1.clearValues();
                            Boolean s = r.Delete(e1);
                            if (s)
                                result.setValueOk(e1);
                            else
                                result.setValue(this.setExecutionError(ErrorDelete, uec, @"05", ErrorDelete));
                        }
                    }
                }
                else
                {
                    result.setValue(this.setExecutionInvalidOperationType(uec));
                }
                t.Commit();
            }
            catch (DbEntityValidationException ex)
            {
                result.setValue(this.setExecutionDbEntityValidationException(ex, uec));
                if (t != null) t.Rollback();
            }
            catch (DbUpdateException ex)
            {
                result.setValue(this.setExecutionDbUpdateException(ex, uec));
                if (t != null) t.Rollback();
            }
            catch (SqlException ex)
            {
                result.setValue(this.setExecutionSqlException(ex, uec));
                if (t != null) t.Rollback();
            }
            catch (InvalidOperationException ex)
            {
                result.setValue(this.setExecutionInvalidOperationException(ex, uec));
                if (t != null) t.Rollback();
            }
            catch (Exception ex)
            {
                result.setValue(this.setExecutionException(ex, uec));
                if (t != null) t.Rollback();
            }
            finally
            {
                if (t != null) t.Dispose();
                if (r != null) r.Dispose();
            }
            #endregion Operation execution
            return result;
        }
        #endregion manageProspectiveClientStatus

        #region getSocialSecurityTypesById
        private SocialSecurityTypes getSocialSecurityTypesById(Repository<SocialSecurityTypes> r, Nullable<Int32> id)
        {
            SocialSecurityTypes result = null;
            result = r.Search(x => x.Id == id);
            return result;
        }
        #endregion getSocialSecurityTypesById
        #region getSocialSecurityTypesByName
        private SocialSecurityTypes getSocialSecurityTypesByName(OperationType op, Repository<SocialSecurityTypes> r, String name, Nullable<Int32> id = null)
        {
            SocialSecurityTypes result = null;
            if (op == OperationType.Create)
            {
                result = r.Search(x => x.Name == name);
            }
            else if (op == OperationType.Update)
            {
                result = r.Search(x => x.Name == name && x.Id != id);
            }
            return result;
        }
        #endregion getSocialSecurityTypesByName
        #region manageSocialSecurityTypes
        public ManageEntityResult<SocialSecurityTypes> manageSocialSecurityTypes(ManageEntityParameter<SocialSecurityTypes> p)
        {
            ManageEntityResult<SocialSecurityTypes> result = new ManageEntityResult<SocialSecurityTypes>();
            String uec = @"03";
            #region Validation of parameters
            if (p == null) p = new ManageEntityParameter<SocialSecurityTypes>();
            else
            {
                if (p.Token == null) p.Token = Guid.Empty;
                if (p.OperationType == null) p.OperationType = OperationType.Indefined;
                if (p.Entity == null) p.Entity = new SocialSecurityTypes();
            }
            #endregion Validation of parameters
            #region Operation execution
            String ExistingName = @"Nombre de Seguro Social existente";
            String ExistingId = @"Identificador de Seguro Social inválido";
            String ErrorUpdate = @"Error al actualizar el Seguro Social";
            String ErrorDelete = @"Error al eliminar el Seguro Social";
            Repository<SocialSecurityTypes> r = null;
            DbContextTransaction t = null;
            try
            {
                r = new Repository<SocialSecurityTypes>();
                t = r.DbModel.Database.BeginTransaction();
                if (p.OperationType == OperationType.Read)
                {
                    result.setValueOk(r.Read().ToList());
                }
                else if (p.OperationType == OperationType.SpecificRead)
                {
                    result.setValueOk(r.Read(x => x.Id == p.Entity.Id || x.Name == p.Entity.Name).ToList());
                }
                else if (p.OperationType == OperationType.Create)
                {
                    // Búsqueda del elemento por 'Nombre'
                    SocialSecurityTypes e = this.getSocialSecurityTypesByName(p.OperationType.Value, r, p.Entity.Name);
                    if (e == null)
                    {
                        // Elemento no existe: Inserción física
                        result.setValueOk(r.Create(p.Entity));
                    }
                    else
                    {
                        // Elemento existente
                        result.setValue(this.setExecutionError(ExistingName, uec, @"01", ExistingName));
                    }
                }
                else if (p.OperationType == OperationType.Update || p.OperationType == OperationType.Delete)
                {
                    // Búsqueda del elemento por 'Id'
                    SocialSecurityTypes e1 = this.getSocialSecurityTypesById(r, p.Entity.Id);
                    if (e1 == null)
                    {
                        result.setValue(this.setExecutionError(ExistingId, uec, @"02", ExistingId));
                    }
                    else
                    {
                        if (p.OperationType == OperationType.Update)
                        {
                            // Búsqueda del elemento por 'Nombre'
                            SocialSecurityTypes e2 = this.getSocialSecurityTypesByName(p.OperationType.Value, r, p.Entity.Name, p.Entity.Id);
                            if (e2 != null)
                            {
                                result.setValue(this.setExecutionError(ExistingName, uec, @"03", ExistingName));
                            }
                            else
                            {
                                e1.updateValue(p.Entity);
                                Boolean s = r.Update(e1);
                                if (s)
                                    result.setValueOk(e1);
                                else
                                    result.setValue(this.setExecutionError(ErrorUpdate, uec, @"04", ErrorUpdate));
                            }
                        }
                        else if (p.OperationType == OperationType.Delete)
                        {
                            // Validación de referencias a otras entidades ???
                            e1.clearValues();
                            Boolean s = r.Delete(e1);
                            if (s)
                                result.setValueOk(e1);
                            else
                                result.setValue(this.setExecutionError(ErrorDelete, uec, @"05", ErrorDelete));
                        }
                    }
                }
                else
                {
                    result.setValue(this.setExecutionInvalidOperationType(uec));
                }
                t.Commit();
            }
            catch (DbEntityValidationException ex)
            {
                result.setValue(this.setExecutionDbEntityValidationException(ex, uec));
                if (t != null) t.Rollback();
            }
            catch (DbUpdateException ex)
            {
                result.setValue(this.setExecutionDbUpdateException(ex, uec));
                if (t != null) t.Rollback();
            }
            catch (SqlException ex)
            {
                result.setValue(this.setExecutionSqlException(ex, uec));
                if (t != null) t.Rollback();
            }
            catch (InvalidOperationException ex)
            {
                result.setValue(this.setExecutionInvalidOperationException(ex, uec));
                if (t != null) t.Rollback();
            }
            catch (Exception ex)
            {
                result.setValue(this.setExecutionException(ex, uec));
                if (t != null) t.Rollback();
            }
            finally
            {
                if (t != null) t.Dispose();
                if (r != null) r.Dispose();
            }
            #endregion Operation execution
            return result;
        }
        #endregion manageSocialSecurityTypes

        #region getEmployeeById
        private Employee getEmployeeById(Repository<Employee> r, Nullable<Int32> id)
        {
            Employee result = null;
            result = r.Search(x => x.Id == id);
            return result;
        }
        #endregion getEmployeeById
        #region getEmployeeByName
        private Employee getEmployeeByName(OperationType op, Repository<Employee> r, String curp, Nullable<Int32> id = null)
        {
            Employee result = null;
            if (op == OperationType.Create)
            {
                result = r.Search(x => x.CURP == curp);
            }
            else if (op == OperationType.Update)
            {
                result = r.Search(x => x.CURP == curp && x.Id != id);
            }
            return result;
        }
        #endregion getEmployeeByName
        #region manageEmployee
        public ManageEntityResult<Employee> manageEmployee(ManageEntityParameter<Employee> p)
        {
            ManageEntityResult<Employee> result = new ManageEntityResult<Employee>();
            String uec = @"04";
            #region Validation of parameters
            if (p == null) p = new ManageEntityParameter<Employee>();
            else
            {
                if (p.Token == null) p.Token = Guid.Empty;
                if (p.OperationType == null) p.OperationType = OperationType.Indefined;
                if (p.Entity == null) p.Entity = new Employee();
            }
            #endregion Validation of parameters
            #region Operation execution
            String ExistingName = @"CURP ya existente";
            String ExistingId = @"Identificador de Empleado inválido";
            String ErrorUpdate = @"Error al actualizar el Empleado";
            String ErrorDelete = @"Error al eliminar el Empleado";
            Repository<Employee> r = null;
            DbContextTransaction t = null;
            try
            {
                r = new Repository<Employee>();
                t = r.DbModel.Database.BeginTransaction();
                if (p.OperationType == OperationType.Read)
                {
                    result.setValueOk(r.Read().ToList());
                }
                else if (p.OperationType == OperationType.SpecificRead)
                {
                    result.setValueOk(r.Read(x => x.Id == p.Entity.Id || x.CURP == p.Entity.CURP).ToList());
                }
                else if (p.OperationType == OperationType.Create)
                {
                    // Búsqueda del elemento por 'Nombre'
                    Employee e = this.getEmployeeByName(p.OperationType.Value, r, p.Entity.CURP);
                    if (e == null)
                    {
                        // Elemento no existe: Inserción física
                        result.setValueOk(r.Create(p.Entity));
                    }
                    else
                    {
                        // Elemento existente
                        result.setValue(this.setExecutionError(ExistingName, uec, @"01", ExistingName));
                    }
                }
                else if (p.OperationType == OperationType.Update || p.OperationType == OperationType.Delete)
                {
                    // Búsqueda del elemento por 'Id'
                    Employee e1 = this.getEmployeeById(r, p.Entity.Id);
                    if (e1 == null)
                    {
                        result.setValue(this.setExecutionError(ExistingId, uec, @"02", ExistingId));
                    }
                    else
                    {
                        if (p.OperationType == OperationType.Update)
                        {
                            // Búsqueda del elemento por 'Nombre'
                            Employee e2 = this.getEmployeeByName(p.OperationType.Value, r, p.Entity.CURP, p.Entity.Id);
                            if (e2 != null)
                            {
                                result.setValue(this.setExecutionError(ExistingName, uec, @"03", ExistingName));
                            }
                            else
                            {
                                e1.updateValue(p.Entity);
                                Boolean s = r.Update(e1);
                                if (s)
                                    result.setValueOk(e1);
                                else
                                    result.setValue(this.setExecutionError(ErrorUpdate, uec, @"04", ErrorUpdate));
                            }
                        }
                        else if (p.OperationType == OperationType.Delete)
                        {
                            // Validación de referencias a otras entidades ???
                            e1.clearValues();
                            Boolean s = r.Delete(e1);
                            if (s)
                                result.setValueOk(e1);
                            else
                                result.setValue(this.setExecutionError(ErrorDelete, uec, @"05", ErrorDelete));
                        }
                    }
                }
                else
                {
                    result.setValue(this.setExecutionInvalidOperationType(uec));
                }
                t.Commit();
            }
            catch (DbEntityValidationException ex)
            {
                result.setValue(this.setExecutionDbEntityValidationException(ex, uec));
                if (t != null) t.Rollback();
            }
            catch (DbUpdateException ex)
            {
                result.setValue(this.setExecutionDbUpdateException(ex, uec));
                if (t != null) t.Rollback();
            }
            catch (SqlException ex)
            {
                result.setValue(this.setExecutionSqlException(ex, uec));
                if (t != null) t.Rollback();
            }
            catch (InvalidOperationException ex)
            {
                result.setValue(this.setExecutionInvalidOperationException(ex, uec));
                if (t != null) t.Rollback();
            }
            catch (Exception ex)
            {
                result.setValue(this.setExecutionException(ex, uec));
                if (t != null) t.Rollback();
            }
            finally
            {
                if (t != null) t.Dispose();
                if (r != null) r.Dispose();
            }
            #endregion Operation execution
            return result;
        }
        #endregion manageEmployee

        #region getProfileById
        private Profile getProfileById(Repository<Profile> r, Nullable<Int32> id)
        {
            Profile result = null;
            result = r.Search(x => x.Id == id);
            return result;
        }
        #endregion getProfileById
        #region getProfileByName
        private Profile getProfileByName(OperationType op, Repository<Profile> r, String name, Nullable<Int32> id = null)
        {
            Profile result = null;
            if (op == OperationType.Create)
            {
                result = r.Search(x => x.Name == name);
            }
            else if (op == OperationType.Update)
            {
                result = r.Search(x => x.Name == name && x.Id != id);
            }
            return result;
        }
        #endregion getProfileByName
        #region manageProfile
        public ManageEntityResult<Profile> manageProfile(ManageEntityParameter<Profile> p)
        {
            ManageEntityResult<Profile> result = new ManageEntityResult<Profile>();
            String uec = @"05";
            #region Validation of parameters
            if (p == null) p = new ManageEntityParameter<Profile>();
            else
            {
                if (p.Token == null) p.Token = Guid.Empty;
                if (p.OperationType == null) p.OperationType = OperationType.Indefined;
                if (p.Entity == null) p.Entity = new Profile();
            }
            #endregion Validation of parameters
            #region Operation execution
            String ExistingName = @"Nombre del Perfil existente";
            String ExistingId = @"Identificador del Perfil inválido";
            String ErrorUpdate = @"Error al actualizar el Perfil";
            String ErrorDelete = @"Error al eliminar el Perfil";
            Repository<Profile> r = null;
            DbContextTransaction t = null;
            try
            {
                r = new Repository<Profile>();
                t = r.DbModel.Database.BeginTransaction();
                if (p.OperationType == OperationType.Read)
                {
                    result.setValueOk(r.Read().ToList());
                }
                else if (p.OperationType == OperationType.SpecificRead)
                {
                    result.setValueOk(r.Read(x => x.Id == p.Entity.Id || x.Name == p.Entity.Name).ToList());
                }
                else if (p.OperationType == OperationType.Create)
                {
                    // Búsqueda del elemento por 'Nombre'
                    Profile e = this.getProfileByName(p.OperationType.Value, r, p.Entity.Name);
                    if (e == null)
                    {
                        // Elemento no existe: Inserción física
                        result.setValueOk(r.Create(p.Entity));
                    }
                    else
                    {
                        // Elemento existente
                        result.setValue(this.setExecutionError(ExistingName, uec, @"01", ExistingName));
                    }
                }
                else if (p.OperationType == OperationType.Update || p.OperationType == OperationType.Delete)
                {
                    // Búsqueda del elemento por 'Id'
                    Profile e1 = this.getProfileById(r, p.Entity.Id);
                    if (e1 == null)
                    {
                        result.setValue(this.setExecutionError(ExistingId, uec, @"02", ExistingId));
                    }
                    else
                    {
                        if (p.OperationType == OperationType.Update)
                        {
                            // Búsqueda del elemento por 'Nombre'
                            Profile e2 = this.getProfileByName(p.OperationType.Value, r, p.Entity.Name, p.Entity.Id);
                            if (e2 != null)
                            {
                                result.setValue(this.setExecutionError(ExistingName, uec, @"03", ExistingName));
                            }
                            else
                            {
                                e1.updateValue(p.Entity);
                                Boolean s = r.Update(e1);
                                if (s)
                                    result.setValueOk(e1);
                                else
                                    result.setValue(this.setExecutionError(ErrorUpdate, uec, @"04", ErrorUpdate));
                            }
                        }
                        else if (p.OperationType == OperationType.Delete)
                        {
                            // Validación de referencias a otras entidades ???
                            e1.clearValues();
                            Boolean s = r.Delete(e1);
                            if (s)
                                result.setValueOk(e1);
                            else
                                result.setValue(this.setExecutionError(ErrorDelete, uec, @"05", ErrorDelete));
                        }
                    }
                }
                else
                {
                    result.setValue(this.setExecutionInvalidOperationType(uec));
                }
                t.Commit();
            }
            catch (DbEntityValidationException ex)
            {
                result.setValue(this.setExecutionDbEntityValidationException(ex, uec));
                if (t != null) t.Rollback();
            }
            catch (DbUpdateException ex)
            {
                result.setValue(this.setExecutionDbUpdateException(ex, uec));
                if (t != null) t.Rollback();
            }
            catch (SqlException ex)
            {
                result.setValue(this.setExecutionSqlException(ex, uec));
                if (t != null) t.Rollback();
            }
            catch (InvalidOperationException ex)
            {
                result.setValue(this.setExecutionInvalidOperationException(ex, uec));
                if (t != null) t.Rollback();
            }
            catch (Exception ex)
            {
                result.setValue(this.setExecutionException(ex, uec));
                if (t != null) t.Rollback();
            }
            finally
            {
                if (t != null) t.Dispose();
                if (r != null) r.Dispose();
            }
            #endregion Operation execution
            return result;
        }
        #endregion manageProfile

        #region getUserById
        private User getUserById(Repository<User> r, Nullable<Int32> id)
        {
            User result = null;
            result = r.Search(x => x.Id == id);
            return result;
        }
        #endregion getUserById
        #region getUserByName
        private User getUserByName(OperationType op, Repository<User> r, String userName, Nullable<Int32> id = null)
        {
            User result = null;
            if (op == OperationType.Create)
            {
                result = r.Search(x => x.UserName == userName);
            }
            else if (op == OperationType.Update)
            {
                result = r.Search(x => x.UserName == userName && x.Id != id);
            }
            return result;
        }
        #endregion getUserByName
        #region manageUser
        public ManageEntityResult<User> manageUser(ManageEntityParameter<User> p)
        {
            ManageEntityResult<User> result = new ManageEntityResult<User>();
            String uec = @"06";
            #region Validation of parameters
            if (p == null) p = new ManageEntityParameter<User>();
            else
            {
                if (p.Token == null) p.Token = Guid.Empty;
                if (p.OperationType == null) p.OperationType = OperationType.Indefined;
                if (p.Entity == null) p.Entity = new User();
            }
            #endregion Validation of parameters
            #region Operation execution
            String ExistingName = @"Nombre de Usuario existente";
            String ExistingId = @"Identificador de Usuario inválido";
            String ErrorUpdate = @"Error al actualizar el Usuario";
            String ErrorDelete = @"Error al eliminar el Usuario";
            Repository<User> r = null;
            DbContextTransaction t = null;
            try
            {
                r = new Repository<User>();
                t = r.DbModel.Database.BeginTransaction();
                if (p.OperationType == OperationType.Read)
                {
                    result.setValueOk(r.Read().ToList());
                }
                else if (p.OperationType == OperationType.SpecificRead)
                {
                    result.setValueOk(r.Read(x => x.Id == p.Entity.Id || x.UserName == p.Entity.UserName).ToList());
                }
                else if (p.OperationType == OperationType.Create)
                {
                    // Búsqueda del elemento por 'Nombre'
                    User e = this.getUserByName(p.OperationType.Value, r, p.Entity.UserName);
                    if (e == null)
                    {
                        // Elemento no existe: Inserción física
                        result.setValueOk(r.Create(p.Entity));
                    }
                    else
                    {
                        // Elemento existente
                        result.setValue(this.setExecutionError(ExistingName, uec, @"01", ExistingName));
                    }
                }
                else if (p.OperationType == OperationType.Update || p.OperationType == OperationType.Delete)
                {
                    // Búsqueda del elemento por 'Id'
                    User e1 = this.getUserById(r, p.Entity.Id);
                    if (e1 == null)
                    {
                        result.setValue(this.setExecutionError(ExistingId, uec, @"02", ExistingId));
                    }
                    else
                    {
                        if (p.OperationType == OperationType.Update)
                        {
                            // Búsqueda del elemento por 'Nombre'
                            User e2 = this.getUserByName(p.OperationType.Value, r, p.Entity.UserName, p.Entity.Id);
                            if (e2 != null)
                            {
                                result.setValue(this.setExecutionError(ExistingName, uec, @"03", ExistingName));
                            }
                            else
                            {
                                e1.updateValue(p.Entity);
                                Boolean s = r.Update(e1);
                                if (s)
                                    result.setValueOk(e1);
                                else
                                    result.setValue(this.setExecutionError(ErrorUpdate, uec, @"04", ErrorUpdate));
                            }
                        }
                        else if (p.OperationType == OperationType.Delete)
                        {
                            // Validación de referencias a otras entidades ???
                            e1.clearValues();
                            Boolean s = r.Delete(e1);
                            if (s)
                                result.setValueOk(e1);
                            else
                                result.setValue(this.setExecutionError(ErrorDelete, uec, @"05", ErrorDelete));
                        }
                    }
                }
                else
                {
                    result.setValue(this.setExecutionInvalidOperationType(uec));
                }
                t.Commit();
            }
            catch (DbEntityValidationException ex)
            {
                result.setValue(this.setExecutionDbEntityValidationException(ex, uec));
                if (t != null) t.Rollback();
            }
            catch (DbUpdateException ex)
            {
                result.setValue(this.setExecutionDbUpdateException(ex, uec));
                if (t != null) t.Rollback();
            }
            catch (SqlException ex)
            {
                result.setValue(this.setExecutionSqlException(ex, uec));
                if (t != null) t.Rollback();
            }
            catch (InvalidOperationException ex)
            {
                result.setValue(this.setExecutionInvalidOperationException(ex, uec));
                if (t != null) t.Rollback();
            }
            catch (Exception ex)
            {
                result.setValue(this.setExecutionException(ex, uec));
                if (t != null) t.Rollback();
            }
            finally
            {
                if (t != null) t.Dispose();
                if (r != null) r.Dispose();
            }
            #endregion Operation execution
            return result;
        }
        #endregion manageUser 

        #region getCreditTypeById
        private CreditType getCreditTypeById(Repository<CreditType> r, Nullable<Int32> id)
        {
            CreditType result = null;
            result = r.Search(x => x.Id == id);
            return result;
        }
        #endregion getCreditTypeById
        #region getCreditTypeByName
        private CreditType getCreditTypeByName(OperationType op, Repository<CreditType> r, String name, Nullable<Int32> id = null)
        {
            CreditType result = null;
            if (op == OperationType.Create)
            {
                result = r.Search(x => x.Name == name);
            }
            else if (op == OperationType.Update)
            {
                result = r.Search(x => x.Name == name && x.Id != id);
            }
            return result;
        }
        #endregion getCreditTypeByName
        #region manageCreditType
        public ManageEntityResult<CreditType> manageCreditType(ManageEntityParameter<CreditType> p)
        {
            ManageEntityResult<CreditType> result = new ManageEntityResult<CreditType>();
            String uec = @"07";
            #region Validation of parameter
            if (p == null) p = new ManageEntityParameter<CreditType>();
            else
            {
                if (p.Token == null) p.Token = Guid.Empty;
                if (p.OperationType == null) p.OperationType = OperationType.Indefined;
                if (p.Entity == null) p.Entity = new CreditType();
            }
            #endregion Validation of parameter
            #region Operation execution
            String ExistingName = @"Tipo de Credito existente";
            String ExistingId = @"Identificador del Tipo de Credito inválido";
            String ErrorUpdate = @"Error al actualizar el Tipo de Credito";
            String ErrorDelete = @"Error al eliminar el Tipo de Credito";
            Repository<CreditType> r = null;
            DbContextTransaction t = null;
            try
            {
                r = new Repository<CreditType>();
                t = r.DbModel.Database.BeginTransaction();
                if (p.OperationType == OperationType.Read)
                {
                    result.setValueOk(r.Read().ToList());
                }
                else if (p.OperationType == OperationType.SpecificRead)
                {
                    result.setValueOk(r.Read(x => x.Id == p.Entity.Id || x.Name == p.Entity.Name).ToList());
                }
                else if (p.OperationType == OperationType.Create)
                {
                    // Búsqueda del elemento por 'Nombre'
                    CreditType e = this.getCreditTypeByName(p.OperationType.Value, r, p.Entity.Name);
                    if (e == null)
                    {
                        // Elemento no existe: Inserción física
                        result.setValueOk(r.Create(p.Entity));
                    }
                    else
                    {
                        // Elemento existente
                        result.setValue(this.setExecutionError(ExistingName, uec, @"01", ExistingName));
                    }
                }
                else if (p.OperationType == OperationType.Update || p.OperationType == OperationType.Delete)
                {
                    // Búsqueda del elemento por 'Id'
                    CreditType e1 = this.getCreditTypeById(r, p.Entity.Id);
                    if (e1 == null)
                    {
                        result.setValue(this.setExecutionError(ExistingId, uec, @"02", ExistingId));
                    }
                    else
                    {
                        if (p.OperationType == OperationType.Update)
                        {
                            // Búsqueda del elemento por 'Nombre'
                            CreditType e2 = this.getCreditTypeByName(p.OperationType.Value, r, p.Entity.Name, p.Entity.Id);
                            if (e2 != null)
                            {
                                result.setValue(this.setExecutionError(ExistingName, uec, @"03", ExistingName));
                            }
                            else
                            {
                                e1.updateValue(p.Entity);
                                Boolean s = r.Update(e1);
                                if (s)
                                    result.setValueOk(e1);
                                else
                                    result.setValue(this.setExecutionError(ErrorUpdate, uec, @"04", ErrorUpdate));
                            }
                        }
                        else if (p.OperationType == OperationType.Delete)
                        {
                            // Validación de referencias a otras entidades ???
                            e1.clearValues();
                            Boolean s = r.Delete(e1);
                            if (s)
                                result.setValueOk(e1);
                            else
                                result.setValue(this.setExecutionError(ErrorDelete, uec, @"05", ErrorDelete));
                        }
                    }
                }
                else
                {
                    result.setValue(this.setExecutionInvalidOperationType(uec));
                }
                t.Commit();
            }
            catch (DbEntityValidationException ex)
            {
                result.setValue(this.setExecutionDbEntityValidationException(ex, uec));
                if (t != null) t.Rollback();
            }
            catch (DbUpdateException ex)
            {
                result.setValue(this.setExecutionDbUpdateException(ex, uec));
                if (t != null) t.Rollback();
            }
            catch (SqlException ex)
            {
                result.setValue(this.setExecutionSqlException(ex, uec));
                if (t != null) t.Rollback();
            }
            catch (InvalidOperationException ex)
            {
                result.setValue(this.setExecutionInvalidOperationException(ex, uec));
                if (t != null) t.Rollback();
            }
            catch (Exception ex)
            {
                result.setValue(this.setExecutionException(ex, uec));
                if (t != null) t.Rollback();
            }
            finally
            {
                if (t != null) t.Dispose();
                if (r != null) r.Dispose();
            }
            #endregion Operation execution
            return result;
        }
        #endregion manageCreditType

        #region getPropertyTypeById
        private PropertyType getPropertyTypeById(Repository<PropertyType> r, Nullable<Int32> id)
        {
            PropertyType result = null;
            result = r.Search(x => x.Id == id);
            return result;
        }
        #endregion getPropertyTypeById
        #region getPropertyTypeByName
        private PropertyType getPropertyTypeByName(OperationType op, Repository<PropertyType> r, String name, Nullable<Int32> id = null)
        {
            PropertyType result = null;
            if (op == OperationType.Create)
            {
                result = r.Search(x => x.Name == name);
            }
            else if (op == OperationType.Update)
            {
                result = r.Search(x => x.Name == name && x.Id != id);
            }
            return result;
        }
        #endregion getPropertyTypeByName
        #region managePropertyType
        public ManageEntityResult<PropertyType> managePropertyType(ManageEntityParameter<PropertyType> p)
        {
            ManageEntityResult<PropertyType> result = new ManageEntityResult<PropertyType>();
            String uec = @"08";
            #region Validation of parameter
            if (p == null) p = new ManageEntityParameter<PropertyType>();
            else
            {
                if (p.Token == null) p.Token = Guid.Empty;
                if (p.OperationType == null) p.OperationType = OperationType.Indefined;
                if (p.Entity == null) p.Entity = new PropertyType();
            }
            #endregion Validation of parameter
            #region Operation execution
            String ExistingName = @"Tipo de Propiedad existente";
            String ExistingId = @"Identificador del Tipo de Propiedad inválido";
            String ErrorUpdate = @"Error al actualizar el Tipo de Propiedad";
            String ErrorDelete = @"Error al eliminar el Tipo de Propiedad";
            Repository<PropertyType> r = null;
            DbContextTransaction t = null;
            try
            {
                r = new Repository<PropertyType>();
                t = r.DbModel.Database.BeginTransaction();
                if (p.OperationType == OperationType.Read)
                {
                    result.setValueOk(r.Read().ToList());
                }
                else if (p.OperationType == OperationType.SpecificRead)
                {
                    result.setValueOk(r.Read(x => x.Id == p.Entity.Id || x.Name == p.Entity.Name).ToList());
                }
                else if (p.OperationType == OperationType.Create)
                {
                    // Búsqueda del elemento por 'Nombre'
                    PropertyType e = this.getPropertyTypeByName(p.OperationType.Value, r, p.Entity.Name);
                    if (e == null)
                    {
                        // Elemento no existe: Inserción física
                        result.setValueOk(r.Create(p.Entity));
                    }
                    else
                    {
                        // Elemento existente
                        result.setValue(this.setExecutionError(ExistingName, uec, @"01", ExistingName));
                    }
                }
                else if (p.OperationType == OperationType.Update || p.OperationType == OperationType.Delete)
                {
                    // Búsqueda del elemento por 'Id'
                    PropertyType e1 = this.getPropertyTypeById(r, p.Entity.Id);
                    if (e1 == null)
                    {
                        result.setValue(this.setExecutionError(ExistingId, uec, @"02", ExistingId));
                    }
                    else
                    {
                        if (p.OperationType == OperationType.Update)
                        {
                            // Búsqueda del elemento por 'Nombre'
                            PropertyType e2 = this.getPropertyTypeByName(p.OperationType.Value, r, p.Entity.Name, p.Entity.Id);
                            if (e2 != null)
                            {
                                result.setValue(this.setExecutionError(ExistingName, uec, @"03", ExistingName));
                            }
                            else
                            {
                                e1.updateValue(p.Entity);
                                Boolean s = r.Update(e1);
                                if (s)
                                    result.setValueOk(e1);
                                else
                                    result.setValue(this.setExecutionError(ErrorUpdate, uec, @"04", ErrorUpdate));
                            }
                        }
                        else if (p.OperationType == OperationType.Delete)
                        {
                            // Validación de referencias a otras entidades ???
                            e1.clearValues();
                            Boolean s = r.Delete(e1);
                            if (s)
                                result.setValueOk(e1);
                            else
                                result.setValue(this.setExecutionError(ErrorDelete, uec, @"05", ErrorDelete));
                        }
                    }
                }
                else
                {
                    result.setValue(this.setExecutionInvalidOperationType(uec));
                }
                t.Commit();
            }
            catch (DbEntityValidationException ex)
            {
                result.setValue(this.setExecutionDbEntityValidationException(ex, uec));
                if (t != null) t.Rollback();
            }
            catch (DbUpdateException ex)
            {
                result.setValue(this.setExecutionDbUpdateException(ex, uec));
                if (t != null) t.Rollback();
            }
            catch (SqlException ex)
            {
                result.setValue(this.setExecutionSqlException(ex, uec));
                if (t != null) t.Rollback();
            }
            catch (InvalidOperationException ex)
            {
                result.setValue(this.setExecutionInvalidOperationException(ex, uec));
                if (t != null) t.Rollback();
            }
            catch (Exception ex)
            {
                result.setValue(this.setExecutionException(ex, uec));
                if (t != null) t.Rollback();
            }
            finally
            {
                if (t != null) t.Dispose();
                if (r != null) r.Dispose();
            }
            #endregion Operation execution
            return result;
        }
        #endregion managePropertyType

        #region getPurchaseTypeById
        private PurchaseType getPurchaseTypeById(Repository<PurchaseType> r, Nullable<Int32> id)
        {
            PurchaseType result = null;
            result = r.Search(x => x.Id == id);
            return result;
        }
        #endregion getPurchaseTypeById
        #region getPurchaseTypeByName
        private PurchaseType getPurchaseTypeByName(OperationType op, Repository<PurchaseType> r, String name, Nullable<Int32> id = null)
        {
            PurchaseType result = null;
            if (op == OperationType.Create)
            {
                result = r.Search(x => x.Name == name);
            }
            else if (op == OperationType.Update)
            {
                result = r.Search(x => x.Name == name && x.Id != id);
            }
            return result;
        }
        #endregion getPurchaseTypeByName
        #region managePurchaseType
        public ManageEntityResult<PurchaseType> managePurchaseType(ManageEntityParameter<PurchaseType> p)
        {
            ManageEntityResult<PurchaseType> result = new ManageEntityResult<PurchaseType>();
            String uec = @"09";
            #region Validation of parameter
            if (p == null) p = new ManageEntityParameter<PurchaseType>();
            else
            {
                if (p.Token == null) p.Token = Guid.Empty;
                if (p.OperationType == null) p.OperationType = OperationType.Indefined;
                if (p.Entity == null) p.Entity = new PurchaseType();
            }
            #endregion Validation of parameter
            #region Operation execution
            String ExistingName = @"Tipo de Compra existente";
            String ExistingId = @"Identificador del Tipo de Compra inválido";
            String ErrorUpdate = @"Error al actualizar el Tipo de Compra";
            String ErrorDelete = @"Error al eliminar el Tipo de Compra";
            Repository<PurchaseType> r = null;
            DbContextTransaction t = null;
            try
            {
                r = new Repository<PurchaseType>();
                t = r.DbModel.Database.BeginTransaction();
                if (p.OperationType == OperationType.Read)
                {
                    result.setValueOk(r.Read().ToList());
                }
                else if (p.OperationType == OperationType.SpecificRead)
                {
                    result.setValueOk(r.Read(x => x.Id == p.Entity.Id || x.Name == p.Entity.Name).ToList());
                }
                else if (p.OperationType == OperationType.Create)
                {
                    // Búsqueda del elemento por 'Nombre'
                    PurchaseType e = this.getPurchaseTypeByName(p.OperationType.Value, r, p.Entity.Name);
                    if (e == null)
                    {
                        // Elemento no existe: Inserción física
                        result.setValueOk(r.Create(p.Entity));
                    }
                    else
                    {
                        // Elemento existente
                        result.setValue(this.setExecutionError(ExistingName, uec, @"01", ExistingName));
                    }
                }
                else if (p.OperationType == OperationType.Update || p.OperationType == OperationType.Delete)
                {
                    // Búsqueda del elemento por 'Id'
                    PurchaseType e1 = this.getPurchaseTypeById(r, p.Entity.Id);
                    if (e1 == null)
                    {
                        result.setValue(this.setExecutionError(ExistingId, uec, @"02", ExistingId));
                    }
                    else
                    {
                        if (p.OperationType == OperationType.Update)
                        {
                            // Búsqueda del elemento por 'Nombre'
                            PurchaseType e2 = this.getPurchaseTypeByName(p.OperationType.Value, r, p.Entity.Name, p.Entity.Id);
                            if (e2 != null)
                            {
                                result.setValue(this.setExecutionError(ExistingName, uec, @"03", ExistingName));
                            }
                            else
                            {
                                e1.updateValue(p.Entity);
                                Boolean s = r.Update(e1);
                                if (s)
                                    result.setValueOk(e1);
                                else
                                    result.setValue(this.setExecutionError(ErrorUpdate, uec, @"04", ErrorUpdate));
                            }
                        }
                        else if (p.OperationType == OperationType.Delete)
                        {
                            // Validación de referencias a otras entidades ???
                            e1.clearValues();
                            Boolean s = r.Delete(e1);
                            if (s)
                                result.setValueOk(e1);
                            else
                                result.setValue(this.setExecutionError(ErrorDelete, uec, @"05", ErrorDelete));
                        }
                    }
                }
                else
                {
                    result.setValue(this.setExecutionInvalidOperationType(uec));
                }
                t.Commit();
            }
            catch (DbEntityValidationException ex)
            {
                result.setValue(this.setExecutionDbEntityValidationException(ex, uec));
                if (t != null) t.Rollback();
            }
            catch (DbUpdateException ex)
            {
                result.setValue(this.setExecutionDbUpdateException(ex, uec));
                if (t != null) t.Rollback();
            }
            catch (SqlException ex)
            {
                result.setValue(this.setExecutionSqlException(ex, uec));
                if (t != null) t.Rollback();
            }
            catch (InvalidOperationException ex)
            {
                result.setValue(this.setExecutionInvalidOperationException(ex, uec));
                if (t != null) t.Rollback();
            }
            catch (Exception ex)
            {
                result.setValue(this.setExecutionException(ex, uec));
                if (t != null) t.Rollback();
            }
            finally
            {
                if (t != null) t.Dispose();
                if (r != null) r.Dispose();
            }
            #endregion Operation execution
            return result;
        }
        #endregion managePurchaseType

        #region manageProspectiveClientDto
        public ManageEntityResult<ProspectiveClientDto> manageProspectiveClientDto(ManageEntityParameter<ProspectiveClientDto> p)
        {
            ManageEntityResult<ProspectiveClientDto> result = new ManageEntityResult<ProspectiveClientDto>();
            String uec = @"10";
            #region Validation of parameter
            if (p == null) p = new ManageEntityParameter<ProspectiveClientDto>();
            else
            {
                if (p.Token == null) p.Token = Guid.Empty;
                if (p.OperationType == null) p.OperationType = OperationType.Indefined;
                if (p.Entity == null) p.Entity = new ProspectiveClientDto();
            }
            #endregion Validation of parameter
            #region Operation execution
            String ExistingFirstName = @"Nombre no debe ser vacío";
            String ExistingLastName1 = @"Apellido Paterno no debe ser vacío";
            String ExistingEmail = @"Email no debe ser vacío";
            String ExistingPhone = @"Teléfono no debe ser vacío";
            Repository<ProspectiveClient> r = null;
            DbContextTransaction t = null;
            try
            {
                r = new Repository<ProspectiveClient>();
                t = r.DbModel.Database.BeginTransaction();
                if (p.OperationType == OperationType.Create)
                {
                    ProspectiveClientDto dto = p.Entity;
                    if (String.IsNullOrEmpty(dto.FirstName))
                    {
                        result.setValue(this.setExecutionError(ExistingFirstName, uec, @"01", ExistingFirstName));
                    }
                    else if (String.IsNullOrEmpty(dto.LastName1))
                    {
                        result.setValue(this.setExecutionError(ExistingLastName1, uec, @"02", ExistingLastName1));
                    }
                    else if (String.IsNullOrEmpty(dto.Email1))
                    {
                        result.setValue(this.setExecutionError(ExistingEmail, uec, @"03", ExistingEmail));
                    }
                    else if (String.IsNullOrEmpty(dto.Telephone1))
                    {
                        result.setValue(this.setExecutionError(ExistingPhone, uec, @"04", ExistingPhone));
                    }
                    else
                    {
                        ProspectiveClient e = p.Entity.ToEntity();
                        e.DateCreation = DateTime.Now;
                        e.ProspectiveClientStatusId = 1;
                        result.setValueOk(p.Entity.FromEntity(r.Create(e)));
                    }
                }
                else
                {
                    result.setValue(this.setExecutionInvalidOperationType(uec));
                }
                t.Commit();
            }
            catch (DbEntityValidationException ex)
            {
                result.setValue(this.setExecutionDbEntityValidationException(ex, uec));
                if (t != null) t.Rollback();
            }
            catch (DbUpdateException ex)
            {
                result.setValue(this.setExecutionDbUpdateException(ex, uec));
                if (t != null) t.Rollback();
            }
            catch (SqlException ex)
            {
                result.setValue(this.setExecutionSqlException(ex, uec));
                if (t != null) t.Rollback();
            }
            catch (InvalidOperationException ex)
            {
                result.setValue(this.setExecutionInvalidOperationException(ex, uec));
                if (t != null) t.Rollback();
            }
            catch (Exception ex)
            {
                result.setValue(this.setExecutionException(ex, uec));
                if (t != null) t.Rollback();
            }
            finally
            {
                if (t != null) t.Dispose();
                if (r != null) r.Dispose();
            }
            #endregion Operation execution
            return result;
        }
        #endregion manageProspectiveClientDto

        #region getProspectiveClientById
        private ProspectiveClient getProspectiveClientId(Repository<ProspectiveClient> r, Nullable<Int32> id)
        {
            ProspectiveClient result = null;
            result = r.Search(x => x.Id == id);
            return result;
        }
        #endregion getProspectiveClientById
        #region getProspectiveClientByEmail
        private ProspectiveClient getProspectiveClientByEmail(OperationType op, Repository<ProspectiveClient> r, String email1, Nullable<Int32> id = null)
        {
            ProspectiveClient result = null;
            if (op == OperationType.Create)
            {
                result = r.Search(x => x.Email1 == email1);
            }
            else if (op == OperationType.Update)
            {
                result = r.Search(x => x.Email1 == email1 && x.Id != id);
            }
            return result;
        }
        #endregion getProspectiveClientByEmail
        #region manageProspectiveClientDetailDto
        public ManageEntityResult<ProspectiveClientDetailDto> manageProspectiveClientDetailDto(ManageEntityParameter<ProspectiveClientDetailDto> p)
        {
            ManageEntityResult<ProspectiveClientDetailDto> result = new ManageEntityResult<ProspectiveClientDetailDto>();
            String uec = @"11";
            #region Validation of parameter
            if (p == null) p = new ManageEntityParameter<ProspectiveClientDetailDto>();
            else
            {
                if (p.Token == null) p.Token = Guid.Empty;
                if (p.OperationType == null) p.OperationType = OperationType.Indefined;
                if (p.Entity == null) p.Entity = new ProspectiveClientDetailDto();
            }
            #endregion Validation of parameter
            #region Operation execution
            String ExistingEmail = @"Email existente";
            String ExistingId = @"Identificador del Prospecto inválido";
            String ErrorUpdate = @"Error al actualizar el Prospecto";
            String ErrorDelete = @"Error al eliminar el Prospecto";
            Repository<ProspectiveClient> r1 = null;
            Repository<ProspectiveClientDetail> r2 = null;
            DbContextTransaction t = null;
            try
            {
                r1 = new Repository<ProspectiveClient>();
                r2 = new Repository<ProspectiveClientDetail>(r1.DbModel);
                t = r1.DbModel.Database.BeginTransaction();
                if (p.OperationType == OperationType.Read)
                {
                    List<ProspectiveClient> l1 = r1.Read().ToList();
                    ProspectiveClientDetail pcd = null;
                    List<ProspectiveClientDetailDto> l = new List<ProspectiveClientDetailDto>();
                    foreach (ProspectiveClient pc in l1)
                    {
                        pcd = r2.Search(x => x.ProspectiveClientId == pc.Id);
                        if(pcd == null) { pcd = new ProspectiveClientDetail(); }
                        l.Add(new ProspectiveClientDetailDto(pc, pcd));
                    }
                    result.setValueOk(l);
                }
                else if (p.OperationType == OperationType.SpecificRead)
                {
                    ProspectiveClientDetailDto item = null;
                    ProspectiveClient pc = r1.Search(x => x.Id == p.Entity.PC.Id);
                    ProspectiveClientDetail pcd = r2.Search(x => x.ProspectiveClientId == p.Entity.PC.Id);
                    item = new ProspectiveClientDetailDto(pc, pcd);
                    result.setValueOk(item);
                }
                else if (p.OperationType == OperationType.Create)
                {
                    // Búsqueda del elemento por 'Email'
                    ProspectiveClient pc = this.getProspectiveClientByEmail(p.OperationType.Value, r1, p.Entity.PC.Email1);
                    if (pc == null)
                    {
                        // Elemento no existe: Inserción física
                        p.Entity.PC.DateCreation = DateTime.Now;
                        pc = r1.Create(p.Entity.PC);
                        p.Entity.PCD.ProspectiveClientId = pc.Id;
                        ProspectiveClientDetail pcd = r2.Create(p.Entity.PCD);
                        ProspectiveClientDetailDto item = new ProspectiveClientDetailDto(pc, pcd);
                        result.setValueOk(item);
                    }
                    else
                    {
                        // Elemento existente
                        result.setValue(this.setExecutionError(ExistingEmail, uec, @"01", ExistingEmail));
                    }
                }
                else if (p.OperationType == OperationType.Update || p.OperationType == OperationType.Delete)
                {
                    // Búsqueda del elemento por 'Id'
                    ProspectiveClient e1 = this.getProspectiveClientId(r1, p.Entity.PC.Id);
                    if (e1 == null)
                    {
                        result.setValue(this.setExecutionError(ExistingId, uec, @"02", ExistingId));
                    }
                    else
                    {
                        if (p.OperationType == OperationType.Update)
                        {
                            // Búsqueda del elemento por 'Email'
                            ProspectiveClient e2 = this.getProspectiveClientByEmail(p.OperationType.Value, r1, p.Entity.PC.Email1, p.Entity.PC.Id);
                            if (e2 != null)
                            {
                                result.setValue(this.setExecutionError(ExistingEmail, uec, @"03", ExistingEmail));
                            }
                            else
                            {
                                e1.updateValue(p.Entity.PC);
                                Boolean s1 = r1.Update(e1);
                                Boolean s2 = r2.Update(p.Entity.PCD);
                                if (s1 && s2)
                                {
                                    ProspectiveClientDetailDto item = new ProspectiveClientDetailDto(e1, p.Entity.PCD);
                                    result.setValueOk(item);
                                }
                                else
                                {
                                    result.setValue(this.setExecutionError(ErrorUpdate, uec, @"04", ErrorUpdate));
                                }
                            }
                        }
                        else if (p.OperationType == OperationType.Delete)
                        {
                            // Validación de referencias a otras entidades ???
                            Boolean s2 = r2.Delete(p.Entity.PCD);
                            Boolean s1 = r1.Delete(e1);
                            e1.clearValues();
                            p.Entity.PCD.clearValues();
                            if (s1 && s2)
                            {
                                ProspectiveClientDetailDto item = new ProspectiveClientDetailDto(e1, p.Entity.PCD);
                                result.setValueOk(item);
                            }
                            else
                                result.setValue(this.setExecutionError(ErrorDelete, uec, @"05", ErrorDelete));
                        }
                    }
                }
                else
                {
                    result.setValue(this.setExecutionInvalidOperationType(uec));
                }
                t.Commit();
            }
            catch (DbEntityValidationException ex)
            {
                result.setValue(this.setExecutionDbEntityValidationException(ex, uec));
                if (t != null) t.Rollback();
            }
            catch (DbUpdateException ex)
            {
                result.setValue(this.setExecutionDbUpdateException(ex, uec));
                if (t != null) t.Rollback();
            }
            catch (SqlException ex)
            {
                result.setValue(this.setExecutionSqlException(ex, uec));
                if (t != null) t.Rollback();
            }
            catch (InvalidOperationException ex)
            {
                result.setValue(this.setExecutionInvalidOperationException(ex, uec));
                if (t != null) t.Rollback();
            }
            catch (Exception ex)
            {
                result.setValue(this.setExecutionException(ex, uec));
                if (t != null) t.Rollback();
            }
            finally
            {
                if (t != null) t.Dispose();
                if (r1 != null) r1.Dispose();
                if (r2 != null) r2.Dispose();
            }
            #endregion Operation execution
            return result;
        }
        #endregion manageProspectiveClientDetailDto
    }
}
