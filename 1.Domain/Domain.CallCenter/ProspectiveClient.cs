﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;

namespace Domain.CallCenter
{
    [DataContract(Name = "ProspectiveClient", Namespace = "http://www.socasesores.com", IsReference = false)]
    public class ProspectiveClient
    {
        #region properties
        [Key] //temp
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)] //temp
        [DisplayName("Id")]
        [DataMember(Name = "Id", EmitDefaultValue = true, IsRequired = true, Order = 1)]
        public Nullable<Int32> Id { get; set; }

        [DisplayName("Nombre:")]
        [DataMember(Name = "FirstName", EmitDefaultValue = true, IsRequired = true, Order = 2)]
        public String FirstName { get; set; }

        [DisplayName("Segundo Nombre")]
        [DataMember(Name = "SecondName", EmitDefaultValue = true, IsRequired = true, Order = 3)]
        public String SecondName { get; set; }

        [DisplayName("Apellido Paterno")]
        [DataMember(Name = "LastName1", EmitDefaultValue = true, IsRequired = true, Order = 4)]
        public String LastName1 { get; set; }

        [DisplayName("Apellido Materno")]
        [DataMember(Name = "LastName2", EmitDefaultValue = true, IsRequired = true, Order = 5)]
        public String LastName2 { get; set; }

        [DisplayName("Fecha de Creación")]
        [DataMember(Name = "DateCreation", EmitDefaultValue = true, IsRequired = true, Order = 6)]
        public Nullable<DateTime> DateCreation { get; set; }

        [DisplayName("Fecha de Nacimiento")]
        [DataMember(Name = "BirthDate", EmitDefaultValue = true, IsRequired = true, Order = 7)]
        public Nullable<DateTime> BirthDate { get; set; }

        [DisplayName("Teléfono")]
        [DataMember(Name = "Telephone1", EmitDefaultValue = true, IsRequired = true, Order = 8)]
        public String Telephone1 { get; set; }

        [DisplayName("Teléfono alterno")]
        [DataMember(Name = "Telephone2", EmitDefaultValue = true, IsRequired = true, Order = 9)]
        public String Telephone2 { get; set; }

        [DisplayName("Email")]
        [DataMember(Name = "Email1", EmitDefaultValue = true, IsRequired = true, Order = 10)]
        public string Email1 { get; set; }

        [DisplayName("Email alterno")]
        [DataMember(Name = "Email2", EmitDefaultValue = true, IsRequired = true, Order = 11)]
        public string Email2 { get; set; }

        [DisplayName("Lugar de residencia")]
        [DataMember(Name = "ResidenceLocationId", EmitDefaultValue = true, IsRequired = true, Order = 12)]
        public Nullable<Int32> ResidenceLocationId { get; set; }

        [DisplayName("Lugar de nacimiento")]
        [DataMember(Name = "BirthLocationId", EmitDefaultValue = true, IsRequired = true, Order = 13)]
        public Nullable<Int32> BirthLocationId { get; set; }

        [DisplayName("CURP")]
        [DataMember(Name = "CURP", EmitDefaultValue = true, IsRequired = true, Order = 14)]
        public String CURP { get; set; }

        [DisplayName("RFC")]
        [DataMember(Name = "RFC", EmitDefaultValue = true, IsRequired = true, Order = 15)]
        public String RFC { get; set; }

        [DisplayName("Valor del inmueble")]
        [DataMember(Name = "PropertyValue", EmitDefaultValue = true, IsRequired = true, Order = 16)]
        public Nullable<decimal> PropertyValue { get; set; }

        [DisplayName("Enganche")]
        [DataMember(Name = "Deposit", EmitDefaultValue = true, IsRequired = true, Order = 17)]
        public Nullable<decimal> Deposit { get; set; }

        [DisplayName("Monto solicitado")]
        [DataMember(Name = "RequestedAmount", EmitDefaultValue = true, IsRequired = true, Order = 18)]
        public Nullable<decimal> RequestedAmount { get; set; }

        [DisplayName("Campaña")]
        [DataMember(Name = "AdvertisingCampaignId", EmitDefaultValue = true, IsRequired = true, Order = 19)]
        public Nullable<Int32> AdvertisingCampaignId { get; set; }

        [DisplayName("Status")]
        [DataMember(Name = "ProspectiveClientStatusId", EmitDefaultValue = true, IsRequired = true, Order = 20)]
        public Nullable<Int32> ProspectiveClientStatusId { get; set; }

        [DisplayName("Tipo de propiedad")]
        [DataMember(Name = "PropertyTypeId", EmitDefaultValue = true, IsRequired = true, Order = 21)]
        public Nullable<Int32> PropertyTypeId { get; set; }

        [DisplayName("Tipo de crédito")]
        [DataMember(Name = "CreditTypeId", EmitDefaultValue = true, IsRequired = true, Order = 22)]
        public Nullable<Int32> CreditTypeId { get; set; }

        [DisplayName("Tipo de compra")]
        [DataMember(Name = "PurchaseTypeId", EmitDefaultValue = true, IsRequired = true, Order = 23)]
        public Nullable<Int32> PurchaseTypeId { get; set; }

        [DisplayName("Estado donde solicita inmueble")]
        [DataMember(Name = "LocationStateId", EmitDefaultValue = true, IsRequired = true, Order = 24)]
        public Nullable<Int32> LocationStateId { get; set; }

        [DisplayName("Ciudad")]
        [DataMember(Name = "LocationCityId", EmitDefaultValue = true, IsRequired = true, Order = 25)]
        public Nullable<Int32> LocationCityId { get; set; }

        [DisplayName("Broker")]
        [DataMember(Name = "BrokerId", EmitDefaultValue = true, IsRequired = true, Order = 26)]
        public Nullable<Int32> BrokerId { get; set; }

        [IgnoreDataMember]
        public AdvertisingCampaign AdvertisingCampaign { get; set; }

        [IgnoreDataMember]
        public  PurchaseType PurchaseType { get; set; }

        [IgnoreDataMember]
        public CreditType CreditType { get; set; }

        [IgnoreDataMember]
        public PropertyType PropertyType { get; set; }

        [IgnoreDataMember]
        public ProspectiveClientDetail ProspectiveClientDetail { get; set; }

        [IgnoreDataMember]
        public ProspectiveClientStatus ProspectiveClientStatus { get; set; }

        [IgnoreDataMember]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public ICollection<ProspectiveClientHistory> ProspectiveClientHistory { get; set; }

        [IgnoreDataMember]
        public LocationState LocationState { get; set; }

        [IgnoreDataMember]
        public LocationState LocationState1 { get; set; }

        [IgnoreDataMember]
        public LocationState LocationState2 { get; set; }

        [IgnoreDataMember]
        public LocationCity LocationCity { get; set; }
        #endregion properties
        #region constructors & destructor
        public ProspectiveClient()
        {
            this.Id = null;
            this.FirstName = null;
            this.SecondName = null;
            this.LastName1 = null;
            this.LastName2 = null;
            this.DateCreation = null;
            this.BirthDate = null;
            this.Telephone1 = null;
            this.Telephone2 = null;
            this.Email1 = null;
            this.Email2 = null;
            this.ResidenceLocationId = null;
            this.BirthLocationId = null;
            this.CURP = null;
            this.RFC = null;
            this.PropertyValue = null;
            this.Deposit = null;
            this.RequestedAmount = null;
            this.AdvertisingCampaignId = null;
            this.ProspectiveClientStatusId = null;
            this.PropertyTypeId = null;
            this.CreditTypeId = null;
            this.PurchaseTypeId = null;
            this.LocationStateId = null;
            this.LocationCityId = null;
            this.BrokerId = null;
            //
            this.AdvertisingCampaign = null;
            this.CreditType = null;
            this.LocationCity = null;
            this.LocationState = null;
            this.LocationState1 = null;
            this.LocationState2 = null;
            this.PropertyType = null;
            this.ProspectiveClientStatus = null;
            this.PurchaseType = null;
            this.ProspectiveClientDetail = null;
            this.ProspectiveClientHistory = new HashSet<ProspectiveClientHistory>();
        }
        public ProspectiveClient(Nullable<Int32> id, String firstName, String secondName, String lastName1, String lastName2, Nullable<DateTime> dateCreation, Nullable<DateTime> birthDate, 
                                 String telephone1, String telephone2, String email1, String email2, Nullable<Int32> residenceLocationId, Nullable<Int32> birthLocationId, String curp,
                                 String rfc, Nullable<decimal> propertyValue, Nullable<decimal> deposit, Nullable<decimal> requestedAmount, Nullable<Int32> advertisingCampaignId, Nullable<Int32> prospectiveClientStatusId,
                                 Nullable<Int32> propertyTypeId, Nullable<Int32> creditTypeId, Nullable<Int32> purchaseTypeId, Nullable<Int32> locationStateId, Nullable<Int32> locationCityId, Nullable<Int32> brokerId) : this()
        {
            this.setValue(id, firstName, secondName, lastName1, lastName2, dateCreation, birthDate, telephone1, telephone2, email1, email2, residenceLocationId, birthLocationId, curp, rfc,propertyValue, deposit, requestedAmount,
                          advertisingCampaignId, prospectiveClientStatusId,propertyTypeId, creditTypeId, purchaseTypeId, locationStateId, locationCityId, brokerId);
        }
        public ProspectiveClient(ProspectiveClient obj) : this()
        {
            this.setValue(obj);
        }
        #endregion constructors & destructor
        #region setValue
        public void setValue(Nullable<Int32> id, String firstName, String secondName, String lastName1, String lastName2, Nullable<DateTime> dateCreation, Nullable<DateTime> birthDate,
                                 String telephone1, String telephone2, String email1, String email2, Nullable<Int32> residenceLocationId, Nullable<Int32> birthLocationId, String curp,
                                 String rfc, Nullable<decimal> propertyValue, Nullable<decimal> deposit, Nullable<decimal> requestedAmount, Nullable<Int32> advertisingCampaignId, Nullable<Int32> prospectiveClientStatusId,
                                 Nullable<Int32> propertyTypeId, Nullable<Int32> creditTypeId, Nullable<Int32> purchaseTypeId, Nullable<Int32> locationStateId, Nullable<Int32> locationCityId, Nullable<Int32> brokerId)
        {
            this.Id = id;
            this.FirstName = firstName;
            this.SecondName = secondName;
            this.LastName1 = lastName1;
            this.LastName2 = lastName2;
            this.DateCreation = dateCreation;
            this.BirthDate = birthDate;
            this.Telephone1 = telephone1;
            this.Telephone2 = telephone2;
            this.Email1 = email1;
            this.Email2 = email2;
            this.ResidenceLocationId = residenceLocationId;
            this.BirthLocationId = birthLocationId;
            this.CURP = curp;
            this.RFC = rfc;
            this.PropertyValue = propertyValue;
            this.Deposit = deposit;
            this.RequestedAmount = requestedAmount;
            this.AdvertisingCampaignId = advertisingCampaignId;
            this.ProspectiveClientStatusId = prospectiveClientStatusId;
            this.PropertyTypeId = propertyTypeId;
            this.CreditTypeId = creditTypeId;
            this.PurchaseTypeId = purchaseTypeId;
            this.LocationStateId = locationStateId;
            this.LocationCityId = locationCityId;
            this.BrokerId = brokerId;
        }
        public void setValue(ProspectiveClient obj)
        {
            this.Id = obj.Id;
            this.FirstName = obj.FirstName;
            this.SecondName = obj.SecondName;
            this.LastName1 = obj.LastName1;
            this.LastName2 = obj.LastName2;
            this.DateCreation = obj.DateCreation;
            this.BirthDate = obj.BirthDate;
            this.Telephone1 = obj.Telephone1;
            this.Telephone2 = obj.Telephone2;
            this.Email1 = obj.Email1;
            this.Email2 = obj.Email2;
            this.ResidenceLocationId = obj.ResidenceLocationId;
            this.BirthLocationId = obj.BirthLocationId;
            this.CURP = obj.CURP;
            this.RFC = obj.RFC;
            this.PropertyValue = obj.PropertyValue;
            this.Deposit = obj.Deposit;
            this.RequestedAmount = obj.RequestedAmount;
            this.AdvertisingCampaignId = obj.AdvertisingCampaignId;
            this.ProspectiveClientStatusId = obj.ProspectiveClientStatusId;
            this.PropertyTypeId = obj.PropertyTypeId;
            this.CreditTypeId = obj.CreditTypeId;
            this.PurchaseTypeId = obj.PurchaseTypeId;
            this.LocationStateId = obj.LocationStateId;
            this.LocationCityId = obj.LocationCityId;
            this.BrokerId = obj.BrokerId;
        }
        #endregion setValue
        #region updateValue
        public void updateValue(ProspectiveClient obj)
        {
            this.FirstName = obj.FirstName;
            this.SecondName = obj.SecondName;
            this.LastName1 = obj.LastName1;
            this.LastName2 = obj.LastName2;
            this.DateCreation = obj.DateCreation;
            this.BirthDate = obj.BirthDate;
            this.Telephone1 = obj.Telephone1;
            this.Telephone2 = obj.Telephone2;
            this.Email1 = obj.Email1;
            this.Email2 = obj.Email2;
            this.ResidenceLocationId = obj.ResidenceLocationId;
            this.BirthLocationId = obj.BirthLocationId;
            this.CURP = obj.CURP;
            this.RFC = obj.RFC;
            this.PropertyValue = obj.PropertyValue;
            this.Deposit = obj.Deposit;
            this.RequestedAmount = obj.RequestedAmount;
            this.AdvertisingCampaignId = obj.AdvertisingCampaignId;
            this.ProspectiveClientStatusId = obj.ProspectiveClientStatusId;
            this.PropertyTypeId = obj.PropertyTypeId;
            this.CreditTypeId = obj.CreditTypeId;
            this.PurchaseTypeId = obj.PurchaseTypeId;
            this.LocationStateId = obj.LocationStateId;
            this.LocationCityId = obj.LocationCityId;
            this.BrokerId = obj.BrokerId;
        }
        #endregion updateValue
        #region clearValues
        public void clearValues()
        {
            this.FirstName = null;
            this.SecondName = null;
            this.LastName1 = null;
            this.LastName2 = null;
            this.DateCreation = null;
            this.BirthDate = null;
            this.Telephone1 = null;
            this.Telephone2 = null;
            this.Email1 = null;
            this.Email2 = null;
            this.ResidenceLocationId = null;
            this.BirthLocationId = null;
            this.CURP = null;
            this.RFC = null;
            this.PropertyValue = null;
            this.Deposit = null;
            this.RequestedAmount = null;
            this.AdvertisingCampaignId = null;
            this.ProspectiveClientStatusId = null;
            this.PropertyTypeId = null;
            this.CreditTypeId = null;
            this.PurchaseTypeId = null;
            this.LocationStateId = null;
            this.LocationCityId = null;
            this.BrokerId = null;
        }
        #endregion clearValues
    }
}
