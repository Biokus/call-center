﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Domain.CallCenter
{
    [DataContract(Name = "ProspectiveClientStatus", Namespace = "http://www.socasesores.com", IsReference = false)]
    public class ProspectiveClientStatus
    {
        #region properties
        [DataMember(Name = "Id", EmitDefaultValue = true, IsRequired = true, Order = 1)]
        public Nullable<Int32> Id { get; set; }

        [DataMember(Name = "Name", EmitDefaultValue = true, IsRequired = true, Order = 2)]
        public string Name { get; set; }

        [IgnoreDataMember]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public ICollection<ProspectiveClient> ProspectiveClient { get; set; }

        [IgnoreDataMember]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public ICollection<ProspectiveClientHistory> ProspectiveClientHistory { get; set; }
        #endregion properties
        #region constructors & destructor
        public ProspectiveClientStatus()
        {
            this.Id = null;
            this.Name = null;
            this.ProspectiveClient = new HashSet<ProspectiveClient>();
            this.ProspectiveClientHistory = new HashSet<ProspectiveClientHistory>();
        }
        public ProspectiveClientStatus(Nullable<Int32> id, String name) : this()
        {
            this.setValue(id, name);
        }
        public ProspectiveClientStatus(ProspectiveClientStatus obj) : this()
        {
            this.setValue(obj);
        }
        #endregion constructors & destructor
        #region setValue
        public void setValue(Nullable<Int32> id, String name)
        {
            this.Id = id;
            this.Name = name;
        }
        public void setValue(ProspectiveClientStatus obj)
        {
            this.Id = obj.Id;
            this.Name = obj.Name;
        }
        #endregion setValue
        #region updateValue
        public void updateValue(ProspectiveClientStatus obj)
        {
            //this.Id = obj.Id;
            this.Name = obj.Name;
        }
        #endregion updateValue
        #region clearValues
        public void clearValues()
        {
            //this.Id = null;
            this.Name = null;
        }
        #endregion clearValues
    }
}
