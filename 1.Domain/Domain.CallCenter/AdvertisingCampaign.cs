﻿using Domain.CallCenter.Validations;
using FluentValidation.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Domain.CallCenter
{
    [Validator(typeof(AdvertisingCampaignValidator))]
    [DataContract(Name = "AdvertisingCampaign", Namespace = "http://www.socasesores.com", IsReference = false)]
    public class AdvertisingCampaign
    {
        #region properties
        [DisplayName("Id")]
        [DataMember(Name = "Id", EmitDefaultValue = true, IsRequired = true, Order = 1)]
        public Nullable<Int32> Id { get; set; }

        [DisplayName("Nombre")]
        [DataMember(Name = "Name", EmitDefaultValue = true, IsRequired = true, Order = 2)]
        public String Name { get; set; }

        [DisplayName("Fecha Inicio")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [DataMember(Name = "DateStart", EmitDefaultValue = true, IsRequired = true, Order = 3)]
        public Nullable<DateTime> DateStart { get; set; }

        [DisplayName("Fecha Fin")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [DataMember(Name = "DateEnd", EmitDefaultValue = true, IsRequired = true, Order = 4)]
        public Nullable<DateTime> DateEnd { get; set; }

        [DisplayName("Contacto")]
        [DataMember(Name = "ContactName", EmitDefaultValue = true, IsRequired = true, Order = 5)]
        public String ContactName { get; set; }

        [DisplayName("Email")]
        [DataMember(Name = "ContactEmail", EmitDefaultValue = true, IsRequired = true, Order = 6)]
        public String ContactEmail { get; set; }

        [DisplayName("Núm. Teléfono")]
        [DataMember(Name = "ContactPhoneNumber", EmitDefaultValue = true, IsRequired = true, Order = 7)]
        public String ContactPhoneNumber { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        [IgnoreDataMember]
        public ICollection<ProspectiveClient> ProspectiveClient { get; set; }
        #endregion properties
        #region constructors & destructor
        public AdvertisingCampaign()
        {
            this.Id = null;
            this.Name = null;
            this.DateStart = null;
            this.DateEnd = null;
            this.ContactName = null;
            this.ContactEmail = null;
            this.ContactPhoneNumber = null;
            //
            this.ProspectiveClient = new HashSet<ProspectiveClient>();
        }
        public AdvertisingCampaign(Nullable<Int32> id, String name, Nullable<DateTime> dateStart, Nullable<DateTime> dateEnd, String contactName, String contactEmail, String contactPhoneNumber) : this()
        {
            this.setValue(id, name, dateStart, dateEnd, contactName, contactEmail, contactPhoneNumber);
        }
        public AdvertisingCampaign(AdvertisingCampaign obj) : this()
        {
            this.setValue(obj);
        }
        #endregion constructors & destructor
        #region setValue
        public void setValue(Nullable<Int32> id, String name, Nullable<DateTime> dateStart, Nullable<DateTime> dateEnd, String contactName, String contactEmail, String contactPhoneNumber)
        {
            this.Id = id;
            this.Name = name;
            this.DateStart = dateStart;
            this.DateEnd = dateEnd;
            this.ContactName = contactName;
            this.ContactEmail = contactEmail;
            this.ContactPhoneNumber = contactPhoneNumber;
        }
        public void setValue(AdvertisingCampaign obj)
        {
            this.Id = obj.Id;
            this.Name = obj.Name;
            this.DateStart = obj.DateStart;
            this.DateEnd = obj.DateEnd;
            this.ContactName = obj.ContactName;
            this.ContactEmail = obj.ContactEmail;
            this.ContactPhoneNumber = obj.ContactPhoneNumber;
        }
        #endregion setValue
        #region updateValue
        public void updateValue(AdvertisingCampaign obj)
        {
            //this.Id = obj.Id;
            this.Name = obj.Name;
            this.DateStart = obj.DateStart;
            this.DateEnd = obj.DateEnd;
            this.ContactName = obj.ContactName;
            this.ContactEmail = obj.ContactEmail;
            this.ContactPhoneNumber = obj.ContactPhoneNumber;
        }
        #endregion updateValue
        #region clearValues
        public void clearValues()
        {
            //this.Id = null;
            this.Name = null;
            this.DateStart = null;
            this.DateEnd = null;
            this.ContactName = null;
            this.ContactEmail = null;
            this.ContactPhoneNumber = null;
        }
        #endregion clearValues
    }
}
