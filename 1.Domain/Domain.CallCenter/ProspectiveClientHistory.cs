﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Domain.CallCenter
{
    [DataContract(Name = "ProspectiveClientHistory", Namespace = "http://www.socasesores.com", IsReference = false)]
    public class ProspectiveClientHistory
    {
        #region properties
        [DataMember(Name = "Id", EmitDefaultValue = true, IsRequired = true, Order = 1)]
        public Nullable<Int32> Id { get; set; }

        [DataMember(Name = "ProspectiveClientId", EmitDefaultValue = true, IsRequired = true, Order = 2)]
        public Nullable<Int32> ProspectiveClientId { get; set; }

        [DataMember(Name = "Date", EmitDefaultValue = true, IsRequired = true, Order = 3)]
        public Nullable<DateTime> Date { get; set; }

        [DataMember(Name = "ProspectiveClientStatusId", EmitDefaultValue = true, IsRequired = true, Order = 4)]
        public Nullable<Int32> ProspectiveClientStatusId { get; set; }

        [DataMember(Name = "EmployeeId", EmitDefaultValue = true, IsRequired = true, Order = 5)]
        public Nullable<Int32> EmployeeId { get; set; }

        [DataMember(Name = "Commentary", EmitDefaultValue = true, IsRequired = true, Order =6)]
        public String Commentary { get; set; }

        [IgnoreDataMember]
        public ProspectiveClientStatus ProspectiveClientStatus { get; set; }

        [IgnoreDataMember]
        public ProspectiveClient ProspectiveClient { get; set; }
        #endregion properties
        #region constructores & destructores
        public ProspectiveClientHistory()
        {
            this.Id = null;
            this.ProspectiveClientId = null;
            this.Date = null;
            this.Commentary = null;
            this.ProspectiveClient = null;
            this.ProspectiveClientStatus = null;
        }
        public ProspectiveClientHistory(Nullable<Int32>  id, Nullable<DateTime> date, String commentary):this()
        {
            this.SetValue(id, date, commentary);
        }
        public ProspectiveClientHistory(ProspectiveClientHistory obj)
        {
            this.SetValue(obj);
        }
        #endregion constructores & destructores
        #region SetValue
        public void SetValue(Nullable<Int32> id, Nullable<DateTime> date, String commentary)
        {
            this.Id = id;
            this.Date = date;
            this.Commentary = commentary;
        }
        public void SetValue(ProspectiveClientHistory obj)
        {
            this.Id = obj.Id;
            this.Date = obj.Date;
            this.Commentary = obj.Commentary;
        }
        #endregion SetValue
        #region UpdateValue
        public void updateValue(ProspectiveClientHistory obj)
        {
            this.Id = obj.Id;
            this.Date = obj.Date;
            this.Commentary = obj.Commentary;
        }
        #endregion UpdateValue
        #region clearValues
        public void clearValues()
        {
            this.Id = null;
            this.Date = null;
            this.Commentary = null;
        }
        #endregion
    }
}
