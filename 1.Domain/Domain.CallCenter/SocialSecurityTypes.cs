﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Domain.CallCenter
{
    [DataContract(Name = "SocialSecurityTypes", Namespace = "http://www.socasesores.com", IsReference = false)]
    public class SocialSecurityTypes
    {
        #region properties
        [DataMember(Name = "Id", EmitDefaultValue = true, IsRequired = true, Order = 1)]
        public Nullable<Int32> Id { get; set; }

        [DataMember(Name = "Name", EmitDefaultValue = true, IsRequired = true, Order = 2)]
        public String Name { get; set; }

        [IgnoreDataMember]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public ICollection<ProspectiveClientDetail> ProspectiveClientDetail { get; set; }
        #endregion properties
        #region constructores & destructores
        public SocialSecurityTypes()
        {
            this.Id = null;
            this.Name = null;
            this.ProspectiveClientDetail = new HashSet<ProspectiveClientDetail>();
        }
        public SocialSecurityTypes(Nullable<Int32> id, String name) : this()
        {
            this.setValue(id,name);
        }
        public SocialSecurityTypes(SocialSecurityTypes obj) : this()
        {
            this.setValue(obj);
        }
        #endregion
        #region setValue
        public void setValue(Nullable<Int32> id, String name)
        {
            this.Id = id;
            this.Name = name;
        }
        public void setValue(SocialSecurityTypes obj)
        {
            this.Id = obj.Id;
            this.Name = obj.Name;
        }
        #endregion setValue
        #region updateValue
        public void updateValue(SocialSecurityTypes obj)
        {
            //this.Id = obj.Id;
            this.Name = obj.Name;
        }
        #endregion updateValue
        #region clearValues
        public void clearValues()
        {
            //this.Id = null;
            this.Name = null;
        }
        #endregion clearValues
    }
}
