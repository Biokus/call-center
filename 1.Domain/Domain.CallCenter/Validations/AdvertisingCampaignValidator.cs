﻿using FluentValidation;

namespace Domain.CallCenter.Validations
{
    public class AdvertisingCampaignValidator : AbstractValidator<AdvertisingCampaign>
    {
        #region constructors & destructor
        public AdvertisingCampaignValidator()
        {
            RuleFor(x => x.Name).NotEmpty().WithMessage(@"Nombre es requerido");
            RuleFor(x => x.Name).Length(1, 250).WithMessage(@"Nombre tiene una longitud máxima de 250 caracteres");
            RuleFor(x => x.DateStart).NotEmpty().WithMessage("Fecha inicio es requerido");
            RuleFor(x => x.DateEnd).NotEmpty().WithMessage("Fecha fin es requerido");
            RuleFor(x => x.DateEnd).GreaterThanOrEqualTo(x => x.DateStart.Value).When(x => x.DateStart.HasValue).WithMessage("Fecha fin debe ser mayor que Fecha inicio");
            RuleFor(x => x.ContactName).Length(1, 250).WithMessage(@"Nombre de Contacto tiene una longitud máxima de 250 caracteres");
            RuleFor(x => x.ContactEmail).EmailAddress().WithMessage(@"Email de Contacto no tiene formato válido");
            RuleFor(x => x.ContactPhoneNumber).Matches(@"(?:[0-9][ ]?){10}").WithMessage(@"Número teléfonico de Contacto no tiene formato válido");
        }
        #endregion constructors & destructor
    }
}
