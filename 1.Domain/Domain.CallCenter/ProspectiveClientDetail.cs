﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;

namespace Domain.CallCenter
{
    [DataContract(Name = "ProspectiveClientDetail", Namespace = "http://www.socasesores.com", IsReference = false)]
    public class ProspectiveClientDetail
    {
        #region properties
        [Key] //temp
        [DatabaseGenerated(DatabaseGeneratedOption.None)] //temp
        [ForeignKey("ProspectiveClient")] //temp
        [DisplayName("ProspectoCliente")]
        [DataMember(Name = "ProspectiveClientId", EmitDefaultValue = true, IsRequired = true, Order = 1)]
        public Nullable<Int32> ProspectiveClientId { get; set; }

        [DisplayName("NSS")]
        [DataMember(Name = "NSS", EmitDefaultValue = true, IsRequired = true, Order = 2)]
        public String NSS { get; set; }

        [DisplayName("Ingreso mensual")]
        [DataMember(Name = "MonthlySalary1", EmitDefaultValue = true, IsRequired = true, Order = 3)]
        public Nullable<decimal> MonthlySalary1{ get; set; }

        [DisplayName("Antigüedad")]
        [DataMember(Name = "Antiquity1", EmitDefaultValue = true, IsRequired = true, Order = 4)]
        public Nullable<short> Antiquity1 { get; set; }

        [DisplayName("Ingreso mensual")]
        [DataMember(Name = "MonthlySalary2", EmitDefaultValue = true, IsRequired = true, Order = 5)]
        public Nullable<decimal> MonthlySalary2 { get; set; }

        [DisplayName("Antigüedad")]
        [DataMember(Name = "Antiquity2", EmitDefaultValue = true, IsRequired = true, Order = 6)]
        public Nullable<short> Antiquity2 { get; set; }

        [DisplayName("Alta en hacienda")]
        [DataMember(Name = "IsActiveSAT", EmitDefaultValue = true, IsRequired = true, Order = 7)]
        public Nullable<Boolean> IsActiveSAT { get; set; }

        [DisplayName("Actividad")]
        [DataMember(Name = "Activiy", EmitDefaultValue = true, IsRequired = true, Order = 8)]
        public String Activiy { get; set; }

        [DisplayName("Comprobante de ingresos")]
        [DataMember(Name = "MoneyReceipt", EmitDefaultValue = true, IsRequired = true, Order = 9)]
        public String MoneyReceipt { get; set; }

        [DisplayName("Estado Civil")]
        [DataMember(Name = "CivilStatusId", EmitDefaultValue = true, IsRequired = true, Order = 10)]
        public Nullable<Int32> CivilStatusId { get; set; }

        [DisplayName("Problemas de buro")]
        [DataMember(Name = "IsWorngCreditBureau", EmitDefaultValue = true, IsRequired = true, Order = 11)]
        public Nullable<Boolean> IsWorngCreditBureau { get; set; }

        [DisplayName("Deudas mensuales")]
        [DataMember(Name = "DebtMonthly", EmitDefaultValue = true, IsRequired = true, Order = 12)]
        public Nullable<decimal> DebtMonthly { get; set; }

        [DisplayName("Tipo de problema")]
        [DataMember(Name = "ProblemType", EmitDefaultValue = true, IsRequired = true, Order = 13)]
        public String ProblemType { get; set; }

        [DisplayName("Historial crediticio")]
        [DataMember(Name = "IsHasCreditHistory", EmitDefaultValue = true, IsRequired = true, Order = 14)]
        public Nullable<Boolean> IsHasCreditHistory { get; set; }

        [DisplayName("Observaciones")]
        [DataMember(Name = "Observations", EmitDefaultValue = true, IsRequired = true, Order = 15)]
        public String Observations { get; set; }

        [DisplayName("Seguro")]
        [DataMember(Name = "SocialSecurityTypesId", EmitDefaultValue = true, IsRequired = true, Order = 16)]
        public Nullable<Int32> SocialSecurityTypesId { get; set; }

        [IgnoreDataMember]
        public CivilStatus CivilStatus { get; set; }

        [IgnoreDataMember]
        public ProspectiveClient ProspectiveClient { get; set; }

        [IgnoreDataMember]
        public SocialSecurityTypes SocialSecurityTypes { get; set; }
        #endregion properties
        #region constructores & destructores
        public ProspectiveClientDetail()
        {
            this.ProspectiveClientId = null;
            this.NSS = null;
            this.MonthlySalary1 = null;
            this.Antiquity1 = null;
            this.MonthlySalary2 = null;
            this.Antiquity2 = null;
            this.IsActiveSAT = null;
            this.Activiy = null;
            this.MoneyReceipt = null;
            this.CivilStatusId = null;
            this.IsWorngCreditBureau = null;
            this.DebtMonthly = null;
            this.ProblemType = null;
            this.IsHasCreditHistory = null;
            this.Observations = null;
            this.SocialSecurityTypesId = null;
            //*===*//
            this.ProspectiveClient = null;
            this.CivilStatus = null;
            this.SocialSecurityTypes = null;
        }
        public ProspectiveClientDetail(Nullable<Int32> prospectiveClientId, String nss, Nullable<decimal> monthlySalary1, Nullable<short> antiquity1, Nullable<decimal> monthlySalary2, Nullable<short> antiquity2, Nullable<Boolean> isActiveSAT,
                                       String activiy, String moneyReceipt, Nullable<Int32> civilStatusId, Nullable<Boolean> isWorngCreditBureau, Nullable<decimal> debtMonthly, String problemType, Nullable<Boolean> isHasCreditHistory, String observations, Nullable<Int32> socialSecurityTypesId) : this()
        {
            this.setValue(prospectiveClientId,nss,monthlySalary1,antiquity1,monthlySalary2,antiquity2,isActiveSAT,activiy,moneyReceipt,civilStatusId,isWorngCreditBureau,debtMonthly,problemType,isHasCreditHistory,observations, socialSecurityTypesId);
        }
        public ProspectiveClientDetail(ProspectiveClientDetail obj)
        {
            this.setValue(obj);
        }
        #endregion constructores & destructores
        #region setValue
        public void setValue(Nullable<Int32> prospectiveClientId, String nss, Nullable<decimal> monthlySalary1, Nullable<short> antiquity1, Nullable<decimal> monthlySalary2, Nullable<short> antiquity2, Nullable<Boolean> isActiveSAT,
                                       String activiy, String moneyReceipt, Nullable<Int32> civilStatusId, Nullable<Boolean> isWorngCreditBureau, Nullable<decimal> debtMonthly, String problemType, Nullable<Boolean> isHasCreditHistory, String observations, Nullable<Int32> socialSecurityTypesId)
        {
            this.ProspectiveClientId = prospectiveClientId;
            this.NSS = nss;
            this.MonthlySalary1 = monthlySalary1;
            this.Antiquity1 = antiquity1;
            this.MonthlySalary2 = MonthlySalary2;
            this.Antiquity2 = antiquity2;
            this.IsActiveSAT = isActiveSAT;
            this.Activiy = activiy;
            this.MoneyReceipt = moneyReceipt;
            this.CivilStatusId = civilStatusId;
            this.IsWorngCreditBureau = isWorngCreditBureau;
            this.DebtMonthly = debtMonthly;
            this.ProblemType = problemType;
            this.IsHasCreditHistory = isHasCreditHistory;
            this.Observations = observations;
            this.SocialSecurityTypesId = socialSecurityTypesId;
        }
        public void setValue(ProspectiveClientDetail obj)
        {
            this.ProspectiveClientId = obj.ProspectiveClientId;
            this.NSS = obj.NSS;
            this.MonthlySalary1 = obj.MonthlySalary1;
            this.Antiquity1 = obj.Antiquity1;
            this.MonthlySalary2 = obj.MonthlySalary2;
            this.Antiquity2 = obj.Antiquity2;
            this.IsActiveSAT = obj.IsActiveSAT;
            this.Activiy = obj.Activiy;
            this.MoneyReceipt = obj.MoneyReceipt;
            this.CivilStatusId = obj.CivilStatusId;
            this.IsWorngCreditBureau = obj.IsWorngCreditBureau;
            this.DebtMonthly = obj.DebtMonthly;
            this.ProblemType = obj.ProblemType;
            this.IsHasCreditHistory = obj.IsHasCreditHistory;
            this.Observations = obj.Observations;
            this.SocialSecurityTypesId = obj.SocialSecurityTypesId;
        }
        #endregion setValue
        #region updateValue
        public void updateValue(ProspectiveClientDetail obj)
        {
            this.ProspectiveClientId = obj.ProspectiveClientId;
            this.NSS = obj.NSS;
            this.MonthlySalary1 = obj.MonthlySalary1;
            this.Antiquity1 = obj.Antiquity1;
            this.MonthlySalary2 = obj.MonthlySalary2;
            this.Antiquity2 = obj.Antiquity2;
            this.IsActiveSAT = obj.IsActiveSAT;
            this.Activiy = obj.Activiy;
            this.MoneyReceipt = obj.MoneyReceipt;
            this.CivilStatusId = obj.CivilStatusId;
            this.IsWorngCreditBureau = obj.IsWorngCreditBureau;
            this.DebtMonthly = obj.DebtMonthly;
            this.ProblemType = obj.ProblemType;
            this.IsHasCreditHistory = obj.IsHasCreditHistory;
            this.Observations = obj.Observations;
            this.SocialSecurityTypesId = obj.SocialSecurityTypesId;
        }
        #endregion updateValue
        #region clearValues
        public void clearValues()
        {
            this.ProspectiveClientId = null;
            this.NSS = null;
            this.MonthlySalary1 = null;
            this.Antiquity1 = null;
            this.MonthlySalary2 = null;
            this.Antiquity2 = null;
            this.IsActiveSAT = null;
            this.Activiy = null;
            this.MoneyReceipt = null;
            this.CivilStatusId = null;
            this.IsWorngCreditBureau = null;
            this.DebtMonthly = null;
            this.ProblemType = null;
            this.IsHasCreditHistory = null;
            this.Observations = null;
            this.SocialSecurityTypesId = null;
        }
        #endregion clearValues
    }
}
