﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Domain.CallCenter
{
    [DataContract(Name = "PurchaseType", Namespace = "http://www.socasesores.com", IsReference = false)]
    public class PurchaseType
    {
        #region properties
        [DataMember(Name = "Id", EmitDefaultValue = true, IsRequired = true, Order = 1)]
        public Nullable<Int32> Id { get; set; }

        [DataMember(Name = "Name", EmitDefaultValue = true, IsRequired = true, Order = 2)]
        public String Name { get; set; }

        [IgnoreDataMember]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public ICollection<ProspectiveClient> ProspectiveClient { get; set; }
        #endregion properties
        #region constructores & destructores
        public PurchaseType()
        {
            this.Id = null;
            this.Name = null;
            this.ProspectiveClient = new HashSet<ProspectiveClient>();
        }
        public PurchaseType(Nullable<Int32> id, String name)
        {
            this.SetValue(id, name);
        }
        public PurchaseType(PurchaseType obj)
        {
            this.SetValue(obj);
        }
        #endregion constructores & destructores
        #region SetValue
        public void SetValue(Nullable<Int32> id, String name)
        {
            this.Id = id;
            this.Name = name;
        }
        public void SetValue(PurchaseType obj)
        {
            this.Id = obj.Id;
            this.Name = obj.Name;
        }
        #endregion SetValue
        #region UpdateValue
        public void updateValue(PurchaseType obj)
        {
            this.Name = obj.Name;
        }
        #endregion UpdateValue
        #region clearValues
        public void clearValues()
        {
            this.Name = null;
        }
        #endregion
    }
}
