﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;

namespace Domain.CallCenter.DTO
{
    [DataContract(Name = "ProspectiveClientDetailDto", Namespace = "http://www.socasesores.com", IsReference = false)]
    public class ProspectiveClientDetailDto
    {
        #region properties
        //[Key] //temp
        //[DatabaseGenerated(DatabaseGeneratedOption.None)] //temp
        //public Nullable<Int32> Id { get; set; } //temp

        [DataMember(Name = "PC", EmitDefaultValue = true, IsRequired = true, Order = 2)]
        public ProspectiveClient PC { get; set; }

        [DataMember(Name = "PCD", EmitDefaultValue = true, IsRequired = true, Order = 3)]
        public ProspectiveClientDetail PCD { get; set; }
        #endregion properties
        #region constructors & destructor
        public ProspectiveClientDetailDto()
        {
            this.PC = new ProspectiveClient();
            this.PCD = new ProspectiveClientDetail();
        }
        public ProspectiveClientDetailDto(Nullable<Int32> id, String firstName, String secondName, String lastName1, String lastName2, Nullable<DateTime> dateCreation, Nullable<DateTime> birthDate,
                                          String telephone1, String telephone2, String email1, String email2, Nullable<Int32> residenceLocationId, Nullable<Int32> birthLocationId, String curp,
                                          String rfc, Nullable<decimal> propertyValue, Nullable<decimal> deposit, Nullable<decimal> requestedAmount, Nullable<Int32> advertisingCampaignId, Nullable<Int32> prospectiveClientStatusId,
                                          Nullable<Int32> propertyTypeId, Nullable<Int32> creditTypeId, Nullable<Int32> purchaseTypeId, Nullable<Int32> locationStateId, Nullable<Int32> locationCityId, Nullable<Int32> brokerId,
                                          Nullable<Int32> prospectiveClientId, String nss, Nullable<decimal> monthlySalary1, Nullable<short> antiquity1, Nullable<decimal> monthlySalary2, Nullable<short> antiquity2, Nullable<Boolean> isActiveSAT,
                                          String activiy, String moneyReceipt, Nullable<Int32> civilStatusId, Nullable<Boolean> isWorngCreditBureau, Nullable<decimal> debtMonthly, String problemType, Nullable<Boolean> isHasCreditHistory, String observations, Nullable<Int32> socialSecurityTypesId) : this()
        {
            this.setValue(id, firstName, secondName, lastName1, lastName2, dateCreation, birthDate, telephone1, telephone2, email1, email2, residenceLocationId, birthLocationId, curp, rfc, propertyValue, deposit, requestedAmount, advertisingCampaignId, prospectiveClientStatusId, propertyTypeId, 
                          creditTypeId, purchaseTypeId, locationStateId, locationCityId, brokerId,
                          prospectiveClientId, nss, monthlySalary1, antiquity1, monthlySalary2, antiquity2, isActiveSAT, activiy, moneyReceipt, civilStatusId, isWorngCreditBureau, debtMonthly, problemType, isHasCreditHistory, observations, socialSecurityTypesId);
        }
        public ProspectiveClientDetailDto(ProspectiveClient obj1, ProspectiveClientDetail obj2) : this()
        {
            this.setValue(obj1, obj2);
        }
        #endregion constructors & destructor
        #region setValue
        public void setValue(Nullable<Int32> id, String firstName, String secondName, String lastName1, String lastName2, Nullable<DateTime> dateCreation, Nullable<DateTime> birthDate,
                             String telephone1, String telephone2, String email1, String email2, Nullable<Int32> residenceLocationId, Nullable<Int32> birthLocationId, String curp,
                             String rfc, Nullable<decimal> propertyValue, Nullable<decimal> deposit, Nullable<decimal> requestedAmount, Nullable<Int32> advertisingCampaignId, Nullable<Int32> prospectiveClientStatusId,
                             Nullable<Int32> propertyTypeId, Nullable<Int32> creditTypeId, Nullable<Int32> purchaseTypeId, Nullable<Int32> locationStateId, Nullable<Int32> locationCityId, Nullable<Int32> brokerId,
                             Nullable<Int32> prospectiveClientId, String nss, Nullable<decimal> monthlySalary1, Nullable<short> antiquity1, Nullable<decimal> monthlySalary2, Nullable<short> antiquity2, Nullable<Boolean> isActiveSAT,
                             String activiy, String moneyReceipt, Nullable<Int32> civilStatusId, Nullable<Boolean> isWorngCreditBureau, Nullable<decimal> debtMonthly, String problemType, Nullable<Boolean> isHasCreditHistory, String observations, Nullable<Int32> socialSecurityTypesId)
        {
            this.PC.setValue(id, firstName, secondName, lastName1, lastName2, dateCreation, birthDate, telephone1, telephone2, email1, email2, residenceLocationId, birthLocationId, curp, rfc, propertyValue, deposit, requestedAmount,
                             advertisingCampaignId, prospectiveClientStatusId, propertyTypeId, creditTypeId, purchaseTypeId, locationStateId, locationCityId, brokerId);
            this.PCD.setValue(prospectiveClientId, nss, monthlySalary1, antiquity1, monthlySalary2, antiquity2, isActiveSAT, activiy, moneyReceipt, civilStatusId, isWorngCreditBureau, debtMonthly, problemType, isHasCreditHistory, observations, socialSecurityTypesId);
        }
        public void setValue(ProspectiveClient obj1, ProspectiveClientDetail obj2)
        {
            this.PC.setValue(obj1);
            this.PCD.setValue(obj2);
        }
        #endregion setValue
        #region updateValue
        public void updateValue(ProspectiveClient obj1, ProspectiveClientDetail obj2)
        {
            this.PC.updateValue(obj1);
            this.PCD.updateValue(obj2);
        }
        #endregion updateValue
        #region clearValues
        public void clearValues()
        {
            this.PC.clearValues();
            this.PCD.clearValues();
        }
        #endregion clearValues
        #region ToProspectiveClient
        public ProspectiveClient ToProspectiveClient()
        {
            ProspectiveClient result = new ProspectiveClient();
            result.Id = this.PC.Id;
            result.FirstName = this.PC.FirstName;
            result.SecondName = this.PC.SecondName;
            result.LastName1 = this.PC.LastName1;
            result.LastName2 = this.PC.LastName2;
            result.Telephone1 = this.PC.Telephone1;
            result.Email1 = this.PC.Email1;
            result.Email2 = this.PC.Email2;
            result.BirthDate = this.PC.BirthDate;
            result.CURP = this.PC.CURP;
            result.RFC = this.PC.RFC;
            result.Deposit = this.PC.Deposit;
            result.PropertyValue = this.PC.PropertyValue;
            result.RequestedAmount = this.PC.RequestedAmount;
            result.AdvertisingCampaignId = this.PC.AdvertisingCampaignId;
            result.BirthLocationId = this.PC.BirthLocationId;
            result.BrokerId = this.PC.BrokerId;
            result.CreditTypeId = this.PC.CreditTypeId;
            result.LocationCityId = this.PC.LocationCityId;
            result.LocationState1 = this.PC.LocationState1;
            result.LocationState2 = this.PC.LocationState2;
            result.LocationStateId = this.PC.LocationStateId;
            result.PropertyTypeId = this.PC.PropertyTypeId;
            result.ProspectiveClientStatusId = this.PC.ProspectiveClientStatusId;
            result.PurchaseTypeId = this.PC.PurchaseTypeId;
            result.ResidenceLocationId = this.PC.ResidenceLocationId;
            return result;
        }
        #endregion ToProspectiveClient
        #region ToProspectiveClientDetail
        public ProspectiveClientDetail ToProspectiveClientDetail()
        {
            ProspectiveClientDetail result = new ProspectiveClientDetail();
            result.Activiy = this.PCD.Activiy;
            result.Antiquity1 = this.PCD.Antiquity1;
            result.Antiquity2 = this.PCD.Antiquity2;
            result.DebtMonthly = this.PCD.DebtMonthly;
            result.IsActiveSAT = this.PCD.IsActiveSAT;
            result.IsHasCreditHistory = this.PCD.IsHasCreditHistory;
            result.IsWorngCreditBureau = this.PCD.IsWorngCreditBureau;
            result.MoneyReceipt = this.PCD.MoneyReceipt;
            result.MonthlySalary1 = this.PCD.MonthlySalary1;
            result.MonthlySalary2 = this.PCD.MonthlySalary2;
            result.NSS = this.PCD.NSS;
            result.Observations = this.PCD.Observations;
            result.ProblemType = this.PCD.ProblemType;
            result.CivilStatusId = this.PCD.CivilStatusId;
            result.ProspectiveClientId = this.PCD.ProspectiveClientId;
            result.SocialSecurityTypesId = this.PCD.SocialSecurityTypesId;
            return result;
        }
        #endregion ToProspectiveClientDetail
        #region FromEntity
        public ProspectiveClientDetailDto FromEntity(ProspectiveClient e)
        {
            ProspectiveClientDetailDto result = new ProspectiveClientDetailDto();
            result.setValue(e, new ProspectiveClientDetail());
            return result;
        }
        public ProspectiveClientDetailDto FromEntity(ProspectiveClientDetail e)
        {
            ProspectiveClientDetailDto result = new ProspectiveClientDetailDto();
            result.setValue(new ProspectiveClient(), e);
            return result;
        }
        #endregion FromEntity
    }
}
