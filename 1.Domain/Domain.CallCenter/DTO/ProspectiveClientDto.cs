﻿using System;
using System.Runtime.Serialization;

namespace Domain.CallCenter.DTO
{
    [DataContract(Name = "ProspectiveClientDto", Namespace = "http://www.socasesores.com", IsReference = false)]
    public class ProspectiveClientDto
    {
        #region properties
        [DataMember(Name = "Id", EmitDefaultValue = true, IsRequired = true, Order = 1)]
        public Nullable<Int32> Id { get; set; }

        [DataMember(Name = "FirstName", EmitDefaultValue = true, IsRequired = true, Order = 2)]
        public String FirstName { get; set; }

        [DataMember(Name = "SecondName", EmitDefaultValue = true, IsRequired = true, Order = 3)]
        public String SecondName { get; set; }

        [DataMember(Name = "LastName1", EmitDefaultValue = true, IsRequired = true, Order = 4)]
        public String LastName1 { get; set; }

        [DataMember(Name = "LastName2", EmitDefaultValue = true, IsRequired = true, Order = 5)]
        public String LastName2 { get; set; }
        
        [DataMember(Name = "Telephone1", EmitDefaultValue = true, IsRequired = true, Order = 6)]
        public String Telephone1 { get; set; }
        
        [DataMember(Name = "Email1", EmitDefaultValue = true, IsRequired = true, Order = 7)]
        public string Email1 { get; set; }

        [DataMember(Name = "AdvertisingCampaignId", EmitDefaultValue = true, IsRequired = true, Order = 8)]
        public Nullable<Int32> AdvertisingCampaignId { get; set; }
        #endregion properties
        #region constructors & destructor
        public ProspectiveClientDto()
        {
            this.Id = null;
            this.FirstName = null;
            this.SecondName = null;
            this.LastName1 = null;
            this.LastName2 = null;
            this.Telephone1 = null;
            this.Email1 = null;
            this.AdvertisingCampaignId = null;
        }
        public ProspectiveClientDto(Nullable<Int32> id, String firstName, String secondName, String lastName1, String lastName2, String telephone1, String email1, Nullable<Int32> advertisingCampaignId) : this()
        {
            this.setValue(id, firstName, secondName, lastName1, lastName2, telephone1, email1, advertisingCampaignId);
        }
        public ProspectiveClientDto(ProspectiveClient obj) : this()
        {
            this.setValue(obj);
        }
        #endregion constructors & destructor
        #region setValue
        public void setValue(Nullable<Int32> id, String firstName, String secondName, String lastName1, String lastName2, String telephone1, String email1, Nullable<Int32> advertisingCampaignId)
        {
            this.Id = id;
            this.FirstName = firstName;
            this.SecondName = secondName;
            this.LastName1 = lastName1;
            this.LastName2 = lastName2;
            this.Telephone1 = telephone1;
            this.Email1 = email1;
            this.AdvertisingCampaignId = advertisingCampaignId;
        }
        public void setValue(ProspectiveClient obj)
        {
            this.Id = obj.Id;
            this.FirstName = obj.FirstName;
            this.SecondName = obj.SecondName;
            this.LastName1 = obj.LastName1;
            this.LastName2 = obj.LastName2;
            this.Telephone1 = obj.Telephone1;
            this.Email1 = obj.Email1;
            this.AdvertisingCampaignId = obj.AdvertisingCampaignId;
        }
        #endregion setValue
        #region updateValue
        public void updateValue(ProspectiveClient obj)
        {
            this.FirstName = obj.FirstName;
            this.SecondName = obj.SecondName;
            this.LastName1 = obj.LastName1;
            this.LastName2 = obj.LastName2;
            this.Telephone1 = obj.Telephone1;
            this.Email1 = obj.Email1;
            this.AdvertisingCampaignId = obj.AdvertisingCampaignId;
        }
        #endregion updateValue
        #region clearValues
        public void clearValues()
        {
            this.FirstName = null;
            this.SecondName = null;
            this.LastName1 = null;
            this.LastName2 = null;
            this.Telephone1 = null;
            this.Email1 = null;
            this.AdvertisingCampaignId = null;
        }
        #endregion clearValues
        #region ToEntity
        public ProspectiveClient ToEntity()
        {
            ProspectiveClient result = new ProspectiveClient();
            result.Id = this.Id;
            result.FirstName = this.FirstName;
            result.SecondName = this.SecondName;
            result.LastName1 = this.LastName1;
            result.LastName2 = this.LastName2;
            result.Telephone1 = this.Telephone1;
            result.Email1 = this.Email1;
            result.AdvertisingCampaignId = this.AdvertisingCampaignId;
            return result;
        }
        #endregion ToEntity
        #region FromEntity
        public ProspectiveClientDto FromEntity(ProspectiveClient e)
        {
            ProspectiveClientDto result = new ProspectiveClientDto();
            result.Id = e.Id;
            result.FirstName = e.FirstName;
            result.SecondName = e.SecondName;
            result.LastName1 = e.LastName1;
            result.LastName2 = e.LastName2;
            result.Telephone1 = e.Telephone1;
            result.Email1 = e.Email1;
            result.AdvertisingCampaignId = e.AdvertisingCampaignId;
            return result;
        }
        #endregion FromEntity
    }
}
