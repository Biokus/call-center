﻿using System;
using System.Runtime.Serialization;

namespace Domain.Security
{
    [DataContract(Name = "User", Namespace = "http://www.socasesores.com", IsReference = false)]
    public class User
    {
        #region properties
        [DataMember(Name = "Id", EmitDefaultValue = true, IsRequired = true, Order = 1)]
        public Nullable<Int32> Id { get; set; }

        [DataMember(Name = "UserName", EmitDefaultValue = true, IsRequired = true, Order = 2)]
        public String UserName { get; set; }

        [DataMember(Name = "Password", EmitDefaultValue = true, IsRequired = true, Order = 3)]
        public String Password { get; set; }

        [DataMember(Name = "IdEmployee", EmitDefaultValue = true, IsRequired = true, Order = 4)]
        public Int32 IdEmployee { get; set; }

        [DataMember(Name = "IdProfile", EmitDefaultValue = true, IsRequired = true, Order = 5)]
        public Int32 IdProfile { get; set; }

        [IgnoreDataMember]
        public Employee Employee { get; set; }
        [IgnoreDataMember]
        public Profile Profile { get; set; }
        #endregion properties
        #region constructors & destructor
        public User()
        {
            this.Id = null;
            this.UserName = null;
            this.Password = null;
            this.Employee = null;
            this.Profile = null; 
        }
        public User(Nullable<Int32>id, String userName, String password)
        {
            this.setValue(id,userName,password);
        }
        public User(User obj)
        {
            this.setValue(obj);
        }
        #endregion constructors & destructor
        #region setValue
        public void setValue(Nullable<Int32> id, String userName, String password)
        {
            this.Id = id;
            this.UserName = userName;
            this.Password = password;
        }
        public void setValue(User obj)
        {
            this.Id = obj.Id;
            this.UserName = obj.UserName;
            this.Password = obj.Password;
        }
        #endregion setValue
        #region updateValue
        public void updateValue(User obj)
        {
            this.UserName = obj.UserName;
            this.Password = obj.Password;
        }
        #endregion updateValue
        #region clearValues
        public void clearValues()
        {
            this.UserName = null;
            this.Password = null;
        }
        #endregion clearValues
    }
}
