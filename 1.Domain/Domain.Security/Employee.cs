﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Domain.Security
{
    [DataContract(Name = "Employee", Namespace = "http://www.socasesores.com", IsReference = false)]
    public class Employee
    {
        #region properties
        [DataMember(Name = "Id", EmitDefaultValue = true, IsRequired = true, Order = 1)]
        public Nullable<Int32> Id { get; set; }
        [DataMember(Name = "FirstName", EmitDefaultValue = true, IsRequired = true, Order = 2)]
        public string FirstName { get; set; }
        [DataMember(Name = "SecondName", EmitDefaultValue = true, IsRequired = true, Order = 3)]
        public string SecondName { get; set; }
        [DataMember(Name = "LastName1", EmitDefaultValue = true, IsRequired = true, Order = 4)]
        public string LastName1 { get; set; }
        [DataMember(Name = "LastName2", EmitDefaultValue = true, IsRequired = true, Order = 5)]
        public string LastName2 { get; set; }
        [DataMember(Name = "CURP", EmitDefaultValue = true, IsRequired = true, Order = 6)]
        public string CURP { get; set; }
        [DataMember(Name = "Email", EmitDefaultValue = true, IsRequired = true, Order = 7)]
        public string Email { get; set; }
        [DataMember(Name = "DateCreation", EmitDefaultValue = true, IsRequired = true, Order = 8)]
        public Nullable<DateTime> DateCreation { get; set; }
        [IgnoreDataMember]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public ICollection<User> User { get; set; }
        #endregion properties
        #region constructors & destructor
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Employee()
        {
            this.Id = null;
            this.FirstName = null;
            this.SecondName = null;
            this.LastName1 = null;
            this.LastName2 = null;
            this.CURP = null;
            this.Email = null;
            this.DateCreation = null; 
            this.User = new HashSet<User>();
        }
        public Employee(Nullable<Int32> id, String firstName, String secondName, String lastName1, String lastName2, String curp, String email, Nullable<DateTime> dateCreation) : this()
        {
            this.setValue(id,firstName,secondName,lastName1,lastName2,curp,email,dateCreation);
        }
        public Employee(Employee obj) : this()
        {
            this.setValue(obj);
        }
        #endregion constructors & destructor
        #region setValue
        public void setValue(Nullable<Int32> id, String firstName, String secondName, String lastName1, String lastName2, String curp, String email, Nullable<DateTime> dateCreation)
        {
            this.Id = id;
            this.FirstName = firstName;
            this.SecondName = secondName;
            this.LastName1 = lastName1;
            this.LastName2 = lastName2;
            this.CURP = curp;
            this.Email = email;
            this.DateCreation = dateCreation;
        }
        public void setValue(Employee obj)
        {
            this.Id = obj.Id;
            this.FirstName = obj.FirstName;
            this.SecondName = obj.SecondName;
            this.LastName1 = obj.LastName1;
            this.LastName2 = obj.LastName2;
            this.CURP = obj.CURP;
            this.Email = obj.Email;
            this.DateCreation = obj.DateCreation;
        }
        #endregion setValue
        #region updateValue
        public void updateValue(Employee obj)
        {
            //this.Id = obj.Id;
            this.FirstName = obj.FirstName;
            this.SecondName = obj.SecondName;
            this.LastName1 = obj.LastName1;
            this.LastName2 = obj.LastName2;
            this.CURP = obj.CURP;
            this.Email = obj.Email;
            this.DateCreation = obj.DateCreation;
        }
        #endregion updateValue
        #region clearValues
        public void clearValues()
        {
            //this.Id = null;
            this.FirstName = null;
            this.SecondName = null;
            this.LastName1 = null;
            this.LastName2 = null;
            this.CURP = null;
            this.Email = null;
            this.DateCreation = null;
        }
        #endregion clearValues
    }
}

