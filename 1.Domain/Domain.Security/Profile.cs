﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Domain.Security
{
    [DataContract(Name = "Profile", Namespace = "http://www.socasesores.com", IsReference = false)]
    public class Profile
    {
        #region properties
        [DataMember(Name = "Id", EmitDefaultValue = true, IsRequired = true, Order = 1)]
        public Nullable<Int32> Id { get; set; }

        [DataMember(Name = "Name", EmitDefaultValue = true, IsRequired = true, Order = 2)]
        public String Name { get; set; }

        [IgnoreDataMember]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public ICollection<User> User { get; set; }
        #endregion properties
        #region constructors & destructor
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Profile()
        {
            this.Id = null;
            this.Name = null;
            this.User = new HashSet<User>();
        }
        public Profile(Nullable<Int32> id, String name)
        {
            this.setValue(id, name);
        }
        public Profile(Profile obj)
        {
            this.setValue(obj);
        }
        #endregion constructors & destructor
        #region setValue
        public void setValue(Nullable<Int32> id, String name)
        {
            this.Id = id;
            this.Name = name;
        }
        public void setValue(Profile obj)
        {
            this.Id = obj.Id;
            this.Name = obj.Name;
        }
        #endregion setValue
        #region updateValue
        public void updateValue(Profile obj)
        {
            this.Name = obj.Name; 
        }
        #endregion updateValue
        #region clearValues
        public void clearValues()
        {
            this.Name = null;
        }
        #endregion clearValues
    }
}
