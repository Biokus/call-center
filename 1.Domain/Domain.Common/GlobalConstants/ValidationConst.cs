﻿using System;

namespace Domain.Common.GlobalConstants
{
    public class ValidationConst
    {
        public const String required = @"es requerido";
        public const String stringLength = @"tiene una longitud máxima de ";
        public const String stringLengthChar = @" caracteres";
        public const String stringLengthDig = @" digitos";
        public const String range = @"tiene un rango de ";
        public const String emailRegExpr = @"^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?";
        public const String email = @"no tiene formato válido";
        public const String rfcRegExpr = @"^([A-Z,Ñ,&]{3,4}([0-9]{2})(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1])[A-Z|\d]{3})";
        public const String rfc = @"no tiene formato válido";
        public const String onlyIntegerRegExpr = @"^[0-9]+";
        public const String onlyInteger = @"sólo permite números enteros";
        public const String onlyDecimalRegExpr = @"^[0-9]*\.?[0-9]+";
        public const String onlyDecimal = @"sólo permite números decimales";
        //
        public const String uecInvalidOperationType = @"90";
        public const String xemInvalidOperationType = @"Tipo de operación inválido";
        //
        public const String remDataValidationException = @"Error de validación de datos";
        public const String uecDataValidationException = @"91";
        //
        public const String remDbEntityValidationException = @"Error(es) de validación de entidad";
        public const String uecDbEntityValidationException = @"92";
        //
        public const String uecDbUpdateException = @"93";
        public const String uemDbUpdateException = @"Error(es) de actualización de datos";
        //
        public const String uecSqlException = @"94";
        public const String uemSqlException = @"Error(es) de acceso a la base de datos";
        //
        public const String uecInvalidOperationException = @"95";
        public const String uemInvalidOperationException = @"Error de base datos, notifique al administrador";
        //
        public const String uecException = @"99";
        public const String uemException = @"Error indeterminado";
    }
}
