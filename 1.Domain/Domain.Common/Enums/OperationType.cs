﻿using System.ComponentModel;
using System.Runtime.Serialization;

namespace Domain.Common.Enums
{
    [DataContract(Name = "OT", Namespace = "http://www.socasesores.com", IsReference = false)]
    public enum OperationType
    {
        [EnumMember(Value = "Indefined")]
        [Description("Indefinido")]
        Indefined = 0,

        [EnumMember(Value = "Read")]
        [Description("Consulta")]
        Read = 1,

        [EnumMember(Value = "SpecificRead")]
        [Description("Consulta especifica")]
        SpecificRead = 2,

        [EnumMember(Value = "Create")]
        [Description("Creación")]
        Create = 3,

        [EnumMember(Value = "Update")]
        [Description("Modificación")]
        Update = 4,

        [EnumMember(Value = "Delete")]
        [Description("Eliminación")]
        Delete = 5
    }
}
