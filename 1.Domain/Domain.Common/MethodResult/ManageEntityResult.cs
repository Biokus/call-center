﻿using Domain.Common.Control;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Domain.Common.MethodResult
{
    [DataContract(Namespace = "http://www.socasesores.com", IsReference = false)]
    public sealed class ManageEntityResult<E> where E : class, new()
    {
        #region fields

        #endregion fields
        #region properties
        [DataMember(Name = "ER", EmitDefaultValue = true, IsRequired = true, Order = 1)]
        public ExecutionResult ExecutionResult { get; set; }

        [DataMember(Name = "Items", EmitDefaultValue = true, IsRequired = true, Order = 2)]
        public List<E> Items { get; set; }
        #endregion properties
        #region constructors & destructor
        public ManageEntityResult()
        {
            this.ExecutionResult = new ExecutionResult();
            this.Items = new List<E>();
        }
        #endregion constructors & destructor
        #region setValue
        public void setValue(ExecutionResult executionResult)
        {
            this.ExecutionResult.setValue(executionResult);
        }
        public void setValue(ExecutionResult executionResult, List<E> items)
        {
            this.ExecutionResult.setValue(executionResult);
            this.Items = items;
        }
        #endregion setValue
        #region setValueOk
        public void setValueOk()
        {
            this.ExecutionResult.setValueOk();
        }
        public void setValueOk(List<E> items)
        {
            this.setValueOk();
            this.Items.Clear();
            this.Items.AddRange(items);
        }
        public void setValueOk(E item)
        {
            this.setValueOk();
            this.Items.Clear();
            this.Items.Add(item);
        }
        #endregion setValueOk
        #region setValueError
        public void setValueError(String errorMessage, String code, String message)
        {
            this.ExecutionResult.setValueError(errorMessage, code, message);
        }
        #endregion setValueError
    }
}
