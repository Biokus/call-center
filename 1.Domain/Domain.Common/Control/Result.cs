﻿using System;
using System.Runtime.Serialization;

namespace Domain.Common.Control
{
    [DataContract(Name = "R", Namespace = "http://www.socasesores.com", IsReference = false)]
    public sealed class Result
    {
        #region fields

        #endregion fields
        #region properties
        [DataMember(Name = "RS", EmitDefaultValue = true, IsRequired = true, Order = 1)]
        public Nullable<Boolean> Successful { get; set; }

        [DataMember(Name = "REM", EmitDefaultValue = true, IsRequired = true, Order = 2)]
        public String ErrorMessage { get; set; }
        #endregion properties
        #region constructors & destructor
        public Result()
        {
            this.Successful = null;
            this.ErrorMessage = null;
        }
        public Result(Nullable<Boolean> successful, String errorMessage)
        {
            this.setValue(successful, errorMessage);
        }
        public Result(Result obj)
        {
            this.setValue(obj);
        }
        #endregion constructors & destructor
        #region setValue
        public void setValue(Nullable<Boolean> successful, String errorMessage)
        {
            this.Successful = successful;
            this.ErrorMessage = errorMessage;
        }
        public void setValue(Result obj)
        {
            this.Successful = obj.Successful;
            this.ErrorMessage = obj.ErrorMessage;
        }
        #endregion setValue
        #region setValueOk
        public void setValueOk()
        {
            this.Successful = true;
            this.ErrorMessage = String.Empty;
        }
        #endregion setValueOk
        #region setValueError
        public void setValueError(String errorMessage)
        {
            this.Successful = false;
            this.ErrorMessage = errorMessage;
        }
        #endregion setValueError
    }
}
