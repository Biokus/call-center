﻿using System;
using System.Runtime.Serialization;

namespace Domain.Common.Control
{
    [DataContract(Name = "ER", Namespace = "http://www.socasesores.com", IsReference = false)]
    public sealed class ExecutionResult
    {
        #region fields

        #endregion fields
        #region properties
        [DataMember(Name = "R", EmitDefaultValue = true, IsRequired = true, Order = 1)]
        public Result Result { get; set; }

        [DataMember(Name = "UE", EmitDefaultValue = true, IsRequired = true, Order = 2)]
        public UserError UserError { get; set; }
        #endregion properties
        #region constructors & destructor
        public ExecutionResult()
        {
            this.Result = new Result();
            this.UserError = new UserError();
        }
        public ExecutionResult(Result result, UserError userError)
        {
            this.setValue(result, userError);
        }
        public ExecutionResult(ExecutionResult obj)
        {
            this.setValue(obj);
        }
        #endregion constructors & destructor
        #region setValue
        public void setValue(Result result, UserError userError)
        {
            this.Result.setValue(result);
            this.UserError.setValue(userError);
        }
        public void setValue(ExecutionResult obj)
        {
            this.Result.setValue(obj.Result);
            this.UserError.setValue(obj.UserError);
        }
        #endregion setValue
        #region setValueOk
        public void setValueOk()
        {
            this.Result.setValueOk();
            this.UserError.setValueOk();
        }
        #endregion setValueOk
        #region setValueError
        public void setValueError(String errorMessage, String code, String message)
        {
            this.Result.setValueError(errorMessage);
            this.UserError.setValueError(code, message);
        }
        #endregion setValueError
    }
}
