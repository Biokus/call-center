﻿using System;
using System.Runtime.Serialization;

namespace Domain.Common.Control
{
    [DataContract(Name = "UE", Namespace = "http://www.socasesores.com", IsReference = false)]
    public sealed class UserError
    {
        #region fields

        #endregion fields
        #region properties
        [DataMember(Name = "UEC", EmitDefaultValue = true, IsRequired = true, Order = 1)]
        public String Code { get; set; }

        [DataMember(Name = "UEM", EmitDefaultValue = true, IsRequired = true, Order = 2)]
        public String Message { get; set; }
        #endregion properties
        #region constructors & destructor
        public UserError()
        {
            this.Code = null;
            this.Message = null;
        }
        public UserError(String code, String message)
        {
            this.setValue(code, message);
        }
        public UserError(UserError obj)
        {
            this.setValue(obj);
        }
        #endregion constructors & destructor
        #region setValue
        public void setValue(String code, String message)
        {
            this.Code = code;
            this.Message = message;
        }
        public void setValue(UserError obj)
        {
            this.Code = obj.Code;
            this.Message = obj.Message;
        }
        #endregion setValue
        #region setValueOk
        public void setValueOk()
        {
            this.Code = String.Empty;
            this.Message = String.Empty;
        }
        #endregion setValueOk
        #region setValueError
        public void setValueError(String code, String message)
        {
            this.setValue(code, message);
        }
        #endregion setValueError
    }
}
