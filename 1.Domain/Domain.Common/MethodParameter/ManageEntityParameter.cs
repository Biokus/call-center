﻿using Domain.Common.Enums;
using System;
using System.Runtime.Serialization;

namespace Domain.Common.MethodParameter
{
    [DataContract(Namespace = "http://www.socasesores.com", IsReference = false)]
    public sealed class ManageEntityParameter<E> where E : class, new()
    {
        #region fields
        
        #endregion fields
        #region properties
        [DataMember(Name = "Token", EmitDefaultValue = true, IsRequired = true, Order = 1)]
        public Nullable<Guid> Token { get; set; }

        [DataMember(Name = "OperationType", EmitDefaultValue = true, IsRequired = true, Order = 2)]
        public Nullable<OperationType> OperationType { get; set; }

        [DataMember(Name = "Entity", EmitDefaultValue = true, IsRequired = true, Order = 3)]
        public E Entity { get; set; }
        #endregion properties
        #region constructors & destructor
        public ManageEntityParameter()
        {
            this.Token = null;
            this.OperationType = new OperationType();
            this.Entity = new E();
        }
        public ManageEntityParameter(Nullable<Guid> token, Nullable<OperationType> operationType)
        {
            this.Token = token;
            this.OperationType = operationType;
            this.Entity = new E();
        }
        public ManageEntityParameter(Nullable<Guid> token, Nullable<OperationType> operationType, E entity)
        {
            this.setValue(token, operationType, entity);
        }
        #endregion constructors & destructor
        #region setValue
        public void setValue(Nullable<Guid> token, Nullable<OperationType> operationType, E entity)
        {
            this.Token = token;
            this.OperationType = operationType;
            this.Entity = entity;
        }
        #endregion setValue
    }
}
